import { observable, action, computed } from "mobx";
import { ToastAndroid } from 'react-native'

export default class RecomStore {
    // attributes
    @observable recommendation 
    @observable curRecom = []
    // reset
    @action reset() {
        this.recommendation = null
    }
    // action/method
    @action storeNew(data) {
       
        this.recommendation = data;
        console.log('da luu xuong mobx: '+ JSON.stringify(this.recommendation))
    }
    
    @action resetCurRecom() {
        this.curRecom = []
    }
    
    @action updateCurRecom(data){
        this.curRecom = data
    }

    @computed get getStore() {
        return this.recommendation;
    }
   
    @computed get getRecom() {
        return this.curRecom;
    }
}
