import { observable, action, computed } from "mobx";
import { ToastAndroid } from 'react-native'

export default class UserStore {
    // attributes
    @observable user = {
        'token': '',
        'fname': '',
        'company_id': ''
    }
    // reset
    @action reset() {
        this.user = {
            'token': '',
            'fname': '',
            'company_id': ''
        }
    }
    // action/method
    @action StoreUser(user) {
        console.log(111)
        this.user = user;
        console.log(user)
    }

    @computed get GetUser() {
        return this.user;
    }
    @computed get GetCompanyID() {
        return this.user.company_id;
    }
    @computed get GetUserRestaurantID() {
        return('Restaurant' + this.user.company_id);
    }
}

