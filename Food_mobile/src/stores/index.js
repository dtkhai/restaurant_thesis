import OrderStore from './OrderStore'
import UserStore from './UserStore'
import NotiStore from './NotiStore'
import RecomStore from './RecomStore'
const orderStore = new OrderStore();
const userStore = new UserStore();
const notiStore = new NotiStore();
const recomStore = new RecomStore();
export { orderStore, userStore, notiStore, recomStore}