import { observable, action, computed } from "mobx";
import { ToastAndroid } from 'react-native'
export default class OrderStore {
	// attributes
	@observable currentOrder = {
		'order_list': [],
		'order_parent': null,
		'canceled': false,
	}
	@observable tracking = 0
	@observable lastFood = null
	// reset
	@action reset() {
		this.tracking = 0
		this.currentOrder = {
			'order_list': [],
			'combo_list': [],
			'order_parent': null,
			'canceled': false,
		};
	}
	@action clearFood(food_id) {
		this.tracking += 1
		this.currentOrder.order_list.forEach((l) => {
			if (l.food_id == food_id) {
				var index = this.currentOrder.order_list.indexOf(l)
				this.currentOrder.order_list.splice(index, 1)
			}
		}
		)
	}
	// action/method
	@action Addfood(food_id, food_name, main_image) {
		this.tracking += 1
		this.lastFood = food_id
		var isInArray = false;
		this.currentOrder.order_list.forEach((l) => {
			if ((l.food_id == food_id) && (l.combo == null)) {
				l.amount += 1
				isInArray = true;
			}
		});
		if (isInArray == false) {
			var a = {
				"food_id": food_id,
				"food_name": food_name,
				"main_image": main_image,
				'comment': "",
				"amount": 1,
				"combo": null
			}
			this.currentOrder.order_list.push(a)
		}
	}


	@action modifyFood(food_id, food_amount, food_comment, food_name, main_image) {
		this.tracking += 1
		var isInArray = false;
		this.currentOrder.order_list.forEach((l) => {
			if ((l.food_id == food_id) && (l.combo == null))  {
				l.amount = food_amount;
				l.comment = food_comment;
				isInArray = true;
			}
		});
		if (isInArray == false) {
			var a = {
				"food_id": food_id,
				"food_name": food_name,
				"main_image": main_image,
				'comment': food_comment,
				"amount": food_amount
			}
			this.currentOrder.order_list.push(a)
		}
	}

	@action modifyComment(food_id, food_comment, combo) {
		this.tracking += 1
		var isInArray = false;
		this.currentOrder.order_list.forEach((l) => {
			if ((l.food_id == food_id) && (l.combo === combo))  {
				l.comment = food_comment;
				isInArray = true;
			}
		});
	
	}

	@action removeFood(food_id) {
		this.tracking += 1
		this.currentOrder.order_list.forEach((l) => {
			if ((l.food_id == food_id) && (l.combo == null))  {
				if (l.amount == 1) {
					var index = this.currentOrder.order_list.indexOf(l)
					this.currentOrder.order_list.splice(index, 1)
				}
				else {
					l.amount -= 1
				}
			}
		});
	}

	@action addCombo(combo_id, combo_name, main_image, foods) {
		this.tracking += 1

		var isInArray = false;
		this.currentOrder.combo_list.forEach((l) => {
			if (l.combo_id == combo_id) {
				l.amount += 1
				isInArray = true;


				this.currentOrder.order_list.forEach((l) => {
					if (l.combo == combo_id) {
						l.amount += 1

					}
				});


			}
		});
		if (isInArray == false) {
			var a = {
				"combo_id": combo_id,
				"combo_name": combo_name,
				"main_image": main_image,
				'comment': "",
				"amount": 1
			}
			this.currentOrder.combo_list.push(a)
			for (var i = 0; i < foods.length; i++) {


				var a = {
					"food_id": foods[i].id,
					"food_name": foods[i].name,
					"main_image":  foods[i].main_image,
					'comment': "",
					"amount": 1,
					"combo": combo_id
				}
				this.currentOrder.order_list.push(a)

			}
		}
	}
	@action removeCombo(combo_id) {
		this.tracking += 1
		this.currentOrder.combo_list.forEach((l) => {
			if (l.combo_id == combo_id) {
				if (l.amount == 1) {
					for(var i =0;i<this.currentOrder.order_list.length; i++){
						if (this.currentOrder.order_list[i].combo  == combo_id) {
							
							this.currentOrder.order_list.splice(i, 1)
							i = i -1
						}
					}
					this.currentOrder.order_list.forEach((l) => {
						if (l.combo  === combo_id) {
							var index = this.currentOrder.order_list.indexOf(l)
							this.currentOrder.order_list.splice(index, 1)
						}
					});
					var index = this.currentOrder.combo_list.indexOf(l)
					this.currentOrder.combo_list.splice(index, 1)
				}
				else {
					l.amount -= 1
					this.currentOrder.order_list.forEach((l) => {
						if (l.combo == combo_id) {
							l.amount -= 1
	
						}
					});
				}
			}
		});
	}
	@action clearCombo(combo_id) {
		this.tracking += 1
		this.currentOrder.combo_list.forEach((l) => {
			if (l.combo_id == combo_id) {
				var index = this.currentOrder.combo_list.indexOf(l)
				this.currentOrder.combo_list.splice(index, 1)
				for(var i =0;i<this.currentOrder.order_list.length; i++){
					if (this.currentOrder.order_list[i].combo  == combo_id) {
						
						this.currentOrder.order_list.splice(i, 1)
						i = i -1
					}
				}
			}
		}
		)
	}

	@action modifyCombo(combo_id, combo_amount, combo_name, main_image,foods) {
		this.tracking += 1
		var isInArray = false;
		this.currentOrder.combo_list.forEach((l) => {
			if ((l.combo_id == combo_id))  {
				l.amount = combo_amount;
				isInArray = true;
				this.currentOrder.order_list.forEach((l) => {
					if (l.combo == combo_id) {
						l.amount = combo_amount
					}
				});
			}
		});
		if (isInArray == false) {
			var a = {
				"combo_id": combo_id,
				"combo_name": combo_name,
				"main_image": main_image,
				'comment': null,
				"amount": combo_amount
			}
			this.currentOrder.combo_list.push(a)
			for (var i = 0; i < foods.length; i++) {


				var a = {
					"food_id": foods[i].id,
					"food_name": foods[i].name,
					"main_image":  foods[i].main_image,
					'comment': "",
					"amount": combo_amount,
					"combo": combo_id
				}
				this.currentOrder.order_list.push(a)

			}
		}
	}


	@computed get totalPrice() {

	}
	@computed get foodGet() {
		return this.currentOrder
	}
	@computed get foodListGet() {
		return this.currentOrder.order_list
	}
	@computed get comboListGet() {
		return this.currentOrder.combo_list
	}
	@computed get foodListIdArray() {
		var temp =[]
		for(var i =0;i<this.currentOrder.order_list.length;i++){
			const tId = this.currentOrder.order_list[i].food_id.toString()
			temp.push(tId)
		}
		return temp
	}

	getAmount(food_id) {

		this.currentOrder.order_list.forEach((l) => {
			if (l.food_id == food_id) {
				return l.amount
			}
		}
		)

	}
}