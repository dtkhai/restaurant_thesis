import { observable, action, computed } from "mobx";
import { ToastAndroid } from 'react-native'

const test = [
    {
        id: 3,
        title: 'test',
        body: 'done Foodd adfa adfadf 1 adfa',
        table: 1 ,
        status: 0
    },
    {
        id: 2,
        title: 'test',
        body: 'done Foodd adfa adfadf 2 adfa',
        table: 1,
        status: 1
    },
    {
        id: 1,
        title: 'test',
        body: 'done Foodd adfa adfadf 3 adfa',
        table: 1,
        status: 1
    }
]
export default class NotiStore {
    
    // attributes
    @observable noti = [
     
    ]
    @observable isShow = false
    // reset
    @action reset() {
        this.noti = []
    }

    @action toggleShow() {
        this.isShow = !this.isShow
    }
    // action/method
    @action NewNoti(noti) {
        this.noti.unshift(noti)
    }

    @action markRead(id) {
        for (i = 0; i < this.noti.length; i++) { 
            if(this.noti[i].id==id){
                this.noti[i].status =1
            }
        }
    }

    @action markReadAll() {
        for (i = 0; i < this.noti.length; i++) {            
                this.noti[i].status =1            
        }
    }

    @computed get getNoti() {
        return this.noti;
    }
    @computed get nextId() {
        var nextID = 0
        for (i = 0; i < this.noti.length; i++) { 
            if(this.noti[i].id>nextID){
                nextID = this.noti[i].id
            }
        }
        return nextID+1
    }

    @computed get countUnread() { 
        var count = 0       
        for (i = 0; i < this.noti.length; i++) { 
            if(this.noti[i].status==0){
                count += 1
            }
        }
        return count
    }

    @computed get haveUnread() {        
        for (i = 0; i < this.noti.length; i++) { 
            if(this.noti[i].status==0){
                return true
            }
        }
        return false
    }
}

