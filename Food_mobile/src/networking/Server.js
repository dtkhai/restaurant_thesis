import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View} from 'react-native';
import {WEB_HOST_NAME} from '../lib/constants.js'
import {userStore} from '../stores/index'
const apiFoods = '/foods'
const apiOrders = '/order-histories'
const apiRecommendation = '/foods/recommendation'
const apiGroups = '/groups'
const apiGetByIds = '/foods/getByIds'
const apiCombos = '/combos'
const apiFilter = '/foods/filter?group_id='

const apiBillDiscount = '/discounts/bill-discount'

async function getFoodFromServer() {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiFoods + "?page_size=100"
  try {
    let response = await fetch( api );
    let responseJson = await response.json();
    return responseJson.data.foods;
  } catch (error) {
    console.error( error );
  }
}

async function FilterFoodFromServer(id) {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiFilter + id
  try {
    let response = await fetch( api );
    let responseJson = await response.json();
    return responseJson.data.foods;
  } catch (error) {
    console.error( error );
  }
}

async function getCombosFromServer() {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiCombos
  try {
    let response = await fetch( api );
    let responseJson = await response.json();
    return responseJson.data.combos;
  } catch (error) {
    console.error( error );
  }
}

async function getFoodGroupsFromServer() {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiGroups
  try {
    let response = await fetch( api );
    let responseJson = await response.json();
    return responseJson.data.groups;
  } catch (error) {
    console.error( error );
  }
}

async function getOrderFromServer() {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiOrders
  try {
    let response = await fetch( api );
    let responseJson = await response.json();
    return responseJson.data.groups;
  } catch (error) {
    console.error( error );
  }
}

async function AddOrderToServer(params) {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiOrders
  try {
    let response = await fetch( api,{
    	method: 'POST',
    	headers: {
    		'Accept' : 'application/json',
    		'Content-Type' : 'application/json',
    	},
    	body: JSON.stringify(params)
    } );
    let responseJson = await response.json();
    return responseJson.success;
  } catch (error) {
    console.error( error );
  }
}

async function getRecommendationFromServer() {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiRecommendation
  console.log( api );
  var params = {
    "user_id" : userStore.user.id
  }
  try {
    let response = await fetch( api,{
    	method: 'POST',
    	headers: {
    		'Accept' : 'application/json',
    		'Content-Type' : 'application/json',
    	},
    	body: JSON.stringify(params)
    } );
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.error( error );
  }
}

async function getFoodByIdsServer(params) {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiGetByIds

  try {
    let response = await fetch( api,{
    	method: 'POST',
    	headers: {
    		'Accept' : 'application/json',
    		'Content-Type' : 'application/json',
    	},
    	body: JSON.stringify(params)
    } );
    let responseJson = await response.json();
  
    return responseJson.data.foods;
  } catch (error) {
    console.error( error );
  }
}

async function GetDiscountFromServer() {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiBillDiscount
  try {
    let response = await fetch( api,{
    	method: 'POST',
    	headers: {
    		'Accept' : 'application/json',
    		'Content-Type' : 'application/json',
    	},
    	body: JSON.stringify({})
    } );
    let responseJson = await response.json();
    return responseJson.data;
  } catch (error) {
    console.error( error );
  }
}

export { getFoodFromServer, getOrderFromServer,AddOrderToServer, getFoodGroupsFromServer, getRecommendationFromServer, getFoodByIdsServer,
  getCombosFromServer,
  FilterFoodFromServer,
  GetDiscountFromServer };
