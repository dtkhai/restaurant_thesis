import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View} from 'react-native';
import {WEB_HOST_NAME} from '../lib/constants.js'
import {userStore} from '../stores/index'
const apiRooms = '/rooms'
const apiTables = '/tables'


async function getRoomFromServer() {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiRooms
  try {
    let response = await fetch( api );
    let responseJson = await response.json();
    return responseJson.data.rooms;
  } catch (error) {
    console.error( error );
  }
}


async function getTableFromServer(room) {
  var api = WEB_HOST_NAME + '/restaurants/'+ userStore.user.company_id + apiTables
  try {
  	var apiString = api + "?room_id=" + room
    let response = await fetch( apiString );
    let responseJson = await response.json();
    return responseJson.data.tables;
  } catch (error) {
    console.error( error );
  }
}

export { getRoomFromServer, getTableFromServer };