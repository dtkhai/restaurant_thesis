import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import {WEB_HOST_NAME} from '../lib/constants.js'
const LoginApi = WEB_HOST_NAME + '/users/login'

async function LoginAPI(body) {
    try {
        let response = await fetch(LoginApi, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        });
        let responseJson = await response.json();
        return responseJson;
    } catch (error) {
        return responseJson;
    }
}

export { LoginAPI };