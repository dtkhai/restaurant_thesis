
var OrderData = [
  {
    id: 1,
    tableId: 3,
    date: '23/1/2018',
    status: 'Done',
    foods: [
      {
        foodId: 1,
        amount: 3,
        comment: 'man'
      },
      {
        foodId: 2,
        amount: 3,
        comment: 'cayyy'
      },
      {
        foodId: 3,
        amount: 3,
        comment: 'cayyy'
      }
    ]
  },
  {
    id: 2,
    tableId: 2,
    date: '23/1/2018',
    status: 'Waiting',
    foods: [
      {
        foodId: 1,
        amount: 3,
        comment: 'aaa'
      },
      {
        foodId: 3,
        amount: 1,
        comment: 'koo'
      }
    ]
  },
  {
    id: 3,
    tableId: 3,
    date: '22/1/2018',
    status: 'Waiting',
    foods: [
      {
        foodId: 1,
        amount: 3,
        comment: 'aaa'
      },
      {
        foodId: 3,
        amount: 1,
        comment: 'koo'
      },
      {
        foodId: 2,
        amount: 5,
        comment: 'caasasa'
      }
    ]
  }
];
module.exports = OrderData
