import React, { Component } from 'react';
import { Alert, TouchableOpacity, Dimensions,PixelRatio, View, ScrollView, FlatList, RefreshControl, StyleSheet } from 'react-native';
import { Badge, Item, Input, Container, Header, Title, Left, Icon, Right, Button, Content, Text, Body } from 'native-base';
import Collapsible from 'react-native-collapsible';
import Modal from "react-native-modal";
import PayModal from '../../components/PayModal.js'
import Food from '../../components/Food.js'
import Combo from '../../components/Combo.js'
import FoodSide from '../../components/FoodSide.js'
import ComboSide from '../../components/ComboSide.js'
import ComboOrdered from '../../components/ComboOrdered.js'
import FoodCheckItem from '../../components/FoodCheckItem.js'
import FoodDetail from '../../components/FoodDetail.js'
import ComboDetail from '../../components/ComboDetail.js'
import RecommendPopUp from '../../components/RecommendPopUp.js'
import {
  getFoodFromServer,
  getCombosFromServer,
  AddOrderToServer,
  getFoodGroupsFromServer,
  FilterFoodFromServer,
  GetDiscountFromServer
} from '../../networking/Server.js'
import firebase from "react-native-firebase"


import { observable } from "mobx"
import { observer, inject } from "mobx-react/native"



@inject('orderStore', 'userStore', 'notiStore', 'recomStore')
@observer
export default class view extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      isLoaded: false,
      refreshing: false,
      isSearch: false,
      lastFood: null,
      searchCollapsed: true,
      isModalVisible: false,
      isComboModalVisible: false,
      isPayModalVisible: false,
      isConfirmModalVisible: false,

      curFoodDetail: null,
      curComboDetail: null,
      curTag: 0,
      numberFood: 0,
      totalPrice: 0,
      tableId: params ? params.tableId : null,
      tableName: params ? params.tableName : null,
      orderId: params ? params.orderId : null,

      billDiscount: null,
      recomendedFood: [],
      foodDataSource: null,
      comboDataSource: [],
      foodDataSourceFiltered: null,
      savedDataSource: null,
      foodGroupsDataSource: null,
      orderDataSource: { order_list: null },
      orderedList: null,
      foodFromFirebase: null,
      comboFromFirebase: null,
      isPaidAll: true,
      tracking: 0,
      lastFoodIds: null
    };

    const restaurantID = this.props.userStore.GetUserRestaurantID
    this.ref = firebase.firestore().collection(restaurantID);
    this.handler = this.handler.bind(this)
    this.handlerModal = this.handlerModal.bind(this)
    this.handlerComboModal = this.handlerComboModal.bind(this)
    this.handlerPayModal = this.handlerPayModal.bind(this)
    this.handlerFoodItem = this.handlerFoodItem.bind(this)
    this.handlerFoodInStore = this.handlerFoodInStore.bind(this)
  }

  componentDidMount() {

    this.refreshDataFromServer();
  }

  loadOrderFoodFromFirebase() {
    this.unsubscribe = this.ref.onSnapshot((query) => {
      const tArray = []
      const tOrdered = []
      const tCombo = []
      this.setState({
        isPaidAll: true,
      })
      query.forEach((item) => {
        if (this.state.orderId) {
          var temp = item.data()
          if (temp.order_id == this.state.orderId) {
            if (this.state.foodDataSource) {
              var curFood = this.state.foodDataSource.find(x => x.id === temp.food_id)
              if (curFood) {
                temp.fname = curFood.name
                temp.image = curFood.main_image
                temp.fprice = curFood.price
              }
            }
            if (temp.isPay == false) {
              this.setState({
                isPaidAll: false,
              })
            }
            if (temp.combo != null) {
              var curCombo = this.state.comboDataSource.find(x => x.id === temp.combo)
              if (temp.food_id == curCombo.foods[0].id) {
                var tempCom = {
                  "id": temp.combo,
                  "amount": temp.amount,
                  "name": curCombo.name,
                  'image': curCombo.image,
                  'foods': [temp],
                  'isPay': temp.isPay
                }
                var notFound = true
                for (var i = 0; i < tCombo.length; i++) {
                  if ((tempCom.id == tCombo[i].id) && (temp.isPay == tCombo[i].isPay)) {
                    tCombo[i].amount += temp.amount
                    tCombo[i].foods.push(temp)
                    notFound = false
                  }
                }
                if (notFound) {
                  tCombo.push(tempCom)
                }
              }
              else {

                var notFound = true
                for (var i = 0; i < tCombo.length; i++) {
                  if ((temp.combo == tCombo[i].id) && (temp.isPay == tCombo[i].isPay)) {

                    tCombo[i].foods.push(temp)
                    notFound = false
                  }
                }

              }
            }
            tOrdered.push(temp)
          }

        }
        tArray.push(item.data())
      })
      this.setState({
        foodFromFirebase: tArray,

      })
      if (this.state.orderId) {
        tOrdered.sort(function (a, b) { return b.isPay - a.isPay });
        this.setState({
          orderedList: tOrdered,
          comboFromFirebase: tCombo,
          isLoaded: true
        }, () => { this.calculateTotal() }
        )
      }
      else {
        this.setState({
          isLoaded: true
        }
        )
      }
    })
  }
  refreshDataFromServer = () => {
    this.setState({
      refreshing: true
    });
    getFoodFromServer().then((foods) => {
      foods.sort(function (a, b) {
        return a.id - b.id;
      });
      this.setState({
        foodDataSource: foods,
        savedDataSource: foods,
        foodDataSourceFiltered: foods,
        refreshing: false
      }, () => {
        getCombosFromServer().then((combos) => {
          this.setState({
            comboDataSource: combos,
          }, () => { this.loadOrderFoodFromFirebase() })
        })
      }
      );
    }).catch((error) => {
      this.setState({
        foodDataSource: [],
        foodDataSourceFiltered: foods,
        refreshing: false
      });
    });
    getFoodGroupsFromServer().then((groups) => {
      var tArray = [
        { "id": 0, "name": "All" },
        { "id": -1, "name": "Combo" }
      ]
      groups = tArray.concat(groups)
      this.setState({
        foodGroupsDataSource: groups,
        //refreshing: false
      });
    }).catch((error) => {
      this.setState({
        foodGroupsDataSource: [],
        //refreshing: false
      });
    });
    GetDiscountFromServer().then((data) => {
      this.setState({
        billDiscount: data,
        //refreshing: false
      });

    }).catch((error) => {
      this.setState({
        billDiscount: null,
        //refreshing: false
      });
    });
  }

  filterSearch(text) {

    newData = this.state.savedDataSource
    newData = newData.filter((item) => {
      const itemData = item.name.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    });
    this.setState({
      text: text,
      foodDataSourceFiltered: newData // after filter we are setting users to new array
    });
  }

  onRefresh = () => {
    this.refreshDataFromServer();
  }

  handler(e) {
    this.calculateTotal()

    this.findRecommendedFood(e)


  }

  handlerModal(e) {
    this.setState({
      curFoodDetail: this.state.foodDataSource.find(x => x.id === e)
    });
    this._toggleModal()
  }

  handlerComboModal(e) {
    this.setState({
      curComboDetail: this.state.comboDataSource.find(x => x.id === e)
    });
    this._toggleComboModal()
  }

  handlerPayModal(e) {
    this.setState({
      isPayModalVisible: false
    });

  }

  handlerFoodItem(e, f, c) {
    if (f == 'done') {

      this.ref.doc(e).update({
        status: 3
      })
    }
    else if (f == 'clear') {
      Alert.alert(
        'Confirm',
        'Do you really want to cancel',
        [
          { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          {
            text: 'OK', onPress: () => this.ref.doc(e).update({
              status: 5
            })
          },
        ],
        { cancelable: false }
      )
    }
    else if (f == 'detail') {
      this.setState({
        curFoodDetail: this.state.foodDataSource.find(x => x.id === e)
      });
      this._toggleModal()
    }
    else if (f == 'editComment') {
      this.ref.doc(e).update({
        note: c
      })
    }

  }

  handlerFoodInStore(f, isCombo, id, comment, combo_id) {
    if (isCombo) {
      if (f == 'add') {
        this.props.orderStore.addCombo(id);
      } else if (f == 'remove') {
        this.props.orderStore.removeCombo(id);
      }
      else if (f == 'clear') {
        this.props.orderStore.clearCombo(id);
      }
    }
    else {
      if (f == 'add') {
        this.props.orderStore.Addfood(id);
      } else if (f == 'remove') {
        this.props.orderStore.removeFood(id);
      }
      else if (f == 'clear') {
        this.props.orderStore.clearFood(id);
      }
      else if (f == 'comment') {
        this.props.orderStore.modifyComment(id, comment, combo_id);
      }
    }
  }

  componentWillMount() {
    this.props.orderStore.reset();
  }

  renderComboItem = ({ item, index }) => {
    var numOrdered = 0
    if (item.foods[0]) {
      if (this.state.orderedList) {
        for (var i = 0; i < this.state.orderedList.length; i++) {
          if ((this.state.orderedList[i].food_id == item.foods[0].id) && (this.state.orderedList[i].combo == item.id)) {
            numOrdered = numOrdered + this.state.orderedList[i].amount
          }
        }
      }
    }
    return (<Combo name={item.name}
      price={item.price}
      id={item.id}
      description=""
      main_image={item.image}
      handler={this.handler}
      numOrdered={numOrdered}
      handlerComboModal={this.handlerComboModal}
      discount={item.Discount}
      foods={item.foods}
      index={index}
    />);
  }

  renderItem = ({ item, index }) => {
    /*<Food name={item.name} price={item.price}/>*/
    var numOrdered = 0
    if (this.state.orderedList) {
      for (var i = 0; i < this.state.orderedList.length; i++) {
        if ((this.state.orderedList[i].food_id == item.id) && (this.state.orderedList[i].combo == null)) {
          numOrdered = numOrdered + this.state.orderedList[i].amount
        }
      }
    }
    return (<Food name={item.name}
      price={item.price}
      id={item.id}
      description={item.description}
      main_image={item.main_image}
      handler={this.handler}
      available={item.available}
      numOrdered={numOrdered}
      handlerModal={this.handlerModal}
      discount={item.Discount}
      index={index}
    />);
  }

  renderGroupItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.changeTag(item.id)}>
        <Badge style={[styles.foodGroup, (this.state.curTag == item.id) && styles.foodGroupActive]}>
          <Text>{item.name}</Text>
        </Badge>
      </TouchableOpacity>

    );
  }
  renderItemSide = ({ item }) => {
    /*<Food name={item.name} price={item.price}/>*/
    if (item.combo == null) {
      return (<FoodSide food_id={item.food_id}
        foodName={item.food_name}
        foodImg={item.main_image}
        comment={item.comment}
        amount={item.amount}
        handler={this.handler}
        handlerModal={this.handlerModal}
        handlerFoodInStore={this.handlerFoodInStore}
        combo={item.combo}
      />);
    }
  }
  renderComboSide = ({ item }) => {
    /*<Food name={item.name} price={item.price}/>*/
    var tFood = this.props.orderStore.currentOrder.order_list.filter(function (o) { return (o.combo == item.combo_id); });

    return (<ComboSide id={item.combo_id}
      name={item.combo_name}
      img={item.main_image}
      amount={item.amount}
      foods={tFood}
      btnActive={true}
      handler={this.handler}
      handlerFoodInStore={this.handlerFoodInStore}
      handlerComboModal={this.handlerComboModal}
    />);
  }
  renderComboOrdered = ({ item }) => {
    /*<Food name={item.name} price={item.price}/>*/
    if (!item.isPay) {
      return (<ComboOrdered id={item.id}
        name={item.name}
        img={item.image}
        amount={item.amount}
        foods={item.foods}
        btnActive={false}
        handler={this.handler}
        handlerComboModal={this.handlerComboModal}
        isPay={item.isPay}
        handlerFoodItem={this.handlerFoodItem}
      />);
    }
  }
  renderComboPaid = ({ item }) => {
    /*<Food name={item.name} price={item.price}/>*/
    if (item.isPay) {
      return (<ComboOrdered id={item.id}
        name={item.name}
        img={item.image}
        amount={item.amount}
        foods={item.foods}
        btnActive={false}
        handler={this.handler}
        handlerModal={this.handlerModal}
        isPay={item.isPay}
        handlerFoodItem={this.handlerFoodItem}
      />);
    }
  }

  renderOrderedFood = ({ item, index }) => {
    if (item.combo == null) {
      return (
        <FoodCheckItem
          id={item.id}
          food_id={item.food_id}
          foodName={item.fname}
          foodImg={item.image}
          price={item.fprice}
          isPay={item.isPay}
          time={item.time}
          status={item.status}
          comment={item.note}
          amount={item.amount}
          handlerFoodItem={this.handlerFoodItem}
        />
      );
    }
  }


  changeTag(tag) {
    this.setState({
      curTag: tag,
      searchCollapsed: true,
      text: null,
    });

    if ((tag != 0) && (tag != -1)) {
      this.setState({
        refreshing: true
      })
      FilterFoodFromServer(tag).then((foods) => {
        foods.sort(function (a, b) {
          return a.id - b.id;
        });
        this.setState({
          foodDataSourceFiltered: foods,
          savedDataSource: foods,
          refreshing: false
        })
      }).catch((error) => {
        this.setState({
          savedDataSource: [],
          foodDataSourceFiltered: [],
          refreshing: false
        });
      });
    }
    else {
      this.setState({

        foodDataSourceFiltered: this.state.foodDataSource,
        savedDataSource: this.state.foodDataSource,
        refreshing: false
      });
    }


  }

  _toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });

  }

  _toggleComboModal = () => {
    this.setState({ isComboModalVisible: !this.state.isComboModalVisible });

  }


  renderModal() {
    if (this.state.curFoodDetail) {
      return (
        <Modal isVisible={this.state.isModalVisible}
          avoidKeyboard={true}
          style={styles.modal}
          onBackdropPress={() => this.setState({ isModalVisible: false })}>
          <FoodDetail handlerModal={this.handlerModal}
            handler={this.handler}
            food={this.state.curFoodDetail}

          />
        </Modal>
      );
    }
  }

  renderComboModal() {
    if (this.state.curComboDetail) {
      return (
        <Modal isVisible={this.state.isComboModalVisible}
          avoidKeyboard={true}
          style={styles.modal}
          onBackdropPress={() => this.setState({ isComboModalVisible: false })}>
          <ComboDetail handlerComboModal={this.handlerComboModal}
            handler={this.handler}
            combo={this.state.curComboDetail}

          />
        </Modal>
      );
    }
  }

  renderPayModal() {
    return (
      <Modal isVisible={this.state.isPayModalVisible}
        avoidKeyboard={true}
        style={styles.modal}
        onBackdropPress={() => this.setState({ isPayModalVisible: false })}>
        <PayModal orderId={this.state.orderId}
          tableId={this.state.tableId}
          handlerModal={this.handlerPayModal}
          foodDataSource={this.state.foodDataSource}
          comboDataSource={this.state.comboDataSource}
          billDiscount={this.state.billDiscount} />
      </Modal>
    );
  }

  searchExpand() {
    if (!this.state.searchCollapsed) {
      const a = ""
      this.setState({
        text: null
      });
      this.filterSearch(a)
    }
    this.setState({
      searchCollapsed: !this.state.searchCollapsed
    });
  }

  renderConfirmAlert() {
    return (
      <Modal isVisible={this.state.isConfirmModalVisible} style={styles.center}>
        <View style={styles.modalSmall}>
          <View style={{ flex: 0.5, justifyContent: 'center', backgroundColor: "#4780c6", borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
            <Title style={{ fontSize: 22 }}>CONFIRMATION!</Title>
          </View>
          <View style={{ flex: 0.5, padding: 10, alignItems: 'center' }}>
            <Text style={{ fontSize: 18, textAlign: 'center' }}>
              Do you want to make an order
              </Text>
          </View>
          <View style={{
            justifyContent: "center",
            alignItems: "flex-end", flex: 0, flexDirection: 'row'
          }}>

            <Button full
              primary
              style={{ margin: 5, flex: 1, borderRadius: 10, marginLeft: 40, backgroundColor: '#c43e50' }}
              onPress={() => this.setState({ isConfirmModalVisible: false })}>
              <Text>
                Cancel
			        </Text>
            </Button>
            <Button full
              primary
              style={{ margin: 5, flex: 1, marginRight: 40, borderRadius: 10 }}
              onPress={() => this.addOrdertoServer()}>
              <Text>
                CONFIRM
			        </Text>
            </Button>

          </View>
        </View>
      </Modal>
    )
  }

  renderSuccessAlert() {

    return (
      <Modal isVisible={this.state.isInfoModalVisible}
      style={styles.center}
        onBackdropPress={() => this.setState({ isInfoModalVisible: false })}>
        <View style={styles.modalSmall}>
          <View style={{ flex: 1, padding: 10, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
            <Icon name="md-checkmark-circle" style={{ color: '#569108' }} />
            <Text style={{ fontSize: 18, textAlign: 'center', color: '#569108' }}>
              Order successfully added!!!
              </Text>
          </View>
          <View style={{
            justifyContent: "center",
            flex: 0, flexDirection: 'row'
          }}>

            <Button full
              primary
              style={{ margin: 5, flex: 1, borderRadius: 10, marginHorizontal: 160 }}
              onPress={() => this.setState({ isInfoModalVisible: false })}>
              <Text>
                OK
			        </Text>
            </Button>


          </View>
        </View>
      </Modal>
    )
    /* Alert.alert(
 
       'Order successful added',
 
     )*/
  }
  addOrdertoServer() {
    /*
    AddOrderToServer(this.props.orderStore.currentOrder).then((success) => {
      if(success ===true){
        this.showSuccessAlert();
      }
    });*/

    var tempCurFood = this.props.orderStore.foodListGet;
    var foodCount = 0;
    /*Alert.alert(JSON.stringify(tempCurFood))*/
    var id = 0
    var orderId = 0

    id = Math.max(...this.state.foodFromFirebase.map(o => o.id), 0);
    if (this.state.orderId) {
      orderId = this.state.orderId
    }
    else {
      orderId = Math.max(...this.state.foodFromFirebase.map(o => o.order_id), 0);
      orderId += 1
    }
    var batch = firebase.firestore().batch();
    for (var i = 0; i < tempCurFood.length; i++) {
      id += 1;
      var fname;
      var fimage;

      const tempID = JSON.stringify(id)
      const tdata = {
        name: tempCurFood[i].food_name,
        id: tempID,
        order_id: orderId,
        food_id: tempCurFood[i].food_id,
        table_id: this.state.tableId,
        table_name: this.state.tableName,
        amount: tempCurFood[i].amount,
        note: tempCurFood[i].comment,
        status: 0,
        time: new Date,
        image: tempCurFood[i].main_image,
        company_id: this.props.userStore.GetCompanyID,
        isPay: false,
        combo: tempCurFood[i].combo
      }
      var tNew = this.ref.doc(tempID)
      batch.set(tNew, tdata);



    }
    batch.commit().then(() => {
      this.props.orderStore.reset();
      this.setState({
        orderId: orderId,
        isInfoModalVisible: true,
        isConfirmModalVisible: false
      }, () => this.refreshDataFromServer())
    });



  }

  releaseTable() {

    var batch = firebase.firestore().batch();
    this.state.orderedList.forEach((item) => {
      batch.delete(this.ref.doc(item.id));
    })
    batch.commit().then(() => {
      this.props.navigation.goBack()
    });
  }

  calculateTotal = () => {
    var total = 0;
    this.props.orderStore.currentOrder.order_list.forEach((l) => {
      if (l.combo == null) {

        const temp = this.state.foodDataSource.find(x => x.id === l.food_id)
        if (temp) {
          const tPercent = temp.Discount ? temp.Discount.percentage_value : 0
          const tValue = temp.Discount ? temp.Discount.value : 0
          const price = temp.price - tPercent * temp.price / 100 - tValue
          total = total + price * l.amount;

        }

      }
    });
    this.props.orderStore.currentOrder.combo_list.forEach((l) => {

      const temp = this.state.comboDataSource.find(x => x.id === l.combo_id)
      if (temp) {
        const tPercent = temp.Discount ? temp.Discount.percentage_value : 0
        const tValue = temp.Discount ? temp.Discount.value : 0
        const price = temp.price - tPercent * temp.price / 100 - tValue
        total = total + price * l.amount;
      }

    });


    if (this.state.orderId) {
      if (this.state.orderedList) {
        this.state.orderedList.forEach((l) => {
          if (l.combo == null) {
            if ((l.status == 4) || (l.status == 5)) {
            }
            else {
              const temp = this.state.foodDataSource.find(x => x.id === l.food_id)
              if ((temp) && (!l.isPay)) {
                const tPercent = temp.Discount ? temp.Discount.percentage_value : 0
                const tValue = temp.Discount ? temp.Discount.value : 0
                const price = temp.price - tPercent * temp.price / 100 - tValue
                total = total + price * l.amount;
              }
            }
          }

        });
      }
      if (this.state.comboFromFirebase) {
        this.state.comboFromFirebase.forEach((l) => {
          if (l.isPay == false) {
            const temp = this.state.comboDataSource.find(x => x.id === l.id)
            if (temp) {
              const tPercent = temp.Discount ? temp.Discount.percentage_value : 0
              const tValue = temp.Discount ? temp.Discount.value : 0
              const price = temp.price - tPercent * temp.price / 100 - tValue
              total = total + price * l.amount;
            }
          }
        });
      }

    }
    total = Math.round(total * 100) / 100
    this.setState({
      totalPrice: total
    });
  }

  findRecommendedFood(id) {

    var maxConfi = 0
    var tempRecom = []
    const tArray = this.props.recomStore.recommendation
    const tFoodList = this.props.orderStore.foodListIdArray
    if (this.state.lastFoodIds !== tFoodList) {
      console.log('Food List' + JSON.stringify(tFoodList))
      id = id.toString()
      console.log('All Recommmend' + JSON.stringify(tArray))
      if (tArray) {
        for (var i = 0; i < tArray.length; i++) {
          
            if (!tArray[i].lhs.slice().some(val => tFoodList.indexOf(val) === -1)) {
              const tRhs = tArray[i].rhs.slice()
              if ((maxConfi < tArray[i].confidence) && (!tFoodList.includes(tRhs[0]))) {

                maxConfi = tArray[i].confidence
                tempRecom = tRhs
              }
            }
          }
        
      }

      tempRecom = tempRecom.filter(function (item, pos) {
        return tempRecom.indexOf(item) == pos;
      })
      tempRecom = tempRecom.slice(0, 3);
      this.setState({
        recomendedFood: tempRecom,
        lastFoodIds: tFoodList
      })
      console.log('Day la REcommendation: ' + JSON.stringify(tempRecom))
    }
  }

  renderPayButton() {
    if (this.state.isLoaded) {
      if (this.state.isPaidAll == false) {
        return (<Button full
          primary
          style={{ margin: 5, flex: 1, backgroundColor: '#c43e50', borderRadius: 10 }}
          onPress={() => {
            this.setState({ isPayModalVisible: true })
          }}>
          <Text>
            PAY
      </Text>

        </Button>)
      }
      else {
        return (<Button full
          primary
          style={{ margin: 5, flex: 1, backgroundColor: '#e0a031', borderRadius: 10 }}
          onPress={() => {
            Alert.alert(
              'Confirm',
              'Do you really want to release table',
              [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => this.releaseTable() },
              ],
              { cancelable: false }
            )

          }}>
          <Text>
            RELEASE
      </Text>

        </Button>)
      }
    }
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        {this.state.isModalVisible && this.renderModal()}
        {this.state.isComboModalVisible && this.renderComboModal()}
        {this.state.isPayModalVisible && this.renderPayModal()}
        {this.state.isConfirmModalVisible &&
          this.renderConfirmAlert()
        }
        {this.state.isInfoModalVisible &&
          this.renderSuccessAlert()
        }
        <View style={styles.bigScreen}>
          <Collapsible collapsed={!this.state.searchCollapsed} align="center">
            <Header>
              <Left>
                <Button transparent onPress={() => this.props.navigation.goBack()}>
                  <Icon name="arrow-back" />
                </Button>
              </Left>
              <Body>
                <Title>
                  Select Food for {this.state.tableName}
                </Title>
              </Body>
              <Right>
                <Button badge transparent onPress={() => { this.props.notiStore.toggleShow() }}>
                  {(this.props.notiStore.countUnread > 0)
                    &&
                    (<Badge style={{ position: 'absolute', elevation: 1, top: 2 }}>
                      <Text style={{ fontWeight: 'bold' }}>{this.props.notiStore.countUnread}</Text>
                    </Badge>)
                  }
                  <Icon name='md-notifications' />
                </Button>
                <Button transparent onPress={() => this.searchExpand()} >
                  <Icon name="search" />
                </Button>
              </Right>
            </Header>
          </Collapsible>
          <Collapsible collapsed={this.state.searchCollapsed} align="center">
            <Header searchBar rounded>
              <Item>
                <Icon active name="search" />
                <Input onChangeText={(text) => this.filterSearch(text)}
                  value={this.state.text} placeholder="Search" />
                <Icon active name="close" onPress={() => this.searchExpand()} />
              </Item>
              <Button transparent>
                <Text>Search</Text>
              </Button>
            </Header>
          </Collapsible>
          <View style={styles.tagContainer}>
            <FlatList data={this.state.foodGroupsDataSource}
              horizontal={true}
              renderItem={this.renderGroupItem}
              keyExtractor={(item, index) => index}
              style={{ margin: 0, padding: 0 }} />
          </View>
          <View
            style={{ padding: 5, flex: 1 }}>
            {(this.state.curTag >= 0) &&
              <FlatList data={this.state.foodDataSourceFiltered}
                renderItem={this.renderItem}
                keyExtractor={item => item.id}
                numColumns={3}
                contentContainerStyle={styles.list}
                refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />}
                style={{ margin: 0, padding: 0 }} />
            }
            {(this.state.curTag == -1) &&
              <FlatList data={this.state.comboDataSource}
                renderItem={this.renderComboItem}
                keyExtractor={(item, index) => index}
                numColumns={3}
                contentContainerStyle={styles.list}
                refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />}
                style={{ margin: 0, padding: 0 }} />
            }
          </View>
          <RecommendPopUp handler={this.handler} recomendedFood={this.state.recomendedFood} />
        </View>
        <View style={styles.smallScreen}   >
          <View style={styles.titleContainer}>
            <Title style={styles.titleLeft}>
              Your Order
	            </Title>
          </View>
          <ScrollView style={{ flex: 1 }}>
            {this.state.orderId && (<View style={{ borderBottomWidth: 0 }}>
              <View style={[{ backgroundColor: '#dddddd' }]}>
                <Text style={[{ paddingHorizontal: 7, paddingVertical: 4, color: 'gray', fontSize: 14 }]}>
                  ORDERED FOOD
							</Text>
              </View>
              <FlatList data={this.state.comboFromFirebase}
                renderItem={this.renderComboPaid}
                keyExtractor={(item, index) => index}
                style={{ margin: 0, padding: 0 }} />
              <FlatList data={this.state.orderedList}
                renderItem={this.renderOrderedFood}
                keyExtractor={(item, index) => index}
                style={{ margin: 0, padding: 0 }} />
              <FlatList data={this.state.comboFromFirebase}
                renderItem={this.renderComboOrdered}
                keyExtractor={(item, index) => index}
                style={{ margin: 0, padding: 0 }} />
            </View>)}

            <View >
              <View style={[{ backgroundColor: '#dddddd' }]}>
                <Text style={[{ paddingHorizontal: 7, paddingVertical: 4, color: 'gray', fontSize: 14 }]}>
                  NEW ORDER
							</Text>
              </View>
              <FlatList data={this.props.orderStore.currentOrder.order_list}
                extraData={this.props.orderStore.tracking}
                renderItem={this.renderItemSide}
                keyExtractor={(item, index) => index}
                style={{ margin: 0, padding: 0 }} />
              <FlatList data={this.props.orderStore.currentOrder.combo_list}
                renderItem={this.renderComboSide}
                keyExtractor={(item, index) => index}
                style={{ margin: 0, padding: 0 }} />
            </View>
          </ScrollView >

          <Text style={{ marginLeft: 10 }}>
            Total: {this.state.totalPrice} $
	    </Text>
          <View style={{ flexDirection: 'row', }}>
            {this.state.orderId &&
              (this.renderPayButton())
            }
            <Button full
              primary
              disabled={!(this.props.orderStore.currentOrder.order_list.length > 0)}
              style={{ margin: 5, flex: 1, borderRadius: 10 }}
              onPress={() => {
                this.setState({
                  isConfirmModalVisible: true
                })
              }}>
              <Text>
                CONFIRM
			        </Text>

            </Button>
          </View>
        </View>

      </View>
    );
  }
}

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const widthPercentageToDP = widthPercent => {
  const screenWidth = Dimensions.get('window').width;
  // Convert string input to decimal number
  const elemWidth = parseFloat(widthPercent);
  return PixelRatio.roundToNearestPixel(screenWidth * elemWidth / 100);
};
const heightPercentageToDP = heightPercent => {
  const screenHeight = Dimensions.get('window').height;
  // Convert string input to decimal number
  const elemHeight = parseFloat(heightPercent);
return PixelRatio.roundToNearestPixel(screenHeight * elemHeight / 100);
};

const styles = StyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  tagContainer: {
    paddingTop: 5,
    alignSelf: 'stretch', backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: 0.9,
    shadowRadius: 10,
    shadowOffset: {
      height: 0,
      width: 0
    },
    elevation: 2,
  },
  headerLeft: {

  },
  titleContainer: {

    alignItems: "center",
    margin: 10,
  },
  titleLeft: {
    textAlign: 'center',
    alignItems: "center",
    color: '#696969'
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'row',
    height: deviceHeight,
    padding: 0,

  },
  list: {

    justifyContent: 'space-between',
  },
  bigScreen: {
    flex: 0.76
  },
  smallScreen: {
    borderLeftWidth: 1,
    borderLeftColor: 'grey',
    flex: 0.24,
    alignItems: 'stretch',
    backgroundColor: 'rgb(255, 255, 255)',
    height: deviceHeight,

  },
  foodGroup: { backgroundColor: "#c44b2d", marginLeft: 5, marginTop: 5, marginBottom: 5 },
  foodGroupActive: { backgroundColor: "#3eb27e" },
  modal: {
    flex: 1,

    
    padding: 0,
  },
  modalContent: {
    flex: 1,
    height: deviceHeight * 0.75,
    backgroundColor: "white",
    padding: 0,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalHeader: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,

    alignSelf: 'stretch',
    flex: 0.1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "blue",
  },
  modalBody: {
    flex: 0.8,
    flexDirection: 'row'
  },
  modalBodyLeft: {
    flex: 0.5,
    backgroundColor: "green",
  },
  modalBodyRight: {
    flex: 0.5,
    backgroundColor: "red",
  },
  modalFooter: {
    flex: 0.1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "blue",
  },
  modalSmall:{
    width: widthPercentageToDP('50%'),
   // height: heightPercentageToDP('10%'),
    flex: 0.3, backgroundColor: 'white',  borderRadius: 10
  }
});
