import React from 'react';
import { StatusBar } from 'react-native';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text, Card, CardItem } from 'native-base';
class HomeScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
  });

  componentWillMount() {}

  render() {
    return (
    <Container>
      <Header>
        <Left>
          <Button transparent onPress={ () => this.props.navigation.navigate( 'DrawerOpen' ) }>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>
            Home
          </Title>
        </Body>
        <Right />
      </Header>
      <Content padder>
      </Content>
    </Container>
    );
  }
}



export default HomeScreen;
