import React from 'react';
import { StatusBar, StyleSheet, TouchableOpacity, FlatList, RefreshControl, TextInput, Dimensions, View, Alert, Image } from 'react-native';
import { Container, Header, Icon, Title, Left, Right, Button, Body, ListItem, Text, Badge } from 'native-base';
import { getRoomFromServer, getTableFromServer } from '../../networking/TableServer.js'
import { getRecommendationFromServer, GetDiscountFromServer } from '../../networking/Server.js'

import Store from 'react-native-store';
import DiscountBillBanner from '../../components/DiscountBillBanner.js'
import Table from '../../components/Table.js'
import firebase from "react-native-firebase";
import { DB } from '../../lib/constants';
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

var tempTableList = []
var tDis = {
  "id": 5,
  "name": "Christmax",
  "price_requried": 1000,
  "percentage_value": null,
  "value": 100,
  "date_from": "2018-06-13T13:05:12.000Z",
  "date_to": "2019-02-13T17:00:00.000Z",
  "time_from": "2018-06-13T23:15:12.142Z",
  "time_to": "2018-06-14T16:50:12.142Z",
  "enable": true,
  "active": true,
  "discount_style_id": 0,
  "company_id": 1,
  "created_at": "2018-06-14T13:06:17.334Z",
  "updated_at": "2018-06-14T13:06:28.948Z"
}

@inject('userStore', 'notiStore', 'recomStore')
@observer
export default class TableScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableRefreshing: false,

      isFoodModalVisible: false,
      isOrderListVisible: false,
      isNoti: false,
      curRoom: 0,
      numberFood: 0,
      isSearch: false,
      collapsed: true,
      refreshing: true,
      roomDataSource: [],
      tableList: [],
      curTable: 0,
      curOrder: 0,
      orderList: [],
      curOrderList: [],
      discount: null,

      curMovingTable: null,
      tempTable: null,
      nullTable: null,
    };
    const restaurantID = this.props.userStore.GetUserRestaurantID
    this.ref = firebase.firestore().collection(restaurantID);

  }

  componentDidMount() {
    this.setState({
      tempTable: this.creatMatrix(5, 5)
    })

    getRecommendationFromServer().then((item) => {

      if (item.success == true) {
        if (item.data.length > 0) {
          DB.Recommendation.remove().then(() => {
            var temp = JSON.parse(item.data)
            console.log(temp)
            DB.Recommendation.multiAdd(temp)
            this.props.recomStore.storeNew(temp)
          })


        }
      }
    })

    DB.Recommendation.find().then((item) => {
      this.props.recomStore.storeNew(item)
      console.log('data day ne: ' + JSON.stringify(item))
    });

    GetDiscountFromServer().then((item) => {
      this.setState({
        discount: item
      })
    })

    this.unsubscribe = this.ref.onSnapshot((query) => {
      const tOrderArray = []

      query.forEach((item) => {
        var temp = item.data()
        var isPaid = true
        if (temp.isPay == false) {
          isPaid = false
        }
        var tData = {
          order_id: temp.order_id,
          table_id: temp.table_id,
          foodCount: 1,
          isPaid: isPaid,
          time: temp.time
        }
        var isHad = false;
        for (i = 0; i < tOrderArray.length; i++) {
          if (tOrderArray[i].order_id == tData.order_id) {
            tOrderArray[i].foodCount += 1;
            if (tOrderArray[i].time > tData.time) {
              tOrderArray[i].time = tData.time
            }
            if (tOrderArray[i].isPaid == true) {
              tOrderArray[i].isPaid = isPaid
            }

            isHad = true;
            break;
          }
        }
        if (isHad == false) {
          tOrderArray.push(tData)
        }
      })

      this.setState({
        orderList: tOrderArray
      })
    })

    this.refreshRoomDataFromServer();

  }
  refreshRoomDataFromServer = () => {
    this.setState({
      tableRefreshing: true
    });

    getRoomFromServer().then((rooms) => {
      rooms.sort((a, b) => {
        return a.id - b.id
      })
      this.refreshTableDataFromServer(rooms[0]);


      //const allRoom = { "id": 0, "name": "All" }
      //rooms.unshift(allRoom)
      this.setState({
        curRoom: rooms[0].id,
        roomDataSource: rooms,
        tableRefreshing: false,
      });

    }).catch((error) => {
      this.setState({
        curRoom: rooms[0].id,
        roomDataSource: [],
        tableRefreshing: false
      });
    });

  }


  refreshTableDataFromServer = (room) => {

    getTableFromServer(room.id).then((table) => {

      var tArray = this.creatMatrix(5, 5)

      for (var i = 0; i < table.length; i++) {
        const tPos = table[i].column + (table[i].row) * 5
        table[i].room_name = room.name
        tArray[tPos] = table[i]
      }
      this.setState({
        tempTable: tArray
      })
    }).catch((error) => {
    });
  }
  onRefresh = () => {


    this.refreshRoomDataFromServer();

  }

  creatMatrix(rows, cols) {
    defaultValue = { isNull: true }
    var arr = [];
    const tLength = rows * cols
    // Creates all lines:
    for (var i = 0; i < tLength; i++) {
      defaultValue.id = String.fromCharCode(65 + Math.floor(Math.random() * 26)) + Date.now();
      // Creates an empty line
      arr.push({ id: i * -1, isNull: true });

    }
    return arr
  }

  handler = (e, f) => {
    if (e == this.state.curMovingTable) {
      this.setState({
        curMovingTable: null
      })
    } else {
      if (f == 'move') {
        this.setState({
          curMovingTable: e
        })
      }
      else if (f == 1) {
        var temp = this.state.orderList.filter(function (o) { return (o.table_id == e); });
        var tname = this.state.tempTable.find(x => x.id === e).name
        this.props.navigation.navigate('FoodSelect', {
          tableId: e,
          tableName: tname,
          orderId: temp[0].order_id
        })
      }
      else {
        this.props.navigation.navigate('FoodSelect', {
          tableId: e,
          tableName: f,
          orderId: null
        })
      }
    }
  }

  moveOrder(e, f) {
    var batch = firebase.firestore().batch();

    this.ref.where("table_id", "==", this.state.curMovingTable).get()
      .then((querySnapshot) => {
        querySnapshot.forEach(function (doc) {
          batch.update(doc.ref, {
            "table_id": e,
            "table_name": f,
          });

        });

        batch.commit().then(() => {
          Alert.alert("moved")
          this.setState({
            curMovingTable: null
          })
        });
      })
  }
  changeRoom(room) {
    this.setState({
      tableRefreshing: true
    });


    getTableFromServer(room.id).then((table) => {

      var tArray = this.creatMatrix(5, 5)

      for (var i = 0; i < table.length; i++) {
        const tPos = table[i].column + (table[i].row) * 5
        table[i].room_name = room.name
        tArray[tPos] = table[i]
      }
      this.setState({
        tempTable: tArray,
        tableRefreshing: false
      })
    }).catch((error) => {
      this.setState({
        tableRefreshing: false
      })
    });
    this.setState({
      curRoom: room.id,
    })
    console.log(JSON.stringify(this.state.tempTable))
  }


  checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }


  renderRoomItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.changeRoom(item)}>
        <Badge style={[styles.foodGroup, (this.state.curRoom == item.id) && styles.foodGroupActive]}>
          <Text>{item.name}</Text>
        </Badge>
      </TouchableOpacity>

    );
  }
  renderItem = ({ item }) => {


    if ((!item.isNull)) {
      var tOrder = this.state.orderList.filter(function (o) { return (o.table_id == item.id); });
      var tableTime = null
      var tStatus = 'Empty'
      if (tOrder.length > 0) {

        if (tOrder[0].isPaid == true) {

          tStatus = 'Hold'
        }
        else {
          tStatus = 'Eating'
        }
        var tTime = tOrder[0].time
        var h = tTime.getHours();
        var m = tTime.getMinutes();
       
        // add a zero in front of numbers<10
       
        tableTime = {
          hour: this.checkTime(h),
          minute: this.checkTime(m),
         
        }
      }

      if (this.state.curMovingTable == null) {
        return (<Table
          id={item.id}
          name={item.name}
          orderList={tOrder}
          capacity={item.capacity}
          room_id={item.room_id}
          room_name={item.room_name}
          status={tStatus}
          time={tableTime}
          handler={this.handler} />);
      }
      else {
        if (this.state.curMovingTable == item.id) {
          return (<Table
            id={item.id}
            name={item.name}
            orderList={tOrder}
            capacity={item.capacity}
            room_id={item.room_id}
            room_name={item.room_name}
            status={tStatus}
            time={tableTime}
            handler={this.handler} />);
        }
        else {
          if (tStatus != "Empty") {
            return (
              <View style={{ flex: 1 / 5 }}>
                <View style={{ paddingVertical: 10, paddingHorizontal: 10, flexDirection: "row", backgroundColor: 'rgba(0,0,0,0.1)', borderRadius: 10, margin: 6, }}>
                  <View style={{ flex: 0.7 }}>
                    <View style={{ marginBottom: 2 }}>
                      <Text style={[{ color: 'white', fontSize: 22, fontWeight: 'bold' }]}>
                        {item.name}
                      </Text>
                    </View>
                    <View>
                      <Text style={{ color: "white" }}>
                        {item.room_name}
                      </Text>
                      <Text style={{ color: "white" }}>
                        Capacity: {item.capacity}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flex: 0.3, alignItems: 'flex-end', justifyContent: 'center', }}>
                    <View style={styles.center}>
                      <Icon name="md-close-circle" style={{ color: 'white', fontSize: 50 }} />


                    </View>
                  </View>
                </View>
              </View>
            )
          }
          else {
            return (
              <TouchableOpacity style={{ flex: 1 / 5 }} onPress={() => { this.moveOrder(item.id, item.name) }}>
                <View style={{ paddingVertical: 10, paddingHorizontal: 10, flexDirection: "row", backgroundColor: '#82d8af', borderRadius: 10, margin: 6, }}>
                  <View style={{ flex: 0.7 }}>
                    <View style={{ marginBottom: 2 }}>
                      <Text style={[{ color: "white", fontSize: 22, fontWeight: 'bold' }]}>
                        {item.name}
                      </Text>
                    </View>
                    <View>
                      <Text style={{ color: "white" }}>
                        {item.room_name}
                      </Text>
                      <Text style={{ color: "white" }}>
                        Capacity: {item.capacity}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flex: 0.3, alignItems: 'flex-end', justifyContent: 'center', }}>
                    <View style={styles.center}>
                      <Icon name="md-checkbox" style={{ color: '#46af4f', fontSize: 50 }} />


                    </View>
                  </View>
                </View>

              </TouchableOpacity>
            )
          }
        }
      }
    }
    else {
      return (
        <View style={{ flex: 1 / 5 }}>
          <View style={{ paddingVertical: 35, backgroundColor: 'rgba(0,0,0,0.05)', borderRadius: 10, margin: 6, }}>
            <Text style={{ color: 'rgba(0,0,0,0)' }}>{item.id}</Text>
          </View>
        </View>
      )
    }
  }

  renderOrderItem = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={(e) => this.orderPressed(item.order_id)}>
        <View style={styles.listItem}>
          <View style={{ flex: 0.6 }}>
            <Text style={{ color: 'white', fontWeight: 'bold' }}>Order {index + 1}</Text>
          </View>
          <View style={[{ flex: 0.4, flexDirection: 'row' }]}>
            <Text style={[{ color: 'white', fontWeight: 'bold', marginRight: 5, }]}> {item.foodCount}</Text>
            <Icon style={{ alignSelf: 'center', color: 'white', fontSize: 14 }} type={'FontAwesome'} name="cutlery" />
          </View>
        </View>
      </TouchableOpacity>
    );
  }


  render() {
    return (
      <Container>



        {!this.state.curMovingTable &&
          <Header>
            <Left>
              <Button transparent onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                <Icon name="menu" />
              </Button>
            </Left>
            <Body>
              <Title>
                Table
            </Title>
            </Body>
            <Right >

              <Button badge transparent onPress={() => { this.props.notiStore.toggleShow() }}>
                {(this.props.notiStore.countUnread > 0)
                  &&
                  (<Badge style={{ position: 'absolute', elevation: 1, top: 2 }}>
                    <Text style={{ fontWeight: 'bold' }}>{this.props.notiStore.countUnread}</Text>
                  </Badge>)
                }
                <Icon name='md-notifications' />
              </Button>
            </Right>
          </Header>
        }
        {this.state.curMovingTable &&
          <Header>
            <Left>
              <Button transparent onPress={() => this.setState({ curMovingTable: null })}>
                <Icon name="md-close" />
              </Button>
            </Left>
            <Body>
              <Title>
                Select table to move
            </Title>
            </Body>
            <Right >

              <Button badge transparent onPress={() => { this.props.notiStore.toggleShow() }}>
                {(this.props.notiStore.countUnread > 0)
                  &&
                  (<Badge style={{ position: 'absolute', elevation: 1, top: 2 }}>
                    <Text style={{ fontWeight: 'bold' }}>{this.props.notiStore.countUnread}</Text>
                  </Badge>)
                }
                <Icon name='md-notifications' />
              </Button>
            </Right>
          </Header>
        }

        <View style={styles.tagContainer}>
          <FlatList data={this.state.roomDataSource}
            horizontal={true}
            renderItem={this.renderRoomItem}
            keyExtractor={item => item.id.toString()}
            style={{ margin: 0, padding: 0 }} />
        </View>
        <View style={{ padding: 10, paddingHorizontal: 20 }} >
          <Image style={{ position: 'absolute', left: 0, top: 20 }} source={require('../../assets/img/door.png')} />
          <FlatList data={this.state.tempTable}
            renderItem={this.renderItem}
            keyExtractor={(item) => item.id.toString()}
            numColumns={5}
            contentContainerStyle={styles.list}
            refreshControl={<RefreshControl refreshing={this.state.tableRefreshing} onRefresh={this.onRefresh} />}
            style={{ margin: 0, padding: 0 }} />
        </View>
        {(this.state.discount && this.state.discount.active) &&
          <DiscountBillBanner discount={this.state.discount} />
        }

      </Container>
    );
  }
}
TableScreen.navigationOptions = ({ navigation }) => {
  const { params = {} } = navigation.state;

};

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  tagContainer: {
    paddingTop: 5,
    alignSelf: 'stretch', backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: 0.9,
    shadowRadius: 10,
    shadowOffset: {
      height: 0,
      width: 0
    },
    elevation: 2,
  },
  headerLeft: {

  },
  titleLeft: {
    textAlign: 'center',
    alignItems: "center",

  },
  mainContainer: {
    flex: 1,
    flexDirection: 'row',
    height: deviceHeight,
    padding: 0,

  },
  list: {

    justifyContent: 'space-between',
  },
  bigScreen: {
    flex: 0.8
  },
  smallScreen: {
    borderLeftWidth: 1,
    borderLeftColor: 'grey',
    flex: 0.2,
    alignItems: 'stretch',
    backgroundColor: 'rgb(255, 255, 255)',
    height: deviceHeight,

  },
  foodGroup: { backgroundColor: "#c44b2d", marginLeft: 5, marginTop: 5, marginBottom: 5 },
  foodGroupActive: { backgroundColor: "#3eb27e" },
  modal: {
    flex: 0,
    padding: 0,
    zIndex: 1,
  },
  orderListPopUp: {
    zIndex: 5,
    padding: 10,
    width: 150,
    position: 'absolute',
    borderRadius: 10,
    backgroundColor: 'transparent',
  },
  listItem: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: "stretch",
    backgroundColor: '#f73b5d',
    padding: 10,
    borderRadius: 100,
    marginBottom: 5,
    shadowColor: 'black',
    shadowOpacity: 0.9,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5
    },
    elevation: 1,
  },
  textRight: {
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
    textAlign: 'right'
  },
});
