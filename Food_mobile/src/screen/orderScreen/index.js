import React, { Component } from 'react';
import { StatusBar, FlatList, FlatListItem, RefreshControl} from 'react-native';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text, Card, CardItem } from 'native-base';
import OrderData from '../../assets/data/OrderData.js'
import OrderItem from '../../components/OrderItem.js'
import { getOrderFromServer } from '../../networking/Server.js'

export default class OrderScreen extends Component {
  constructor( props ) {
    super( props );
    this.state = {
    	refreshing: false,
      isSearch: false,
      collapsed: true,
      orderDataSource: null
    };
  }
  refreshDataFromServer = () => {
    this.setState( {
      refreshing: true
    } );
    getOrderFromServer().then( (orders) => {
      this.setState( {
        orderDataSource: orders,
        refreshing: false
      } );
    } ).catch( (error) => {
      this.setState( {
        orderDataSource: [],
        refreshing: false
      } );
    } );
  }
  onRefresh = () => {
    this.refreshDataFromServer();
  }
  componentDidMount() {

    this.refreshDataFromServer();
  }

  handler = (e) => {
    this.props.navigation.navigate( 'OrderDetailScreen', {
      orderID: e
    } )
  }
  renderItem = ({item}) => {
    /*<Food name={item.name} price={item.price}/>*/
    return (<OrderItem id={ item.id }
                       tableId={ item.tableId }
                       status={ item.canceled }
                       handler={ this.handler } />);
  }
  render() {
    return (

    <Container>
      <Header>
        <Left>
          <Button transparent onPress={ () => this.props.navigation.navigate( 'DrawerOpen' ) }>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>
            Order
          </Title>
        </Body>
        <Right />
      </Header>
      <Content padder>
        <FlatList data={ this.state.orderDataSource }
                  renderItem={ this.renderItem }
                  keyExtractor={ item => item.id }
                  style={ { margin: 0, padding: 0 } } 
                  refreshControl={ <RefreshControl refreshing={ this.state.refreshing } onRefresh={ this.onRefresh } /> }/>
      </Content>
    </Container>
    );
  }
}

OrderScreen.navigationOptions = ({navigation}) => ({
});
