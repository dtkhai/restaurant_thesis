import React, { Component } from 'react';
import { TouchableOpacity, View, FlatList, FlatListItem } from 'react-native';
import { Item, Input, Container, Header, Title, Left, Icon, Right, Button, Content, List, ListItem, Thumbnail, Text, Body } from 'native-base';
import Collapsible from 'react-native-collapsible';

import OrderData from '../../assets/data/OrderData.js';

import OrderedFood from '../../components/OrderedFood.js'
import { getOrderFromServer, getFoodFromServer } from '../../networking/Server.js'

export default class OrderDetailScreen extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;

    this.state = {
      refreshing: false,
      numberFood: 0,
      isSearch: false,
      collapsed: true,
      foodList: null,
      foodDataSource: null,
      orderDataSource: null,
      orderID: params ? params.orderID : null
    };

    this.handler = this.handler.bind(this)

  }

  handler(e) { }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

  }

  refreshDataFromServer = () => {
    this.setState({
      refreshing: true
    });
    getFoodFromServer().then((foods) => {
      this.setState({
        foodDataSource: foods,
        refreshing: false
      });
    }).catch((error) => {
      this.setState({
        foodDataSource: [],
        refreshing: false
      });
    });
    getOrderFromServer().then((orders) => {
      var temp = orders.find(x => x.id === this.state.orderID)
      this.setState({
        orderDataSource: orders,
        refreshing: false,
        foodList: temp.order_list
      });
    }).catch((error) => {
      this.setState({
        orderDataSource: [],
        refreshing: false
      });
    });
  }
  onRefresh = () => {
    this.refreshDataFromServer();
  }
  componentDidMount() {

    this.refreshDataFromServer();
  }



  renderItem = ({ item }) => {
    var fname;
    var image;
    if (this.state.foodDataSource) {
      var curFood = this.state.foodDataSource.find(x => x.id === item.food_id)
      if (curFood) {
        fname = curFood.name
        image = curFood.main_image
      }
    }
    return (<OrderedFood
      comment={item.comment}
      foodName={fname}
      foodImg={image}
      food_id={item.food_id}
      amount={item.amount}
      handler={this.handler} />);
  }
  _toggleExpanded() {
    numberFood: this.state.numberFood + 1,
      this.setState({
        collapsed: !this.state.collapsed
      });
  }
  ViewCart() { }
  render() {


    return (
      <Container style={{ padding: 0 }}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>
              Ordered Foods {this.state.orderID}
            </Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name="search" />
            </Button>
          </Right>
        </Header>
        <Collapsible collapsed={this.state.collapsed} align="center">
          <Item>
            <Icon active name="search" />
            <Input placeholder="Search" />
            <Button transparent>
              <Text>
                Search
            </Text>
            </Button>
          </Item>
        </Collapsible>
        <Content style={{ padding: 0 }}>
          <FlatList data={this.state.foodList}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
            style={{ margin: 0, padding: 0 }} />
        </Content>
        <Button full
          primary
          style={{ margin: 10 }}
          onPress={() => {
            this.ViewCart()
          }}>
          <Text>
            (
          {this.state.numberFood})
        </Text>
          <Icon name="cart" />
        </Button>
      </Container>
    );
  }
}
