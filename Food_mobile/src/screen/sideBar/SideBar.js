import React from 'react';
import { View, AppRegistry, Image, StatusBar, StyleSheet } from 'react-native';
import { Container, Content, Text, List, ListItem, Left, Right, Thumbnail, Icon } from 'native-base';

const data = [
  /*{
    name: 'TableScreen',
    route: 'TableScreen',
    icon: 'home'
  },  */
  {
    name: 'Log out',
    route:  'Login',
    icon: 'md-log-out'
  },
];

import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";



@inject('userStore')
@observer
export default class SideBar extends React.Component {
  render() {
    return (
      <View>

        <View style={{
          
          height: 150,
          alignSelf: 'stretch',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#3e5fad'
        }}>
          <View style={{
            height: 70, width: 70, borderRadius: 35, backgroundColor: 'white', justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Image source={require('../../assets/img/Avatar.png')} style={{ height: 60, width: 60, borderRadius: 100 }} />
          </View>
          <View style={{ marginLeft: 10 }}>
            <Text style={{ color: 'white' }}>
              {this.props.userStore.user.fname} {this.props.userStore.user.lname}
            </Text>
            <Text >
              {this.props.userStore.user.company.name}
            </Text>
          </View>
        </View>
        <List dataArray={data} renderRow={data => {
          return (
            <ListItem button onPress={() => this.props.navigation.navigate(data.route)}>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: "#777", fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
            </ListItem>
          );
        }} />

      </View>
    );
  }
}


const styles = StyleSheet.create( {
 
  text: {
    fontWeight:  "400",
    fontSize: 16,
    marginLeft: 20
  },
  badgeText: {
    fontSize: 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: -3 
  }
}
)