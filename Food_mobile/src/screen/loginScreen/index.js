
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ImageBackground, Dimensions } from 'react-native';
import { Button, Icon, Header, CheckBox } from 'native-base';
import LoginForm from '../../components/LoginForm.js'
const { width, height } = Dimensions.get('window');

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handlerLogin = this.handlerLogin.bind(this)
  }
  handlerLogin = () => this.props.navigation.navigate('TableScreen')

  render() {

    return (
      <ImageBackground style={styles.container} source={require('../../assets/img/HomeBackground1.jpg')}>

          <View style={styles.titleContainer}>
            
            <Image source={require('../../assets/img/logo.png')}/>
              
          </View>
          <View style={styles.box}  >
            <View style={styles.formContainer}>
              <LoginForm handlerLogin={this.handlerLogin} />

            </View>
          </View>
    </ImageBackground>

        );
      }
    }
    
const styles = StyleSheet.create( {
          container: {
          flex: 1,
   
  
        alignItems: 'center',
        justifyContent: 'center',
      },
  titleContainer: {
         
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    
        
      },
  logo: {
          width: 70,
        height: 70
      },
  title: {
    alignSelf: 'center',
          fontSize: 30,
        fontWeight: 'bold',
        color: '#fff',
        marginTop: 35,
        textAlign: 'center',
 
      },
  box: {
        
        justifyContent: 'center',      
        width: 400
      },
  footer: {
          flex: 4
      }
    } );
    
    export default Login;
