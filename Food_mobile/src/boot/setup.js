import React, { Component } from "react";
import { StyleProvider } from "native-base";

import App from "../App";
import getTheme from "../theme/components";
import variables from "../theme/variables/commonColor";
import { Provider, observer } from "mobx-react";
import {orderStore, userStore, notiStore, recomStore} from "../stores/index";
import { StatusBar } from 'react-native';


@observer
export default class Setup extends Component {
  componentDidMount() {
    StatusBar.setHidden(true);
 }
  render() {
    return (
      <StyleProvider style={getTheme(variables)}>
        <Provider
          orderStore={orderStore}
          userStore={userStore}
          notiStore={notiStore}
          recomStore={recomStore}>
          <App />
        </Provider>
      </StyleProvider>
    );
  }
}
console.ignoredYellowBox = ['Warning:']