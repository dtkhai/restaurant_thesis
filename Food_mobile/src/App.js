import { Root } from 'native-base';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import React, { Component } from 'react';
import HomeScreen from './screen/homeScreen/index.js';
import OrderScreen from './screen/orderScreen/index.js';
import FoodSelect from './screen/foodSelect/index.js';
import TableScreen from './screen/tableScreen/index.js'

import OrderDetailScreen from './screen/orderScreen/OrderDetailScreen.js';
import SideBar from './screen/sideBar/SideBar.js';
import Login from './screen/loginScreen/index.js';

import NotificationList from './components/NotificationList.js'


const Drawer = DrawerNavigator(
  {
    TableScreen: {
      screen: TableScreen
    },
    Home: {
      screen: HomeScreen
    },
    Order: {
      screen: OrderScreen
    },

    Login: {
      screen: Login
    },

  },
  {
    initialRouteName: 'TableScreen',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
    contentComponent: props => <SideBar {...props} />
  }
)


const AppNavigator = StackNavigator(

  {
    Drawer: {
      screen: Drawer
    },
    FoodSelect: {
      screen: FoodSelect
    },
    OrderDetailScreen: {
      screen: OrderDetailScreen
    },
    Login: {
      screen: Login
    },
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
    cardStyle: {backgroundColor: '#eaebed'},
   
  }
);

export default () => <Root> 
  
  <AppNavigator />
  <NotificationList/>
</Root>;
