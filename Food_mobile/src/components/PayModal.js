import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Text, Image, FlatList, CheckBox, TextInput, Picker, Alert } from 'react-native';
import { Icon } from 'native-base';
import styles from './styles';
import { getFoodFromServer, AddOrderToServer } from '../networking/Server.js'
import FoodPayment from './FoodPayment.js'
import CashPayForm from './CashPayForm.js'
import CardPayForm from './CardPayForm.js'
import QRPayForm from './QRPayForm.js'
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
import firebase from "react-native-firebase";


@inject('userStore', 'orderStore')
@observer
export default class PayModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			numText: null,
			comment: null,

			totalPrice: 0,
			subTotal: 0,
			payMethod: 1,
			exportChecked: false,
			allFoods: [],
			foods: [],
			billList: [
				{
					billId: 1,
					order_id: this.props.orderId,
				}
			],
			curBill: 0,
			billNum: 1,
			isSpliting: false,
			splitetype: 1,
			moneyLeft: 0,
			discountAmount: 0,
			tax: 0,
			lastIndexBill :0,
			isAppliedBillDiscount: null
		};
		const restaurantID = this.props.userStore.GetUserRestaurantID
		this.ref = firebase.firestore().collection(restaurantID);


	}
	componentWillMount() {


	}
	getOrderedFood() {
		this.unsubscribe = this.ref.where("order_id", "==", this.props.orderId).where("isPay", "==", false).onSnapshot((query) => {
			const tArray = []
			const tAllFood = []
			const tCombo = []
			query.forEach((item) => {
				var temp = item.data()
				if ((temp.status == 4) || (temp.status == 5)) {
				}
				else {

					var isFHad = false;
					var tNewFood = {
						"food_id": temp.food_id,
						"amount": temp.amount
					}
					tAllFood.forEach((item) => {
						if (item.food_id === tNewFood.food_id) {
							item.amount = item.amount + tNewFood.amount
							isFHad = true;

						}
					}
					)
					if (!isFHad) {
						tAllFood.push(tNewFood)
					}
					if (temp.combo == null) {
						var isHad = false;
						var index = 0;
						for (i = 0; i < tArray.length; i++) {
							if (tArray[i].food_id === temp.food_id) {
								console.log("ruoc do: " + JSON.stringify(temp.amount) + "va " + tArray[i].amount)
								tArray[i].amount = tArray[i].amount + temp.amount
								console.log("sau do: " + tArray[i].amount)
								isHad = true;
								index = i;
								break;
							}
						}
						if (!isHad) {
							tArray.push(item.data())
						}
					}
					else {
						var curCombo = this.props.comboDataSource.find(x => x.id === temp.combo)
						if (temp.food_id == curCombo.foods[0].id) {
							var tempCom = {

								"combo": temp.combo,
								"name": curCombo.name,
								"amount": temp.amount,
								"comment": temp.comment
							}
							var notFound = true
							for (var i = 0; i < tCombo.length; i++) {
								if (tempCom.id == tCombo[i].id) {
									tCombo[i].amount += temp.amount
									notFound = false
								}
							}
							if (notFound) {
								tCombo.push(tempCom)
							}
						}

					}
				}
			})
			console.log("Dayla list ordered: " + JSON.stringify(tAllFood))
			console.log("Dayla list ordered2 : " + JSON.stringify(tArray))
			this.setState({
				allFoods: tAllFood,
				foods: tArray.concat(tCombo)
			}, () => { this.initialBill() })
		})
	}
	componentDidMount() {
		this.getOrderedFood()
	}

	initialBill() {

		const temp = [
			{
				billId: 1,
				order_id: this.props.orderId,
				grand_Amount: this.calculateTotal(),
			}
		]
		this.setState({
			billList: temp
		})
	}


	showDetail() {
		this.props.handlerModal(this.props.id);
	}

	calculateTotal = () => {
		const { billDiscount } = this.props
		var isBillDiscount = false;
		var total = 0;
		var rawTotal = 0;
		this.state.foods.forEach((l) => {
			if (l.combo == null) {
				const temp = this.props.foodDataSource.find(x => x.id === l.food_id)
				if (temp) {
					const tPercent = temp.Discount ? temp.Discount.percentage_value : 0
					const tValue = temp.Discount ? temp.Discount.value : 0

					const price = temp.price - tPercent * temp.price / 100 - tValue

					rawTotal = rawTotal + temp.price * l.amount
					total = total + price * l.amount;

				}
			}
			else {
				const temp = this.props.comboDataSource.find(x => x.id === l.combo)
				if (temp) {
					const tPercent = temp.Discount ? temp.Discount.percentage_value : 0
					const tValue = temp.Discount ? temp.Discount.value : 0
					const price = temp.price - tPercent * temp.price / 100 - tValue

					rawTotal = rawTotal + temp.price * l.amount
					total = total + price * l.amount;

				}

			}
		});
		total = Math.round(total * 100) / 100
		var tDiscount = 0

		if (billDiscount) {
			if (billDiscount.price_requried < rawTotal) {
				tDiscount = billDiscount.value + billDiscount.percentage_value * total / 100
				tDiscount = Math.round(tDiscount * 100) / 100
				if (this.state.isAppliedBillDiscount == null) {
					this.setState({
						isAppliedBillDiscount: true
					})
					isBillDiscount = true
				}
			} else {
				tDiscount = 0
				if (this.state.isAppliedBillDiscount == null) {
					this.setState({
						isAppliedBillDiscount: false
					})
					isBillDiscount = false
				}
			}
		} else {
			if (this.state.isAppliedBillDiscount == null) {
				this.setState({
					isAppliedBillDiscount: false
				})
			}
			isBillDiscount = false
		}
		if (this.state.isAppliedBillDiscount != null) {
			isBillDiscount = this.state.isAppliedBillDiscount
		}
		if (isBillDiscount == true) {
			this.setState({
				tax: Math.round((rawTotal * (10 / 100)) * 100) / 100,
				discountAmount: tDiscount,
				subTotal: rawTotal,
				totalPrice: Math.round((rawTotal + rawTotal * (10 / 100) - tDiscount) * 100) / 100
			});
		} else {
			this.setState({
				tax: Math.round((total * (10 / 100)) * 100) / 100,
				discountAmount: tDiscount,
				subTotal: total,
				totalPrice: Math.round((total + total * (10 / 100)) * 100) / 100
			});
		}
		return total
	}
	onChanged(text) {
		var temp
		if (text == null) {
			temp = 0
		} else {
			let newText = '';
			let numbers = '0123456789';

			for (var i = 0; i < text.length; i++) {
				if (numbers.indexOf(text[i]) > -1) {
					newText = newText + text[i];
				}
				else {

				}
			}
			temp = parseInt(newText, 10);
		}

		this.setState({ numText: temp });
	}
	editBillSplitForm() {
		if (this.state.splitetype == 1) {
			var tBillList = []
			var tGrandAmount = Math.round((this.state.totalPrice / this.state.billNum) * 10) / 10

			for (i = 0; i < this.state.billNum; i++) {
				var tNewBill = {
					billId: i + 1,
					order_id: this.props.orderId,
					grand_Amount: tGrandAmount
				}
				tBillList.push(tNewBill)
			}

			this.setState({
				billList: tBillList,
				moneyLeft: 0
			})
		}
		else if (this.state.splitetype == 2) {
			var tBillList = []
			for (i = 0; i < this.state.billNum; i++) {
				var tNewBill = {
					billId: i + 1,
					order_id: this.props.orderId,
					grand_Amount: 0
				}
				tBillList.push(tNewBill)
			}

			this.setState({
				billList: tBillList,
				moneyLeft: this.state.totalPrice
			})
		}

	}
	saveBillSplit() {
		var tBillList = this.state.billList
		const maxId = Math.max(...tBillList.map(o => o.billId), 0);
		var tNewBill = {
			billId: maxId + 1,
			order_id: this.props.orderId,
			grand_Amount: this.state.numText
		}
		tBillList[this.state.curBill].grand_Amount = tBillList[this.state.curBill].grand_Amount - this.state.numText
		tBillList.push(tNewBill)
		/*for (i = 0; i < tBillList.length; i++) {
			if (tBillList[i].order_id == tData.order_id) {
				tOrderArray[i].foodCount += 1
				isHad = true;
				break;
			}
		}*/

		this.setState({
			isSpliting: !this.state.isSpliting,
			numText: 0,
			billList: tBillList
		})
	}

	confirmPayment() {
		if (this.state.moneyLeft < 0) {
			Alert.alert('Error', '$' + (-this.state.moneyLeft + this.state.totalPrice) + ' exceed the total ' + '$' + (this.state.totalPrice))
		}
		else if (this.state.moneyLeft > 0) {
			Alert.alert('Error', '$' + (-this.state.moneyLeft + this.state.totalPrice) + ' are below the total ' + '$' + (this.state.totalPrice))
		}
		var upOrder = {
			"order_list": [],
			"order_parent": null,
			"total_price": this.state.totalPrice,
			"canceled": false
		}
		var tFoods = this.state.allFoods
		

		upOrder.order_list = tFoods
		var batch = firebase.firestore().batch();
		const restaurantID = this.props.userStore.GetUserRestaurantID
		this.ref.where("order_id", "==", this.props.orderId).get()
			.then((querySnapshot) => {
				querySnapshot.forEach(function (doc) {
					batch.update(doc.ref, { "isPay": true });

				});

				batch.commit().then(() => {
					this.props.handlerModal(-1);
				});
			})


		AddOrderToServer(upOrder).then((success) => {
			console.log(JSON.stringify(success))
			if (success === true) {
				Alert.alert('ok')
			}
		})
	}

	renderItemSide = ({ item, index }) => {

		var fname;
		var fprice;
		if (item.combo) {
			if (this.props.comboDataSource) {
				var curFood = this.props.comboDataSource.find(x => x.id === item.combo)
				if (curFood) {
					fname = curFood.name
					if (this.state.isAppliedBillDiscount) {
						fprice = curFood.price
					} else {
						const tPercent = curFood.Discount ? curFood.Discount.percentage_value : 0
						const tValue = curFood.Discount ? curFood.Discount.value : 0
						fprice = curFood.price - tPercent * curFood.price / 100 - tValue
					}

					fTotalPrice = fprice * item.amount
					fTotalPrice = Math.round(fTotalPrice * 100) / 100
				}
			}
		} else {
			if (this.props.foodDataSource) {
				var curFood = this.props.foodDataSource.find(x => x.id === item.food_id)
				if (curFood) {
					fname = curFood.name
					if (this.state.isAppliedBillDiscount) {
						fprice = curFood.price
					} else {
						const tPercent = curFood.Discount ? curFood.Discount.percentage_value : 0
						const tValue = curFood.Discount ? curFood.Discount.value : 0
						fprice = curFood.price - tPercent * curFood.price / 100 - tValue
					}
					fTotalPrice = fprice * item.amount
					fTotalPrice = Math.round(fTotalPrice * 100) / 100
				}
			}
		}
		return (<FoodPayment
			index={index}

			foodName={item.name}
			price={fprice}
			comment={item.comment}
			amount={item.amount}
			totalPrice={fTotalPrice}
		/>);
	}
	renderPayForm() {
		if (this.state.payMethod == 1) {
			return (
				<View totalPrice={this.state.totalPrice} />
			);
		}
		else if (this.state.payMethod == 2) {
			return (
				<CardPayForm />
			);
		}
		else {
			return (
				<QRPayForm />
			);
		}
	}

	increaseBill() {
		if (this.state.billNum < 6) {
			this.setState({
				billNum: this.state.billNum + 1
			}, () => { this.editBillSplitForm() }
			)
		}

	}

	decreaseBill() {
		if (this.state.billNum > 1) {
			this.setState({
				billNum: this.state.billNum - 1
			}, () => { this.editBillSplitForm() }
			)
		}

	}

	onSpliteTypeChange(itemValue) {
		if (itemValue == 0) {
			this.toggleSplitForm();
		}
		else
			this.setState({ splitetype: itemValue }, () => { this.editBillSplitForm() })

	}

	calMoneyLeft() {
		
		var tLeft = this.state.totalPrice
		for (var i = 0; i < this.state.billList.length; i++) {
			if (this.state.billList[i].grand_Amount) {
				tLeft = tLeft - this.state.billList[i].grand_Amount
			}
			
		}
		
		this.setState({
			moneyLeft: tLeft,
		})
	}
	autoFillBill(){
		
		var numberOfZero = 0
		var tIndex = -1
		for (var i = 0; i < this.state.billList.length; i++) {
			if ((this.state.billList[i].grand_Amount == 0)||(this.state.billList[i].grand_Amount ==null)) {
				
				numberOfZero +=1
				
				tIndex = i
			}
		}
		if((numberOfZero==1)&&(this.state.moneyLeft>=0)){
			
			var tBillList = this.state.billList
			tBillList[tIndex].grand_Amount = this.state.moneyLeft
			this.setState({
				billList: tBillList,
				moneyLeft: 0
			})
		}
	}
	onChangeBillValue(receiveCash, index) {
		var temp
		if (receiveCash == null) {
			temp = 0
		} else {

			temp = parseInt(receiveCash, 10);
		}

		var tBillList = this.state.billList
		if(temp){
		tBillList[index].grand_Amount = temp
		}else{
			tBillList[index].grand_Amount = 0
		}

		this.setState({
			billList: tBillList,
		}, () => { this.calMoneyLeft() })
	}

	renderBill = ({ item, index }) => {
		return (
			<TouchableOpacity
				style={[styles.billHeader]}
				onPress={() => this.setState({ curBill: index })}
			>
				<View style={[{ flex: 0.4 }]}>
					<Text style={styles.bold}>Bill {item.billId}</Text>
				</View>
				<View style={[{ flex: 0.6 }]}>
					<TextInput
						style={[styles.textRight, styles.bold, { color: '#5E5E5E' }]}
						underlineColorAndroid="#d6d7da"
						keyboardType='numeric'
						placeholder={JSON.stringify(item.grand_Amount)}
						onEndEditing ={()=>this.autoFillBill()}
						onChangeText={(receiveCash) => this.onChangeBillValue(receiveCash, index)}
						value={item.grand_Amount ? JSON.stringify(item.grand_Amount) : '0'}
					/>
				</View>
			</TouchableOpacity>
		)
	}

	renderSpliteBill() {
		if (this.state.splitetype == 1) {
			return (<View style={[styles.center, { flex: 1, alignSelf: 'stretch' }]}>
				<Text style={[styles.bold, { fontSize: 18 }]}>${this.state.billList[0].grand_Amount}/person</Text>
			</View>)
		} else if (this.state.splitetype == 2) {
			return (
				<View style={{ flex: 1 }}>
					<View style={{ flex: 0.8 }}>
						<FlatList data={this.state.billList}
							numColumns={3}
							renderItem={this.renderBill}
							keyExtractor={item => item.billId}
							style={{ margin: 0, padding: 0 }} />
					</View>
					<View style={{ flex: 0.2, justifyContent: 'flex-end' }}>
						<Text style={[{ color: '#5E5E5E' }, (this.state.moneyLeft > 0) && { color: 'red' }]}>${this.state.moneyLeft} Left</Text>
					</View>
				</View>
			)
		}
	}

	toggleSplitForm() {
		this.setState({
			isSpliting: !this.state.isSpliting,
		})
	}

	renderSplitePart() {
		if (!this.state.isSpliting) {
			return (
				<View style={{ flex: 1, alignSelf: 'stretch', justifyContent: 'flex-end', alignItem: 'flex-end' }}>
					<TouchableOpacity style={[styles.buttonSplite, styles.dropShadow, { height: 40, width: 120 }]}
						onPress={() => { this.toggleSplitForm() }} >
						<Text style={[{ fontWeight: 'bold' }]}>
							Splite Bill
                                      </Text>
					</TouchableOpacity>
				</View>
			)
		}
		else
			return (
				<View style={{ flex: 1 }}>
					<View style={[styles.horizontal, { padding: 5, justifyContent: 'center' }]}>
						<Picker
							selectedValue={this.state.splitetype}
							style={{ height: 36, width: 180 }}
							mode='dropdown'
							onValueChange={(itemValue, itemIndex) => this.onSpliteTypeChange(itemValue)}>
							<Picker.Item label="No Split" value={0} />
							<Picker.Item label="Split Equally" value={1} />
							<Picker.Item label="Split Unequal" value={2} />
						</Picker>
						<View style={[styles.buttonGroup1, styles.horizontal, styles.dropShadow]}>

							<TouchableOpacity elevation={2} style={[styles.circleButton]} onPress={() => {
								this.decreaseBill()
							}}>
								<Icon style={{ alignSelf: 'center', color: "#5A7587", fontSize: 20 }} type={'Entypo'} name="minus" />
							</TouchableOpacity>
							<View style={[styles.center, { alignSelf: 'center', width: 30 }]}>
								<Text style={[styles.amountText, styles.center, { alignSelf: 'center' }]}>
									{this.state.billNum}
								</Text>
							</View>
							<TouchableOpacity elevation={2} style={[styles.circleButton]} onPress={() => {
								this.increaseBill()
							}}>
								<Icon style={{ alignSelf: 'center', color: "#5A7587", fontSize: 20 }} type={'Entypo'} name="plus" />
							</TouchableOpacity>
						</View>

					</View>
					<View style={[styles.billContainer, { flex: 1 }]} >
						{this.renderSpliteBill()}
					</View>
				</View>
			)
	}
	renderDiscount() {
		if (this.state.isAppliedBillDiscount) {
			return (
				<View style={styles.foodLabel}>
					<View style={{ flex: 0.7 }}>
						<Text style={[styles.bold, { fontSize: 14 }]}>
							Discount
							            </Text>
					</View>
					<View style={[{ flexDirection: 'row', flex: 0.3 }]}>
					<TouchableOpacity style={styles.buttonDiscountGroup} onPress={() => { this.setState({ isAppliedBillDiscount: false }, () => this.calculateTotal()) }}>
						<View style={{flex: 0.7,justifyContent: 'flex-end'}}>
						<Text style={[styles.textRight, styles.bold, { fontSize: 14,alignSelf: 'flex-end' }]} note>
							{this.state.discountAmount} $
							            </Text>
										</View>
						<View style={{alignItems: 'flex-end',flex: 0.3}}>
						<View style={{borderWidth: 1,aspectRatio: 1}}>
						<Icon style={[styles.iconDefault, { fontSize: 12, }]} type={'FontAwesome'} name="minus" />
							</View>
						</View>
						</TouchableOpacity>
					</View>
				</View>
			)
		}
		else {
			return (
				<View style={styles.foodLabel}>
					<View style={{ flex: 0.7 }}>
						<Text style={[styles.bold, { fontSize: 14 }]}>
							Discount
											</Text>
					</View>
					<View  style={[{ flexDirection: 'row', flex: 0.3 }]}>
					<TouchableOpacity style={styles.buttonDiscountGroup} onPress={() => { this.setState({ isAppliedBillDiscount: true }, () => this.calculateTotal())}}>
						<View style={{flex: 0.7,justifyContent: 'flex-end'}}>
						<Text style={[styles.textRight, styles.bold, { fontSize: 14,alignSelf: 'flex-end' }]} note>
						Enable
							            </Text>
										</View>
						<View style={{alignItems: 'flex-end',flex: 0.3}}>
						<View style={{borderWidth: 1,aspectRatio: 1}}>
						<Icon style={[styles.iconDefault, { fontSize: 12, }]} type={'FontAwesome'} name="plus" />
							</View>
						</View>
						</TouchableOpacity>
					
					</View>
				</View>
			)
		}
	}
	render() {

		return (
			<View style={[styles.modalContent]}>
				<View style={[styles.modalBody, { flex: 1, }]}>
					<View style={[styles.modalLeft, { flex: 0.5, backgroundColor: 'white', borderRadius: 10, paddingLeft: 10, paddingRight: 10, }]}>
						<View style={[{ flex: 0.15 }, styles.center,]}>
							<Text style={[styles.bold, styles.center, { fontSize: 18 }]}>
								Sale Summary
							    </Text>
							<Text style={[styles.center, { fontSize: 14 }]}>
								Cashier: {this.props.userStore.user.fname}
							</Text>
						</View>
						<View style={[styles.horizontal, { borderBottomWidth: 0.5, marginHorizontal: 5 }]}>
							<View style={[{ justifyContent: 'center', flex: 0.1 }]}>
								<Text style={{ fontWeight: "bold" }}>
									No
								</Text>
							</View>
							<View style={[{ justifyContent: 'center', flex: 0.35 }]}>
								<Text style={{ fontWeight: "bold" }}>
									Name
								</Text>
							</View>
							<View style={[{ justifyContent: 'center', flex: 0.2 }]}>
								<Text style={{ fontWeight: "bold" }}>
									Unit price
		          				</Text>
							</View>
							<View style={[{ justifyContent: 'center', flex: 0.15, padding: 5 }]}>
								<Text style={{ fontWeight: "bold" }}>
									Qties
								</Text>
							</View>
							<View style={[{ justifyContent: 'center', flex: 0.2, padding: 5 }]}>
								<Text style={[styles.textRight, { fontWeight: "bold" }]} >
									Amount
			          			</Text>
							</View>
						</View>
						<View style={{ flex: 0.6 }}>
							<FlatList data={this.state.foods}
								extraData={this.state.foods}
								renderItem={this.renderItemSide}
								keyExtractor={(item, index) => index}
								style={{ margin: 0, padding: 0 }} />
						</View>
						<View style={[{ flex: 0.2, padding: 6 }, styles.borderTop]}>
							<View style={styles.foodLabel}>
								<View style={{ flex: 0.7 }}>
									<Text style={[styles.bold, { fontSize: 14 }]}>
										Sub Total
							            </Text>
								</View>
								<View style={[{ flex: 0.3 }]}>
									<Text style={[styles.textRight, styles.bold, { fontSize: 14 }]} note>
										{this.state.subTotal} $
							            </Text>
								</View>
							</View>
							<View style={styles.foodLabel}>
								<View style={{ flex: 0.7 }}>
									<Text style={[styles.bold, { fontSize: 14 }]}>
										VAT Tax (10%)
							            </Text>
								</View>
								<View style={[{ flex: 0.3 }]}>
									<Text style={[styles.textRight, styles.bold, { fontSize: 14 }]} note>
										{this.state.tax} $
							            </Text>
								</View>
							</View>
							{(this.state.discountAmount > 0) && this.renderDiscount()

							}
							<View style={styles.foodLabel}>
								<View style={{ flex: 0.7 }}>
									<Text style={[styles.bold, { fontSize: 18 }]}>
										Total
							            </Text>
								</View>
								<View style={[{ flex: 0.3 }]}>
									<Text style={[styles.textRight, styles.bold, { fontSize: 18 }]} note>
										{this.state.totalPrice} $
							            </Text>
								</View>
							</View>
						</View>
					</View>

					<View style={[styles.modalRight, { flex: 0.5, paddingHorizontal: 20, marginRight: 10, }]}>
						<View style={{ flex: 0.9, justifyContent: 'space-between', }}>
							<View style={[styles.center, { flex: 0.2, }]}>
								<Text style={[styles.bold, { fontSize: 24, marginBottom: 10, }]}>
									PAYMENT METHOD
							    </Text>
								<View style={styles.segmentGroup}>
									<TouchableOpacity style={[styles.segmentTab, styles.horizontal, (this.state.payMethod == 1) && styles.segmentActive]} onPress={() => this.setState({ payMethod: 1 })}>
										<Icon style={[styles.iconDefault, { fontSize: 20 }, (this.state.payMethod == 1) && { color: 'white' }]} type={'FontAwesome'} name="money" />
										<Text style={[styles.bold, { fontSize: 18, marginLeft: 5 }, , (this.state.payMethod == 1) && { color: 'white' }]} >CASH</Text>
									</TouchableOpacity>

									<TouchableOpacity style={[styles.segmentTab, styles.horizontal, (this.state.payMethod == 3) && styles.segmentActive]} onPress={() => this.setState({ payMethod: 3 })}>
										<Icon style={[styles.iconDefault, { fontSize: 20 }, (this.state.payMethod == 3) && { color: 'white' }]} type={'FontAwesome'} name="qrcode" />
										<Text style={[styles.bold, { fontSize: 18, marginLeft: 5 }, (this.state.payMethod == 3) && { color: 'white' }]}>QR</Text>
									</TouchableOpacity>
								</View>
							</View>
							<View style={{ flex: 0.3 }}>
								{this.renderPayForm()}
							</View >
							<View style={{ flex: 0.3 }}>
								{this.renderSplitePart()}
							</View>
							<View style={[{ flex: 0.1, alignItem: 'center' }, styles.horizontal, styles.borderTop]}>
								<CheckBox
									style={{ alignSelf: 'center' }}
									value={this.state.exportChecked}
									onValueChange={() => this.setState({ exportChecked: !this.state.exportChecked })}
								/>
								<Text style={{ alignSelf: 'center', fontSize: 18 }}> Print Bill </Text>
							</View>


						</View>
						<View style={[styles.modalFooter]}>
							<TouchableOpacity style={[styles.modalButton2]} onPress={() => { this.showDetail() }}>
								<Text style={[styles.focusText, styles.white]}>BACK</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.modalButton} onPress={() => { this.confirmPayment() }}>
								<Text style={[styles.focusText, styles.white]}>CONFIRM</Text>
							</TouchableOpacity>

						</View>
					</View>
				</View>
			</View>
		);
	}
}