import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';

export default class DiscountBanner extends Component {

    render() {
        if(this.props.discount.percentage_value){
     return(
        <ImageBackground style={styles.discountImg}  source={require('../assets/img/Discount.png')} >
                <Text style={{fontWeight: 'bold',color: 'white',fontSize: 18}}>{this.props.discount.percentage_value}%</Text>
                <Text style={{fontWeight: 'bold',color: 'yellow', fontSize: 12}}>OFF</Text>
                </ImageBackground>
     )
    }
    else if(this.props.discount.value){
        return(
            <ImageBackground style={styles.discountImg}  source={require('../assets/img/Discount.png')} >
            <Text style={{fontWeight: 'bold',color: 'yellow', fontSize: 12}}>SAVE</Text>
                    <Text style={{fontWeight: 'bold',color: 'white',fontSize: 18}}>${this.props.discount.value}</Text>
                    
                    </ImageBackground>
         )
    }
    }
}