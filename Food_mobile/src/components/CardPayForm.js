import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet,Text, TextInput} from 'react-native';
import styles from './styles';

export default class CardPayForm extends Component {

  render() {
  	return (
  		<View style={{padding:10,paddingLeft:40,paddingRight: 40,}}>
        <Text style={ [ styles.bold, { fontSize: 18 } ] }>
                              Credit Card Details
                          </Text>
        <TextInput  style={[styles.formInput2,]}
                    underlineColorAndroid="transparent"          
                    placeholder='Name'
                    onChangeText={(cardName) => this.setState({cardName})}
                    value={this.state.cardName}/>  
        <TextInput
                    style={[styles.formInput2,]}
                    underlineColorAndroid="transparent"                  
                    keyboardType='numeric'
                    placeholder='Card Number'
                    onChangeText={(cardNum) => this.setState({cardNum})}
                    value={this.state.cardNum}/>
        <View style={styles.horizontal}>
          <TextInput
                      style={[styles.formInput2, {flex: 0.2}]}
                      underlineColorAndroid="transparent"                  
                      keyboardType='numeric'
                      placeholder='MM'
                      onChangeText={(cardMonth) => this.setState({cardMonth})}
                      value={this.state.cardMonth}/>
          <TextInput
                      style={[styles.formInput2, {flex: 0.3,marginLeft:10,marginRight: 10}]}
                      underlineColorAndroid="transparent"                  
                      keyboardType='numeric'
                      placeholder='YYYY'
                      onChangeText={(cardYear) => this.setState({cardYear})}
                      value={this.state.cardYear}/>            
          <TextInput
                      style={[styles.formInput2,{flex: 0.5}]}
                      underlineColorAndroid="transparent"                  
                      keyboardType='numeric'
                      placeholder='CSV'
                      onChangeText={(cardCSV) => this.setState({cardCSV})}
                      value={this.state.cardCSV}/>    
        </View>            
      </View>
  	)  	
  }
}

