import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Text, Image, FlatList, TextInput, Alert } from 'react-native';
import { Button } from 'native-base';
import styles from './styles';
import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/Entypo';
import FoodCheckItem from './FoodCheckItem.js'
import FoodDetailForm from './FoodDetailForm.js'
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
import { getFoodFromServer } from '../networking/Server.js'
import firebase from "react-native-firebase"
@inject('userStore', 'orderStore')
@observer

export default class FoodListModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			numText: null,
			comment: null,
			foodDataSource: null,
			foods: [],
			curFoodIndex: -1,
			tempCurFood: null,
			isLoadedFood: false,
			isLoaded: false
		};
		const restaurantID = this.props.userStore.GetUserRestaurantID
		this.ref = firebase.firestore().collection(restaurantID);

		this.handler = this.handler.bind(this)
		this.handlerFoodItem = this.handlerFoodItem.bind(this)

	}
	refreshDataFromServer = () => {
		getFoodFromServer().then((foods) => {
			this.setState({
				foodDataSource: foods,
				isLoadedFood: true
			});
			this.calculateTotal()
		}).catch((error) => {
			this.setState({
				foodDataSource: [],

			});
		});

	}
	componentWillMount() {
		this.unsubscribe = this.ref.where("order_id", "==", this.props.orderId).onSnapshot((query) => {
			const tArray = []
			query.forEach((item) => {
				tArray.push(item.data())
			})

			this.setState({
				foods: tArray,
				isLoaded: true
			})
			this.calculateTotal()
		})
		this.refreshDataFromServer();

	}
	componentDidMount() {
		this.calculateTotal()

	}
	calculateTotal = () => {
		if (this.state.isLoadedFood && this.state.isLoaded) {
			var total = 0;
			this.state.foods.forEach((l) => {
				var price = this.state.foodDataSource.find(x => x.id === l.food_id).price
				total = total + price * l.amount;

			});
			this.setState({
				totalPrice: total
			});
		}
	}

	handler(e, f) {
		if (e == 'num') {
			var temp = this.state.tempCurFood
			temp.amount = f
			this.setState({
				tempCurFood: temp
			})
		}
		else if (e == 'comment') {
			var temp = this.state.tempCurFood
			temp.note = f
			this.setState({
				tempCurFood: temp
			})
		}
		this.calculateTotal()
	}
	handlerFoodItem(e, f) {
		if (f == 'done') {

			this.ref.doc(e).update({
				status: 3
			})
		}
		else if (f == 'clear') {
			Alert.alert(
				'Confirm',
				'Do you really want to cancel',
				[
					{ text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
					{ text: 'OK', onPress: () => this.ref.doc(e).delete() },
				],
				{ cancelable: false }
			)

		}
		this.calculateTotal()
	}
	showDetail() {
		this.props.handlerModal(this.props.id);
	}
	saveDetail() {
		var tId = this.state.tempCurFood.id
		this.ref.doc(tId).update(this.state.tempCurFood).then(() => {
			Alert.alert(
				'Notification',
				'Successfully updated',

			)
		})
	}

	changeCurFoodIndex(index) {
		if (this.state.curFoodIndex != index) {
			this.setState({
				curFoodIndex: index,
				tempCurFood: this.state.foods[index]
			})
		}
		else {
			this.setState({
				curFoodIndex: -1,
				tempCurFood: null
			})
		}
	}

	renderItemSide = ({ item, index }) => {
		var fname;
		var image;
		var fprice;
		if (this.state.foodDataSource) {
			var curFood = this.state.foodDataSource.find(x => x.id === item.food_id)
			if (curFood) {
				fname = curFood.name
				image = curFood.main_image
				fprice = curFood.price
			}
		}
		return (
			<TouchableOpacity style={(this.state.curFoodIndex == index) && styles.active} onPress={() => this.changeCurFoodIndex(index)}>
				<FoodCheckItem
					id={item.id}
					food_id={item.food_id}
					foodName={fname}
					foodImg={image}
					price={fprice}
					status={item.status}
					comment={item.comment}
					amount={item.amount}
					handlerFoodItem={this.handlerFoodItem}
				/>
			</TouchableOpacity>
		);
	}

	renderModalRight() {
		var tPrice = 0
		var tDescription = ''
		if (this.state.isLoadedFood && this.state.isLoaded && (this.state.curFoodIndex > -1)) {
			tDescription = this.state.foodDataSource.find(x => x.id === this.state.tempCurFood.food_id).description
			tPrice = this.state.foodDataSource.find(x => x.id === this.state.tempCurFood.food_id).price
		}
		if (this.state.isLoaded && (this.state.curFoodIndex > -1)) {
			return (<View style={[styles.modalRight, { paddingLeft: 20, paddingTop: 0, }]}>
				<FoodDetailForm handler={this.handler}
					numText={this.state.tempCurFood.amount}
					comment={this.state.tempCurFood.note}
					name={this.state.tempCurFood.name}
					price={tPrice}
					description={tDescription}
					isNotEditable={!this.state.tempCurFood.status == 0}
				/>
				<View style={styles.modalFooter}>
					<Button style={styles.modalButton} Full onPress={() => { this.saveDetail() }}>
						<Text style={[styles.focusText, styles.white]}>SAVE</Text>
					</Button>
				</View>
			</View>)
		}
	}

	render() {
		return (
			<View style={[styles.modalContent, (this.state.curFoodIndex == -1) && { aspectRatio: 0.8 }]}>
				<View style={[styles.modalBody, { flex: 1 }]}>

					<View
						style={[styles.modalLeft,
						{ flex: 0.5, backgroundColor: 'white', borderRadius: 10, paddingLeft: 10, paddingRight: 10, },
						(this.state.curFoodIndex == -1) && { flex: 1 }
						]}>
						<View style={[{ flex: 0.15 }, styles.center,]}>
							<Text style={[styles.bold, styles.center, { fontSize: 18 }]}>
								Table {this.props.tableId}
							</Text>
						</View>
						<View style={{ flex: 0.7 }}>
							<FlatList data={this.state.foods}
								renderItem={this.renderItemSide}
								keyExtractor={item => item.id}
								style={{ margin: 0, padding: 0 }} />
						</View>
						<View style={[{ flex: 0.15, padding: 10 }, styles.borderTop]}>
							<View style={styles.foodLabel}>
								<View style={{ flex: 0.7 }}>
									<Text style={[styles.bold, { fontSize: 18 }]}>
										Total
							            </Text>
								</View>
								<View style={[{ flex: 0.3 }]}>
									<Text style={[styles.textRight, styles.bold, { fontSize: 18 }]} note>
										{this.state.totalPrice} $
							            </Text>
								</View>
							</View>
						</View>
					</View>
					{this.renderModalRight()}
				</View>
			</View>
		);
	}
}