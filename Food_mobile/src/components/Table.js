import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { } from 'native-base';
import styles from './styles';
import Collapsible from 'react-native-collapsible';
export default class Table extends Component {
  constructor(props) {
    super(props);
  
  }

  componentDidMount() {
    
    
       
    

  }

  


  MoveOnpress(){
    if(this.props.status=='Eating'){
      this.props.handler(this.props.id, 'move');
    }
  }
  OrderOnpress() {
    if (this.props.status == 'Empty') {
      this.props.handler(this.props.id, JSON.stringify(this.props.name));
    }
    else if (this.props.status == 'Eating') {
      this.props.handler(this.props.id, 1);
    }else{
      this.props.handler(this.props.id, 1);
    }
  }
  ViewOnpress(e) {
    var pos = {
      x: e.nativeEvent.pageX,
      y: e.nativeEvent.pageY
    }

    this.props.handler(this.props.id, 1);
  }
  PayOnpress(e) {
    var pos = {
      x: e.nativeEvent.pageX,
      y: e.nativeEvent.pageY
    }
    this.props.handler(this.props.id, 0);
  }

  renderIconStatus(){
    if(this.props.status=='Empty'){
      return(
        <View  style={styles.center}>
        <Icon name="check-circle" color='#0a9b24' size={50} />
        
        <Text style={[{ color: "#0a9b24" , fontWeight: "bold"}]} note>
        {this.props.status}
      </Text>
      </View>
      )
    }
    else if(this.props.status=='Eating'){
      return(
        <View  style={styles.center}>
        <Icon name="flickr" color='#d8344f' size={50} />
        <Text style={[{ color: "#d8344f" , fontWeight: "bold"}]} note>
        {this.props.status}
      </Text>
        </View>
      )
    }
    else{
      return(
        <View  style={styles.center}>
        <Icon name="minus-circle" color='#c69a1f' size={50} />
        <Text style={[{ color: "#c69a1f" , fontWeight: "bold"}]} note>
        {this.props.status}
      </Text>
        </View>
      )
    }
  }


  render() {
    var isEating = (this.props.status == 'Eating')
    var isHolding = (this.props.status == 'Hold')
    return (
      <TouchableOpacity
        style={[styles.cardContainer, { flex: 0.20 }]}
         onPress={() => { this.OrderOnpress()}}
         onLongPress ={() => { this.MoveOnpress()}}
        >
        <View onPress={this._toggleExpanded} 
        style={[styles.card, styles.horizontal, 
        { padding: 10, backgroundColor: '#30d160' }, 
        (isEating) && { backgroundColor: '#ff6889' },
        (isHolding) && { backgroundColor: '#f4d422' },
        ]}>
          <View style={{ flex: 0.7 }}>
            <View style={{marginBottom: 2}}>
              <Text style={[styles.bold, { color: 'white', fontSize: 22, fontWeight: 'bold' }]}>
                {this.props.name}
              </Text>
            </View>
            <View>
            
              <Text style={{ color: "white" }}>
                Capacity: {this.props.capacity}
              </Text>
              { (this.props.status != "Empty")&&
              <Text style={{ color: "white" }}>
                Time: {this.props.time.hour}: {this.props.time.minute}
              </Text>
              }
            </View>
          </View>
          <View style={{ flex: 0.3, alignItems: 'flex-end', justifyContent: 'center', }}>
            
            {this.renderIconStatus()}
              
            
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

