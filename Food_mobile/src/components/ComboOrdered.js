import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Image, Text, Button, TouchableHighlight, FlatList } from 'react-native';

import FoodCheckItem from './FoodCheckItem'
import Icon from 'react-native-vector-icons/Feather';
import Collapsible from 'react-native-collapsible';

import styles from './styles';

export default class ComboOrdered extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 0,
            collapsed: true
        };
        this.handlerFoodItem = this.handlerFoodItem.bind(this)
    }
    _toggleExpanded = () => {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }
    handlerFoodItem(e, f, c) {
        this.props.handlerFoodItem(e, f, c);
    }
    showDetail() {
        this.props.handlerComboModal(this.props.id);
    }
   

    renderOrderedFood = ({ item, index }) => {

        return (
            <FoodCheckItem
                id={item.id}
                food_id={item.food_id}
                foodName={item.fname}
                foodImg={item.image}
                price={item.fprice}
                isPay={item.isPay}
                time={item.time}
                status={item.status}
                comment={item.note}
                amount={item.amount}
                btnHide={true}
                handlerFoodItem={this.handlerFoodItem}
            />

        );
    }

    render() {
        return (
            <TouchableOpacity style={[{ margin: 0,  }, styles.horizontal]} onPress={this._toggleExpanded}>
                <View style={[{ flex: 0.02, paddingBottom: 5 }]}>
                    <View style={{ backgroundColor: 'red', flex: 1, alignSelf: 'stretch' }}>

                    </View>
                </View>
                <View style={{ flex: 0.98 }}>
                    <View onPress={this._toggleExpanded} style={[styles.listItem, styles.borderBottom, styles.comboHeader, { marginLeft: 0 }]}>
                        <View style={[{ flex: 0.15, padding: 5, }, styles.centerImg, styles.horizontal]}>

                            <Image
                                source={{ uri: this.props.img }}
                                style={[styles.foodThumnail, { flex: 0.5, width: 30 }, this.state.collapsed && { flex: 1 }]}
                            />

                        </View>

                        <View style={[{ justifyContent: 'center', flex: 0.7 }]}>
                            <View style={{ flex: 0.3, justifyContent: 'center' }}>
                                <Text style={{ fontWeight: "bold" }}>
                                    {this.props.name}
                                </Text>
                            </View>
                            { this.state.collapsed&&
                            <View style={{ flex: 0.3, justifyContent: 'center' }}>
                                <Text style={{color: '#ed5a6f',fontSize:12}}>
                                    Combo
                            </Text>
                            </View>
                            }
                        </View>

                        <View style={[{ justifyContent: 'center', flex: 0.15 }]}>
                            <Text note>
                                x {this.props.amount}
                            </Text>
                        </View>
                    </View>
                    {!this.state.collapsed &&
                        <View style={[{ flex: 1, padding: 0, alignItem: 'stretch' }, styles.center,]} collapsed={this.state.collapsed}>
                         
                                <View style={styles.horizontal}>

                                    <View style={[{ flex: 1 / 3, }, styles.horizontal, styles.center]}>
                                      
                                        <View style={[styles.center, { alignSelf: 'center', width: 10 }]}>

                                        </View>
                                        <TouchableOpacity onPress={() => { this.showDetail() }}>
                                            <Icon name="info" size={25} color="#5A7587" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={[{ flex: 1 / 3, }]}>
                                    </View>
                                    <View style={[{ flex: 1 / 3, }]}>
                                    </View>
                                </View>
                            
                            <View style={{ alignSelf: 'stretch' }}>
                                <FlatList data={this.props.foods}
                                    renderItem={this.renderOrderedFood}
                                    keyExtractor={item => item.id}
                                    style={{ margin: 0, padding: 0 }} />
                            </View>
                        </View>
                    }
                </View>
                {this.props.isPay && <Image style={[styles.paidIcon]} source={require('../assets/img/Paidicon.png')} />}
            </TouchableOpacity>
        )
    }
}