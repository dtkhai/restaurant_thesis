import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet, ImageBackground, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';

export default class DiscountBillBanner extends Component {

    renderDiscountValue() {
        if (this.props.discount.percentage_value) {
            return (
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 34 }}>- {this.props.discount.percentage_value}%</Text>
            )
        }
        else {
            return (
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 34 }}>- {this.props.discount.value}$</Text>
            )
        }
    }
    render() {
        var dateF = this.props.discount.date_from.toString().slice(0, 10);
        var dateT = this.props.discount.date_to.toString().slice(0, 10);
        return (
            <View style={styles.discountBill} >

                <View style={{ flex: 0.2 }}>
                    <Image style={{
                        justifyContent: 'center',
                        width: "85%",
                        height: "85%",
                        position: 'absolute',
                        top: -5,
                        left: 5,
                        zIndex: 10,
                        elevation: 1
                    }} source={require('../assets/img/DiscountBill.png')} />
                </View>
                <View style={{
                    position: 'absolute',
                    width: 300,
                    right: 0,
                    borderStyle: 'solid',
                    borderLeftWidth: 50,
                    borderTopWidth: 80,
                    borderLeftColor: 'transparent',
                    borderTopColor: '#F33E75'
                }}>
                </View>
                <View style={[{ flex: 0.5, }]}>
                    <View style={{ flex: 0.4 }}>
                        <Text style={{ fontWeight: 'bold', color: '#F33E75', fontSize: 24 }}>{this.props.discount.name.toUpperCase()}</Text>
                    </View>
                    <View style={{ flex: 0.6}}>
                    {this.props.discount.price_requried&&
                        <View style={styles.horizontal}>
                        <Text style={{ fontWeight: 'bold'}}>Apply for bill above </Text>
                            <Text style={{ fontWeight: 'bold',color:'#FD8C7C'}}>{this.props.discount.price_requried}$</Text>
                        </View>
                    }
                        <View style={styles.horizontal}>
                        <Text style={{ fontWeight: 'bold'}}>From </Text>
                            <Text style={{  }}>{dateF}</Text>
                            <Text style={{ fontWeight: 'bold'}}> To </Text>
                            <Text style={{ }}>{dateT}</Text>
                        </View>
                    </View>
                </View>
                <View style={[styles.center, { flex: 0.3}]}>
                    {
                        this.renderDiscountValue()
                    }
                </View>
            </View>
        )
    }

}