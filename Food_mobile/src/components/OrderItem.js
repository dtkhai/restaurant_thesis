import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet} from 'react-native';
import { Content, Left, Icon, Right, Button, ListItem, Thumbnail, Text, Body } from 'native-base';

import Collapsible from 'react-native-collapsible';
export default class OrderItem extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      collapsed: true
    };
  }


  _toggleExpanded = () => {
    this.setState( {
      collapsed: !this.state.collapsed
    } );
  }

  OrderOnpress() {
    this.props.handler( this.props.id );
  }

  render() {
    return (
    <TouchableOpacity style={ { margin: 0 } } onPress={ this._toggleExpanded }>
      <ListItem onPress={ this._toggleExpanded } style={ [ styles.normal, this.state.number && styles.active ] }>
        <Thumbnail square
                   size={ 80 }
                   source={ { uri: 'Image URL' } } />
        <Body>
          <Text>
            ID:
            { this.props.id }
          </Text>
          <Text note>
            Table:
            { this.props.tableId }
          </Text>
        </Body>
        <View>
          <Text>
            { this.props.status }
          </Text>
          <Button onPress={ () => {
                              this.OrderOnpress()
                            } }>
            <Text>
              View
            </Text>
          </Button>
        </View>
      </ListItem>
      <Collapsible collapsed={ this.state.collapsed } align="center">
        <View>
          <Right>
            <Button transparent
                    dark
                    onPress={ () => {
                                this.OrderOnpress()
                              } }>
              <Text>
                MoreButton Goes Here
              </Text>
            </Button>
          </Right>
        </View>
      </Collapsible>
    </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create( {
  active: {
    backgroundColor: 'rgb(178, 255, 198)'
  },
  normal: {
    margin: 0
  }
}
);
