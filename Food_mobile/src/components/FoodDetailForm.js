import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Text, TextInput, Image, FlatList } from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/Entypo';



export default class FoodDetailForm extends Component {


	increaseNum() {
		this.props.handler('num', this.props.numText + 1);
	}
	decreaseNum() {
		if (this.props.numText > 0) {
			this.props.handler('num', this.props.numText - 1);
		}
	}
	onChanged(text) {
		var temp
		if (text == null) {
			temp = 0
		} else {

			temp = parseInt(text, 10);
		}

		//this.setState({ numText: temp });
		this.props.handler('num', temp);
	}
	commentChanged(comment) {
		//this.setState({ comment: comment });

		this.props.handler('comment', comment);
	}

	renderItem = ({ item }) => {
		return (
			<View style={[styles.listItem]}>
				<View style={[{ flex: 0.1, padding: 5, aspectRatio: 1 }, styles.centerImg, styles.horizontal]}>
					<Image
						source={{ uri: item.main_image }}
						style={[styles.foodThumnail, { flex: 1, width: 30 }]}
					/>
				</View>

				<View style={[{ justifyContent: 'center', flex: 0.9 }]}>
					<View style={{ flex: 0.4, justifyContent: 'center' }}>
						<Text style={{ fontWeight: "bold" }}>
							{item.name}
						</Text>
					</View>
				</View>
			</View>
		)
	}

	renderDiscount() {
		const { discount } = this.props
		if (discount) {
			var tStr = discount.name + ': '
			if (discount.percentage_value) {
				tStr += discount.percentage_value + '% ' + ', '
			}
			if (discount.value) {
				tStr += '$' + discount.value + ' directly'
			}

			return (
				<View>
					<Text style={{
						fontWeight: "bold", color: 'red', marginTop: 5, textDecorationLine: "underline",
						textDecorationStyle: "solid",
						textDecorationColor: "red",
					}}>
						Discount:
					</Text>
					<Text >
						{tStr}
					</Text>
				</View>)
		}
	}

	renderPrice() {
		if (this.props.discount) {
			return (
				<View style={[styles.horizontal, { justifyContent: 'flex-end' }]}>
					<Text style={[{ marginRight: 5, fontSize: 14, fontWeight: 'bold', color: '#4a6184', textDecorationLine: 'line-through', textDecorationStyle: 'solid' }]} note>
						{this.props.price} $
						  </Text>
					<Text style={[styles.textRight, styles.price, { fontSize: 24, color: '#e21d3b' }]} note>
						{this.props.price - this.props.price * (this.props.discount.percentage_value / 100) - this.props.discount.value}$
						  </Text>

				</View>
			)
		}
		else {
			return (<Text style={[styles.textRight, styles.price, { fontSize: 24 }]} note>
				{this.props.price} $
				  </Text>
			)
		}
	}

	render() {
		return (
			<View style={{ flex: 0.9, padding: 5 }}>
				<View style={[{ flex: 0.4 }, this.props.isCombo && { flex: 0.8 }]}>
					<View style={[styles.foodLabel, styles.borderBottom]}>
						<View style={{ flex: 0.7 }}>
							<Text style={[styles.bold, { color: '#062E42', fontSize: 24 }]}>
								{this.props.name}
							</Text>
						</View>
						<View style={[{ flex: 0.3, }]}>
							{this.renderPrice()}
						</View>
					</View>

					<View style={{ height: 10 }}>
					</View>
					{
						!this.props.isCombo &&
						<View>
							<Text style={{
								fontWeight: 'bold', textDecorationLine: "underline",
								textDecorationStyle: "solid",
								textDecorationColor: "#000",
							}}>Description: </Text>
							<Text style={{ marginTop: 5, }}>{this.props.description}</Text>
						</View>
					}
					{
						this.props.isCombo &&
						(
							<View>
								<Text style={{ fontSize: 18, fontWeight: 'bold'}}>
									Combo:
                      			</Text>
								<FlatList data={this.props.foods}
									renderItem={this.renderItem}
									keyExtractor={(item, index) => index}
									style={{ margin: 0, padding: 0 }} />
							</View>
						)
					}
					{this.renderDiscount()}
				</View>
				<View style={[{ flex: 0.6, marginTop: 20, paddingTop: 10, }, styles.borderTop, this.props.isCombo && { flex: 0.2 }]}>
					<View>
						<Text style={[styles.bold, { marginBottom: 5, }]}>Amount</Text>
						<View style={[styles.horizontal, { alignSelf: 'stretch', marginBottom: 10, }]}>

							<TouchableOpacity style={[styles.buttonAdd]} onPress={() => {
								this.decreaseNum()
							}}>
								<Icon name="minus" color='#5A7587' size={20} />
							</TouchableOpacity>

							<TextInput
								editable={true}
								underlineColorAndroid="transparent"
								keyboardType='numeric'
								style={[styles.formInput, { textAlign: 'right', flex: 0.2, marginLeft: 5, marginRight: 5 }]}
								placeholder='0'
								onChangeText={(text) => this.onChanged(text)}
								value={(this.props.numText) ? this.props.numText.toString() : 0} />
							<TouchableOpacity style={[styles.buttonAdd]} onPress={() => {
								this.increaseNum()
							}}>
								<Icon name="plus" color='#5A7587' size={20} />
							</TouchableOpacity>
						</View>
						{!this.props.isCombo &&
							(
								<View>
									<Text style={[styles.bold, { marginBottom: 5 }]}>Comment</Text>
									<TextInput
										style={[styles.formInput, { height: 120, }]}
										multiline={true}
										underlineColorAndroid="transparent"
										numberOfLines={4}
										textAlignVertical='top'
										placeholder='Enter your comment on this...'
										onChangeText={(comment) => this.commentChanged(comment)}
										value={this.props.comment} />
								</View>
							)
						}

					</View>
				</View>
			</View>



		);
	}
}