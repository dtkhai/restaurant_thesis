import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Text, } from 'react-native';


import styles from './styles';

export default class FoodPayment extends Component {


	render() {
		return (
			<View style={{ margin: 0 }} onPress={this._toggleExpanded}>
				<View onPress={this._toggleExpanded} style={[styles.listItem]}>

					<View style={[{ justifyContent: 'center', flex: 0.1 }]}>
						<Text style={{fontSize: 13}}>
							{this.props.index + 1}
						</Text>
					</View>

					<View style={[{ justifyContent: 'center', flex: 0.35 }]}>
						<Text style={{fontSize: 13}}>
							{this.props.foodName}
						</Text>

					</View>
					<View style={[{ justifyContent: 'center', flex: 0.2 }]}>
						<Text style={{fontSize: 13}}>
							{this.props.price} $
		          		</Text>
					</View>

					<View style={[{ justifyContent: 'center', flex: 0.15, padding: 5 }]}>
						<Text style={{fontSize: 13}}>
							x {this.props.amount}
						</Text>
					</View>
					<View style={[{ justifyContent: 'center', flex: 0.2, padding: 5 }]}>
						<Text style={[styles.textRight,{fontSize: 13}]} >
							{this.props.totalPrice} $
			          </Text>
					</View>

				</View>
			</View>
		)
	}
}