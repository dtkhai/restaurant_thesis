import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Text, Image, FlatList, TextInput, Alert, ScrollView } from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/Entypo';
import FoodDetailForm from './FoodDetailForm.js'
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

@inject('orderStore')
@observer
export default class ComboDetail extends Component {
    constructor(props) {
        super(props);
        var temp = [
            {
                id: 0,
                name: this.props.combo.name,
                main_image: this.props.combo.image
            }
        ]
        this.state = {
            data: this.props.orderStore.comboListGet,
            numText: null,
            comment: null,
            listFood: temp.concat(this.props.combo.foods),
            currentImg: 0,
            description: null
        };
        this.handler = this.handler.bind(this)
    }
    componentDidMount() {
        var temp = 0
        var tempcom = null
        
        var a = this.state.data.find(x => { return (x.combo_id == this.props.combo.id) })
        if (a) {
                temp = a.amount
                tempcom = a.comment
            }
        this.setState(
            {
                
                numText: temp,
                comment: tempcom,
                currentUrl: this.props.combo.image
            }
        )
    }


    handler(e, f) {
        if (e == 'num') {
            this.setState({
                numText: f
            })
        }
        else if (e == 'comment') {

            this.setState({
                comment: f
            })
        }
    }
    showDetail() {
        this.props.handlerComboModal(this.props.id);
    }
    saveDetail() {
        const a = ''
        var tempComment = this.state.comment ? this.state.comment : a
        if (this.state.numText > 0) {
            this.props.orderStore.modifyCombo(this.props.combo.id, this.state.numText, this.props.combo.name, this.props.combo.image, this.props.combo.foods)
        }
        else {
            this.props.orderStore.clearCombo(this.props.combo.id);
        }
        this.props.handler(-1)
        this.props.handlerComboModal(this.props.id);
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={[{ padding: 5, alignItems: 'stretch', flex: 1, aspectRatio: 1, backgroundColor: 'white' }]}
                onPress={() => this.setState({ currentUrl: item.main_image })}>
                <View style={{ flex: 1 }}>
                    <Image style={[styles.foodThumnail3]} source={{ uri: item.main_image }} />
                </View>
                <View style={{ position: 'absolute', alignItems: "center", bottom: 0,left:0,right:0, backgroundColor: 'rgba(255, 255, 255, 0.75)' }}>
                    <Text style={[styles.bold, { color: '#062E42', fontSize: 14, textAlign: 'center', paddingBottom: 5 }]}> {item.name}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.modalContent}>
                <View style={[styles.modalBody, { flex: 1 }]}>

                    <View style={[styles.modalLeft, { borderColor: '#d6d7da', backgroundColor: 'white', borderRadius: 10, }]}>
                        <View style={[{ padding: 5, margin: 5, backgroundColor: 'white', }, styles.centerImg]}>
                            <Image style={styles.foodThumnail} source={{ uri: this.state.currentUrl }} />
                        </View>
                        <View style={[{ flex: 1, margin: 5, alignSelf: 'stretch', backgroundColor: 'white', }, styles.centerImg]}>
                            <FlatList data={this.state.listFood}
                                horizontal={true}
                                renderItem={this.renderItem}

                                keyExtractor={(item, index) => index}
                                style={{ margin: 0, padding: 0 }} />
                        </View>
                    </View>
                    <View style={[styles.modalRight, { paddingLeft: 20, paddingTop: 0, }]}>

                        <FoodDetailForm handler={this.handler}
                            numText={this.state.numText}
                            comment={this.state.comment}
                            name={this.props.combo.name}
                            price={this.props.combo.price}
                            foods={this.props.combo.foods}
                            description={''}
                            discount={this.props.combo.Discount}
                            isCombo={true}
                        />

                        <View style={styles.modalFooter}>
                            <TouchableOpacity style={styles.modalButton} Full onPress={() => { this.saveDetail() }}>
                                <Text style={[styles.focusText, styles.white]}>CONFIRM</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.modalButton2]} Full onPress={() => { this.showDetail() }}>
                                <Text style={[styles.focusText, styles.white]}>DISCARD</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}