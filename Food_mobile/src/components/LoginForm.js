
import React, { Component } from 'react';
import { Alert, Platform, StyleSheet, Text, View, TextInput,KeyboardAvoidingView  } from 'react-native';
import { Button, CheckBox, Container, Content } from 'native-base';
import { Sae } from 'react-native-textinput-effects';
import firebase from "react-native-firebase";
import Icon from 'react-native-vector-icons/FontAwesome';
import {LoginAPI} from '../networking/UserServer.js';

import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

@inject('userStore')
@observer


class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chkbox_chk: false,
      email: 'cuong',
      password: '123456',
      isLoging: false,
    };
  }
  ChangeCheck() {
    this.setState({
      chkbox_chk: !this.state.chkbox_chk
    });
  }
  OnLoginFireBase() {
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {

        this.setState({
          email: '',
          password: '',
        });
        this.props.handler();
      })
      .catch((error) => {
        if ((this.state.email.trim() == '') || (this.state.password.trim() == '')) {
          Alert.alert(
            'Error',
            'Please fill all the fields',
            [
              { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
          )
        }
        else {
          Alert.alert(
            'Error',
            'Wrong email or password',
            [
              { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
          )
        }
        this.setState({
          email: '',
          password: '',
        });
      });

  }
  ResetLogin(){
    
    this.setState({
      email: '',
      password: '',
    });
  }
  OnLogin() {
    //this.props.handlerLogin();

    var body = {
      'email': this.state.email,
      'password': this.state.password
    }
    this.setState({
      isLoging: true,
    })
    LoginAPI(body).then((user) => {
     
        if(user.success){
          this.props.userStore.StoreUser(user.data)
          this.props.handlerLogin();
        }
        else{
          Alert.alert('Incorrect')
          this.ResetLogin();
        }
        this.setState({
          isLoging: false,
        })
    }).catch((error)=>{
      Alert.alert('Network error')
      this.ResetLogin();
      this.setState({
        isLoging: false,
      })
    }) 
  }

  renderButton(){
    if(this.state.isLoging){
      return(
        <View 
          style={[{ height: 60, borderRadius: 30,alignItems:'center'}]}
         >
          <Text style={[styles.buttonText,{textAlign:'center'}]}>
          Loging In ...
        </Text>
        </View>
      )
    }
    else{
      return(
        <Button full
          primary
          style={{ height: 60, borderRadius: 30,backgroundColor: '#00A59A' }}
          onPress={() => this.OnLogin()}>
          <Text style={styles.buttonText}>
          Log In
        </Text>
        </Button>
      )
    }
  }

  render() {
    return (
      <KeyboardAvoidingView  style={styles.container} behavior="padding" enabled>
        <View style={styles.inputContainer}>
          <Icon name="user"
            size={30}
            color="#062E42" />
          <TextInput style={styles.input}
            placeholder="Username"
            placeholderTextColor="rgba(0,0,0,0.3)"
            underlineColorAndroid='transparent'
            onChangeText={(email) => this.setState({ email })}
            value={this.state.email}
          />
        </View>
        <View style={styles.inputContainer}>
          <Icon name="lock"
            size={30}
            color="#062E42" />
          <TextInput style={styles.input}
            placeholder="Password"
            placeholderTextColor="rgba(0,0,0,0.3)"
            underlineColorAndroid='transparent'
            secureTextEntry
            onChangeText={(password) => this.setState({ password })}
            value={this.state.password} />
        </View>
      
        {this.renderButton()}
        
      </KeyboardAvoidingView>


    );
  }
}

const styles = StyleSheet.create({
  inputContainer:{
   
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 30,
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
    height: 60,
  },
  rmbText: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10

  },
  container: {
    padding: 20,
    paddingTop:10
  },
  input: {
    flex: 1,
    
    borderRadius: 20,
    fontSize: 20,
    marginLeft: 20,
    color: '#062E42',
    borderColor: 'rgb(111,111,111)',
    paddingHorizontal: 10,
  },
  buttonText: {
    color: '#FFF',
    fontWeight: "bold",
    fontSize: 23

  }
});

export default LoginForm;
