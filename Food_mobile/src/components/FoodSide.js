import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Image, Text, Button, TouchableHighlight, TextInput } from 'react-native';
import { Content, Left, Right, ListItem, Thumbnail, Body } from 'native-base';
import Icon from 'react-native-vector-icons/Feather';
import Collapsible from 'react-native-collapsible';
import styles from './styles';


export default class FoodSide extends Component {
	constructor(props) {
		super(props);
		this.state = {
			number: 0,
			collapsed: true,
			comment: null
		};
	}
	_toggleExpanded = () => {
		this.setState({
			collapsed: !this.state.collapsed
		});
	}

	componentDidMount() {

	}

	static getDerivedStateFromProps(nextProps, prevState) {
		if (prevState) {
			if (nextProps.comment !== prevState.comment) {
				return {
					comment: nextProps.comment
				};
			}
		}
	}

	showDetail() {
		this.props.handlerModal(this.props.food_id);
	}
	increaseScore() {
		if (this.props.combo) {
	
			this.props.handlerFoodInStore('add', true, this.props.combo,null, null )
		}
		else {
		
			this.props.handlerFoodInStore('add', false, this.props.food_id,null, null )
			this.props.handler(this.props.food_id);
		}


	}

	editComment() {		
		this.props.handlerFoodInStore('comment', false, this.props.food_id,this.state.comment, this.props.combo )
	
	}
	decreaseScore() {
		if (this.props.combo) {
		
			this.props.handlerFoodInStore('remove', true, this.props.combo,null, null )
		}
		else {
			this.props.handlerFoodInStore('remove', false, this.props.food_id,null, null )
		}
		this.props.handler(-1);
	}
	clearNum() {
		if (this.props.combo) {
			this.props.handlerFoodInStore('clear', true, this.props.combo,null, null )
		}
		else {
			this.setState({
				number: 0
			});
			this.props.handlerFoodInStore('clear', false, this.props.food_id,null, null )

		}
		this.props.handler(-1);
	}

	render() {
		return (
			<TouchableOpacity style={{ margin: 0 }} onPress={this._toggleExpanded}>
				<View onPress={this._toggleExpanded} style={[styles.listItem, styles.borderBottom, this.state.number && styles.active]}>
					<View style={[{ flex: 0.15, padding: 5, aspectRatio: 1}, styles.centerImg, styles.horizontal]}>
						<Image
							source={{ uri: this.props.foodImg }}
							style={[styles.foodThumnail, { flex: 1, width: 30 }]}
						/>
					</View>

					<View style={[{ justifyContent: 'center', flex: 0.7 }]}>
						<View style={{ flex: 0.4, justifyContent: 'center' }}>
							<Text style={{ fontWeight: "bold" }}>
								{this.props.foodName}
							</Text>
						</View>
						<View style={{ flex: 0.4, justifyContent: 'center' }}>
							{(this.props.comment.length > 0) && <Icon name="message-square" size={13} color="#5A7587" />}
						</View>
					</View>

					<View style={[{ justifyContent: 'center', flex: 0.15 }]}>
						<Text note>
							x {this.props.amount}
						</Text>
					</View>
				</View>
				<Collapsible style={[{ flex: 1, padding: 5, backgroundColor: "#f9f9f9" }, styles.center]} collapsed={this.state.collapsed} align="center">
					{ !this.props.btnHide &&
					<View style={[styles.horizontal, styles.center]}>
						<View style={[{ flex: 1 / 3, }, styles.horizontal, styles.center]}>
							<TouchableOpacity onPress={() => {
								this.clearNum()
							}}>
								<Icon name="x" size={25} color="#5A7587" />
							</TouchableOpacity>
							<View style={[styles.center, { alignSelf: 'center', width: 10 }]}>

							</View>
							<TouchableOpacity onPress={() => { this.showDetail() }}>
								<Icon name="info" size={25} color="#5A7587" />
							</TouchableOpacity>
						</View>
						<View style={[{ flex: 1 / 3, }]}>
						</View>

						<View style={[{ flex: 1 / 3, }, styles.horizontal, styles.center]}>
							<View style={[styles.horizontal, styles.center]}>
								<TouchableOpacity elevation={2} style={[styles.buttonAddSmall,]} onPress={() => {
									this.decreaseScore()
								}}>

									<Icon name="minus" size={15} color="#5A7587" />

								</TouchableOpacity>
								<View style={[styles.center, { alignSelf: 'center', width: 10 }]}>

								</View>
								<TouchableOpacity elevation={2} style={[styles.buttonAddSmall,]} onPress={() => {
									this.increaseScore()
								}}>

									<Icon name="plus" size={15} color="#5A7587" />

								</TouchableOpacity>
							</View>
						</View>
					</View>
					}
					<View style={{ alignSelf: 'stretch', marginTop: 5, borderRadius: 5, backgroundColor: 'lightgrey' }}>
						<View style={styles.foodLabel}>
							<View style={{ flex: 0.6 }}>
								<Text style={{ margin: 5 }}>Note </Text>
							</View>
							{(this.state.comment != this.props.comment) &&
								(<View style={[styles.horizontal, { flex: 0.4, justifyContent: 'flex-end' }]}>
									<View style={[{ backgroundColor: 'rgba(0,0,0,0.15)', margin: 1, marginRight: 3, borderRadius: 5, paddingHorizontal: 3 }, styles.center, styles.horizontal,]}>
										<TouchableOpacity onPress={() => {
											this.editComment()
										}}>
											<Icon name="check" size={20} color="#63915f" />
										</TouchableOpacity>
										<View style={[styles.center, { alignSelf: 'center', width: 10 }]}>

										</View>
										<TouchableOpacity onPress={() => { this.setState({ comment: this.props.comment }) }}>
											<Icon name="x" size={20} color="#b76161" />
										</TouchableOpacity>
									</View>
								</View>)
							}
						</View>

						<TextInput
							style={[styles.formInput, { height: 60, marginHorizontal: 5, marginBottom: 5 }]}
							multiline={true}
							underlineColorAndroid="transparent"
							numberOfLines={4}
							textAlignVertical='top'
							placeholder='Enter your comment on this...'
							onChangeText={(comment) => this.setState({ comment })}
							value={this.state.comment} />
					</View>
				</Collapsible>
			</TouchableOpacity>
		)
	}
}