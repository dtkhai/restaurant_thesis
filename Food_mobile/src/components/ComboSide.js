import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Image, Text, Button, TouchableHighlight, FlatList } from 'react-native';
import { Content, Left, Right, ListItem, Thumbnail, Body } from 'native-base';
import FoodSide from './FoodSide'
import Icon from 'react-native-vector-icons/Feather';
import Collapsible from 'react-native-collapsible';

import styles from './styles';

export default class ComboSide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 0,
            collapsed: true
        };
        this.handlerFoodInStore = this.handlerFoodInStore.bind(this)
    }
    _toggleExpanded = () => {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }
    handlerFoodInStore(f, isCombo, id,comment, combo_id ){
        this.props.handlerFoodInStore(f, isCombo, id,comment, combo_id )
    }

    showDetail() {
        this.props.handlerComboModal(this.props.id);
    }
    increaseScore() {

        this.props.handlerFoodInStore('add', true, this.props.id,null, null )


    }


    decreaseScore() {

        
        this.props.handlerFoodInStore('remove', true, this.props.id,null, null )

        this.props.handler(-1);
    }
    clearNum() {

        this.props.handlerFoodInStore('clear', true, this.props.id,null, null )

        this.props.handler(-1);
    }


    renderItemSide = ({ item }) => {
        /*<Food name={item.name} price={item.price}/>*/

        return (<FoodSide food_id={item.food_id}
            foodName={item.food_name}
            foodImg={item.main_image}
            comment={item.comment}
            amount={item.amount}
            handler={this.handler}
            handlerModal={this.handlerModal}
            btnHide = {true}
            combo={this.props.id}
            handlerFoodInStore={this.handlerFoodInStore}
        />);
    }
    render() {
        return (
            <TouchableOpacity style={[{ margin: 0, }, styles.horizontal]} onPress={this._toggleExpanded}>
                <View style={[{ flex: 0.02, paddingBottom: 5 }]}>
                    <View style={{ backgroundColor: 'red', flex: 1, alignSelf: 'stretch' }}>

                    </View>
                </View>
                <View style={{ flex: 0.98 }}>
                    <View onPress={this._toggleExpanded} style={[styles.listItem, styles.borderBottom, styles.comboHeader,{marginLeft:0}]}>
                        <View style={[{ flex: 0.15, padding: 5, }, styles.centerImg, styles.horizontal]}>

                            <Image
                                source={{ uri: this.props.img }}
                                style={[styles.foodThumnail, { flex: 0.5, width: 30 }, this.state.collapsed && { flex: 1 }]}
                            />

                        </View>

                        <View style={[{ justifyContent: 'center', flex: 0.7 }]}>
                            <View style={{ flex: 0.4, justifyContent: 'center' }}>
                                <Text style={{ fontWeight: "bold" }}>
                                    {this.props.name}
                                </Text>
                                { this.state.collapsed&&
                            <View style={{ flex: 0.3, justifyContent: 'center' }}>
                                <Text style={{color: '#ed5a6f',fontSize:12}}>
                                    Combo
                            </Text>
                            </View>
                            }
                            </View>

                        </View>

                        <View style={[{ justifyContent: 'center', flex: 0.15 }]}>
                            <Text note>
                                x {this.props.amount}
                            </Text>
                        </View>
                    </View>
                    {!this.state.collapsed &&
                        <View style={[{ flex: 1, alignItem: 'stretch' }, styles.center]}>
                            {this.props.btnActive &&
                                <View style={[styles.horizontal,{marginVertical: 5}]}>

                                    <View style={[{ flex: 1 / 3, }, styles.horizontal, styles.center]}>
                                        <TouchableOpacity onPress={() => {
                                            this.clearNum()
                                        }}>
                                            <Icon name="x" size={25} color="#5A7587" />
                                        </TouchableOpacity>
                                        <View style={[styles.center, { alignSelf: 'center', width: 10 }]}>

                                        </View>
                                        <TouchableOpacity onPress={() => { this.showDetail() }}>
                                            <Icon name="info" size={25} color="#5A7587" />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={[{ flex: 1 / 3, }]}>
                                    </View>

                                    <View style={[{ flex: 1 / 3, }, styles.horizontal, styles.center]}>
                                        <View style={[styles.horizontal, styles.center]}>
                                            <TouchableOpacity elevation={2} style={[styles.buttonAddSmall,]} onPress={() => {
                                                this.decreaseScore()
                                            }}>

                                                <Icon name="minus" size={15} color="#5A7587" />

                                            </TouchableOpacity>
                                            <View style={[styles.center, { alignSelf: 'center', width: 10 }]}>

                                            </View>
                                            <TouchableOpacity elevation={2} style={[styles.buttonAddSmall,]} onPress={() => {
                                                this.increaseScore()
                                            }}>

                                                <Icon name="plus" size={15} color="#5A7587" />

                                            </TouchableOpacity>
                                        </View>

                                    </View>
                                </View>
                            }
                            <View style={{ alignSelf: 'stretch' }}>
                                <FlatList data={this.props.foods}
                                    renderItem={this.renderItemSide}
                                    keyExtractor={item => item.id}
                                    style={{ margin: 0, padding: 0 }} />
                            </View>
                        </View>
                    }
                </View>
            </TouchableOpacity>
        )
    }
}