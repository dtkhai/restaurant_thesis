import { Dimensions } from 'react-native';

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;


const widthPercentageToDP = widthPercent => {
  const screenWidth = Dimensions.get('window').width;
  // Convert string input to decimal number
  const elemWidth = parseFloat(widthPercent);
  return PixelRatio.roundToNearestPixel(screenWidth * elemWidth / 100);
};
const heightPercentageToDP = heightPercent => {
  const screenHeight = Dimensions.get('window').height;
  // Convert string input to decimal number
  const elemHeight = parseFloat(heightPercent);
return PixelRatio.roundToNearestPixel(screenHeight * elemHeight / 100);
};

export default {
  overlay:{
    position: 'absolute',
    alignItems: 'flex-end',
    width: deviceWidth,
    height: deviceHeight,
    backgroundColor: 'rgba(0,0,0,0.3)',
    elevation: 2,
  },
  container: {
    backgroundColor: '#FFF'
  },
  text: {
    alignSelf: 'center',
    marginBottom: 7
  },
  mb: {
    marginBottom: 15
  },
  cardContainer: {
    flex: 1/3,

  }
  ,
  dropShadow: {

    shadowColor: 'black',
    shadowOpacity: 0.9,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5
    },
    elevation: 1,
  },
  card: {
    backgroundColor: '#f9f9f9',
    borderRadius: 10,
  
    elevation: 3,
    padding: 2,
    margin: 6,
    borderRadius: 10,
    flex: 1,
   
    shadowRadius: 1,
    shadowColor: '#000',
    shadowOpacity: 0.9,    
    shadowOffset: {width:1,height: 10},
  },
  foodThumnail: {
    borderRadius: 10,
    aspectRatio: 1
  },
  foodThumnail2: {
    borderRadius: 10,
    
    aspectRatio: 1.4
  },
  foodThumnail3: {
    borderRadius: 10,
    aspectRatio: 1
  },
  foodImage: {
    borderRadius: 10,

  },
  active: {
    backgroundColor: "#96e8d9"
  },
  active2: {
    backgroundColor: "#e3e896"
  },
  disabled: {
    backgroundColor: "#c6c6c6"
  },
  borderTop: {
    borderTopWidth: 1, borderColor: '#d6d7da', paddingBottom: 5,
  },
  borderBottom: {
    borderBottomWidth: 1, borderColor: '#a8adaf', paddingBottom: 2,
  },
  bold: {
    fontWeight: 'bold',
  },
  focusText: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  white: {
    color: 'white',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  expand: {
    margin: 5,
    marginTop: 0,
    padding: 10,
    backgroundColor: 'rgb(250, 250, 250)'
  },
  horizontal: {
    flexDirection: 'row'
  },
  textRight: {    
    justifyContent: 'center',
    textAlign: 'right'
  },
  textLeft: {    
    justifyContent: 'center',
    textAlign: 'right'
  },
  foodLabel: {
    padding: 2,
    
    flexDirection: 'row',

  },
  desciption: {
    height: 43,
    paddingHorizontal: 5,
    overflow: 'hidden',
    backgroundColor: 'rgba(255,255,255,0.05)',
  },
  price: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#45598e'
  },
  amountText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  buttonGroup1: {
    padding: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F9F7F7',
    borderWidth: 0.2,
    borderColor: '#d6d7da',
    borderRadius: 10,
    overflow: 'hidden'
  },
  buttonMain: {
    backgroundColor: '#F9F7F7',
    marginLeft: 0,
    width: 60,
    height: 27,
    borderRadius: 10,
    borderWidth: 0.2,
    borderColor: '#d6d7da',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonSplite: {
    backgroundColor: '#C7DDEA',
    marginLeft: 5,
    width: 60,
    alignSelf: 'stretch',
    borderRadius: 10,
    borderWidth: 0.2,
    borderColor: '#d6d7da',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonAdd: {
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: '#BCD6E5',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0, backgroundColor: '#C7DDEA', borderWidth: 1, flex: 0.1, height: 40, width: 40
  },
  buttonAddSmall: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    backgroundColor: '#C9D9E2',
    borderRadius: 5,

  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  floatingButton: {
    borderWidth: 0,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
    position: 'absolute',
    bottom: 130,
    right: 10,
    height: 40,
    backgroundColor: '#1abc9c',
    borderRadius: 100,
  },
  circleButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 32,
    height: 32,
    backgroundColor: '#C7DDEA',
    borderRadius: 10,
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 1
    },

  },
  modalContent: {
    
   
    flex: 1,
    aspectRatio: 1.5,
    backgroundColor: "#EFF2F4",
    padding: 20,
    justifyContent: "center",
    alignItems: "center",

    borderRadius: 10,
    borderColor: "rgba(0, 0, 0, 0.1)",
    
    alignSelf: "center",
  },
  modalHeader: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    alignSelf: 'stretch',
    flex: 0.1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "blue",
  },
  modalBody: {
    borderRadius: 5,
    flex: 0.8,
    flexDirection: 'row'
  },
  modalBodyLeft: {
    flex: 0.2,

  },
  modalBodyMid: {
    flex: 0.4,

  },
  modalLeft: {
    flex: 0.5,
  },
  modalBodyRight: {
    flex: 0.4,
    flexDirection: 'column',

    justifyContent: "space-between",
  },
  modalRight: {
    flex: 0.5,
    flexDirection: 'column',
    padding: 10,
    paddingTop: 0,
    paddingBottom: 0,
    justifyContent: "space-between",
  },
  modalFooter: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "flex-end",
    
  },
  modalButton: {
    alignSelf: 'center',
    justifyContent: "center",
    backgroundColor: '#278964',
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 12,
    paddingBottom: 12,
    marginLeft: 10,
    marginRight: 10,
    flex: 0.5,
    borderRadius: 10,
  },
  modalButton2: {
    alignSelf: 'center',
    justifyContent: "center",
    backgroundColor: "#c43e50",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 12,
    paddingBottom: 12,
    flex: 0.5,
    marginLeft: 10,
    marginRight: 10,
    
    borderRadius: 10,
  },
  centerImg: {
    justifyContent: "center",
  },
  bottomButtonGroup: {
    flex: 0.2,
    alignSelf: 'flex-end',
  },
  listItem: {
    flex: 1,
    flexDirection: 'row',
    borderColor: '#768B96',
    borderBottomWidth: 0,
    marginHorizontal: 5,
    
  },
  segmentGroup: {
    alignSelf: 'stretch',
    height: 50,
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 3,
  },
  segmentTab: { 
    borderRadius: 10,
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  segmentTabLeft: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  segmentTabMid: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  segmentTabRight: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  segmentActive: {
    backgroundColor: '#EF6461'
  },
  formInput: {
    height: 40, borderColor: 'gray', backgroundColor: 'white', borderRadius: 5,
    paddingLeft: 10,
    paddingRight: 10,
    shadowColor: 'black',
    shadowOpacity: 0.9,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5
    },
    elevation: 1,
  },
  formInput2: {
    height: 50, borderColor: 'gray', backgroundColor: 'white', borderRadius: 5,
    marginTop: 10,

    fontSize: 18,
    paddingLeft: 10,
    paddingRight: 10,
    shadowColor: 'black',
    shadowOpacity: 0.9,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5
    },
    elevation: 1,
  },
  formInputIcon:{
    height: 40,  backgroundColor: 'white', borderRadius: 5,
    justifyContent: 'center',
    fontSize: 14,
    paddingLeft: 10,
    paddingRight: 10,
    flex: 0.7
  },
  formInputContainer: {
    height: 40, borderColor: 'gray', backgroundColor: 'white', borderRadius: 5,
    width: 150,
    flexDirection: 'row',
    justifyContent: 'center',
    
    shadowColor: 'black',
    shadowOpacity: 0.9,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5
    },
    elevation: 1,
  },
  orderListPopUp: {
    borderWidth: 0,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    position: 'absolute',
    bottom: 130,
    right: 10,
    height: 200,
    borderRadius: 10,
    backgroundColor: 'red',
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 1
    },
  },
  billHeader: {
    height: 35, borderColor: 'gray', backgroundColor: '#EFF2F4', 
    flexDirection: "row",
    justifyContent: 'center',
    width: 115,
    margin: 5,
    marginTop: 0,
    borderBottomLeftRadius: 0,
		borderBottomRightRadius: 0,
		borderTopLeftRadius: 5,
		borderTopRightRadius: 5,
  },
  billContainer: {
    alignSelf: 'stretch',
    
  },
  billActive: {
    backgroundColor: 'white', 
  },
  iconDefault:{
    alignSelf: 'center', fontSize:14, color: '#5E5E5E'
  },
  discount: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    height: 60,
    borderRadius: 100,
    backgroundColor: '#e82a19',
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 10,
    elevation: 3,
    shadowColor: 'black',
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 0,
      width: 0
    },
  },
  discountImg: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    height: 70,   
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 10,
    elevation: 3,  
  },
  orderedFood: {
    paddingLeft: 5,
    justifyContent: 'center',
    width: 110,
    height: 37,   
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 10,
    elevation: 3,  
  },
  paidIcon: {
    
    justifyContent: 'center',
     height: 35,
     width: 35,
    position: 'absolute',
    top: -2,
    right: 3,
    zIndex: 10,
    elevation: 1,  
  },
  comboHeader: {
    backgroundColor: '#ffe6dd'
  },
  discountBill:{
    justifyContent: 'space-between', 
    backgroundColor: 'white', 
    borderTopWidth: 0.5,
     borderColor: 'grey',
      height: 80, 
      elevation: 4,
      flexDirection: 'row'
  },
  buttonDiscountGroup:{
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center'
  }
};
