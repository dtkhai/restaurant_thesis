import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet,Text,Image,FlatList, TextInput} from 'react-native';
import QRCode from 'react-native-qrcode';
import styles from './styles';

export default class CashPayForm extends Component {
	constructor( props ) {
    super( props );
    this.state = {
      link: 'http://facebook.com/dtkhai'
    };
  }
  render() {
  	return (
  		<View style={{padding:5}}>
  			<View style={styles.center}>
	  			<QRCode
		          value={this.state.link}
		          size={120}
		          bgColor='#000'
		          fgColor='white'/>
		        <Text style={ [ styles.bold, { fontSize: 18 ,marginTop: 5} ] }>
					Use Payoo to scan
				</Text>
	        </View>
  		</View>
  	)  	
  }
}

