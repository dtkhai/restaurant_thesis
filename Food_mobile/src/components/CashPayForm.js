import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet,Text,TextInput} from 'react-native';
import styles from './styles';

export default class CashPayForm extends Component {
	constructor( props ) {
    super( props );
    this.state = {
      receiveCash: 0
    };
  }
  render() {
  	return (
  		<View style={{padding:5}}>
  			<TextInput
									style={[styles.formInput, styles.bold,{textAlign: 'right',height:40, fontSize: 18}]}
									underlineColorAndroid="transparent"							    
								    keyboardType='numeric'
								    placeholder='0'
								    onChangeText={(receiveCash) => this.setState({receiveCash})}
								    value={this.state.receiveCash}/>	
  			<View style={ styles.foodLabel }>
							        <View style={ { flex: 0.7 } }>
							            <Text style={ [ styles.bold, { fontSize: 18 } ] }>
							                Total
							            </Text>
							        </View>
							        <View style={ [ { flex: 0.3 } ] }>
							            <Text style={ [ styles.textRight, styles.bold, { fontSize: 18 }] } note>
							                {this.props.totalPrice} $
							            </Text>
							        </View>
			</View> 		
  			<View style={ styles.foodLabel }>
							        <View style={ { flex: 0.7 } }>
							            <Text style={ [ styles.bold, { fontSize: 18 } ] }>
							                Cash Received 
							            </Text>
							        </View>
							        <View style={ [ { flex: 0.3 } ] }>
							            <Text style={ [ styles.textRight, styles.bold, { fontSize: 18 }] } note>
							                {this.state.receiveCash} $
							            </Text>
							        </View>
			</View>
			<View style={ styles.foodLabel }>
							        <View style={ { flex: 0.7 } }>
							            <Text style={ [ styles.bold, { fontSize: 18 } ] }>
							                Return
							            </Text>
							        </View>
							        <View style={ [  { flex: 0.3 } ] }>
							            <Text style={ [ styles.textRight, styles.bold, { fontSize: 18 }] } note>
							                { (parseInt(this.state.receiveCash, 10) >= this.props.totalPrice) ?
												(parseInt(this.state.receiveCash, 10) - this.props.totalPrice) : 0} $
							            </Text>
							        </View>
			</View>  
  		</View>
  	)  	
  }
}

