import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Image, Text, Button, TouchableHighlight, Alert, TextInput } from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import Collapsible from 'react-native-collapsible';
import styles from './styles';

export default class FoodCheckItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 0,
            collapsed: true,
            hour: 0,
            minute: 0,
            second: 0,
            comment: this.props.comment
        };
    }
    _toggleExpanded = () => {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    componentDidMount() {

        if ((this.props.status == 0) || (this.props.status == 1)) {
            const tTime = new Date()
            var totalSeconds = Math.round((tTime.getTime() - this.props.time.getTime()) / 1000);


            var h = Math.floor(totalSeconds / 3600);

            var m = Math.floor(totalSeconds / 60) % 60;
            var s = totalSeconds % 60;

            if (h <= 9) {
                h = "0" + h
            }
            if (m <= 9) {
                m = "0" + m
            }
            if (s <= 9) {
                s = "0" + s
            }
            this.setState({
                hour: h,
                minute: m,
                second: s
            })

            setInterval(() => {
                const tTime = new Date()
                var totalSeconds = Math.round((tTime.getTime() - this.props.time.getTime()) / 1000);


                var h = Math.floor(totalSeconds / 3600);

                var m = Math.floor(totalSeconds / 60) % 60;
                var s = totalSeconds % 60;

                if (h <= 9) {
                    h = "0" + h
                }
                if (m <= 9) {
                    m = "0" + m
                }
                if (s <= 9) {
                    s = "0" + s
                }
                this.setState({
                    hour: h,
                    minute: m,
                    second: s
                })
            }, 1000)
        }
    }

    checkDone() {
        this.props.handlerFoodItem(this.props.id, 'done', 0);
    }
    showDetail() {
        this.props.handlerFoodItem(this.props.food_id, 'detail', 0);
    }
    editComment() {
        this.props.handlerFoodItem(this.props.id, 'editComment', this.state.comment);
    }
    renderStatus() {
        if (this.props.status == 0) {
            return (
                <View style={[styles.center, { flex: 1 }]}>
                    <Icon name="clock" color='#4479ff' size={15} />
                    <Text style={{ fontSize: 12 }}>
                        {this.state.hour}:{this.state.minute}:{this.state.second}
                    </Text>
                </View>
            )
        }
        else if (this.props.status == 1) {
            return (
                <View style={[styles.center, { flex: 1 }]}>
                    <Icon name="more-horizontal" color='#eac23c' size={15} />
                    <Text style={{ fontSize: 12 }}>
                        {this.state.hour}:{this.state.minute}:{this.state.second}
                    </Text>
                </View>
            )
        }
        else if (this.props.status == 2) {
            return (
                <View style={[styles.center, { flex: 1 }]}>
                    <Icon name="alert-circle" color='#cd51ff' size={15} />
                    <Text style={{ fontSize: 12, color: '#cd51ff', fontWeight: 'bold' }}>
                        Food Done
                    </Text>
                </View>
            )
        }
        else if (this.props.status == 3) {
            return (
                <View style={[styles.center, { flex: 1 }]}>
                    <Icon name="check" color='#30d13d' size={15} />
                    <Text style={{ fontSize: 12 }}>
                        Delivered
                    </Text>
                </View>
            )
        }
        else if ((this.props.status == 4) || (this.props.status == 5)) {
            return (
                <View style={[styles.center, { flex: 1 }]}>
                    <Icon name="x" color='#f44c41' size={15} />
                    <Text style={{ fontSize: 12, color: '#f44c41', fontWeight: "bold" }}>
                        Canceled
                    </Text>
                </View>
            )
        }

    }
    cancelFood() {
        this.props.handlerFoodItem(this.props.id, 'clear');
    }
    renderButton() {
        if (this.props.status == 2) {
            return (
                <TouchableOpacity onPress={() => { this.checkDone() }}>
                    <Icon name="check-square" size={25} color="#5A7587" />
                </TouchableOpacity>
            )
        }
        else if (this.props.status == 0) {
            return (
                <TouchableOpacity onPress={() => { this.cancelFood() }}>
                    <Icon name="x" size={25} color="#5A7587" />
                </TouchableOpacity>
            )
        }
    }
    renderButtonHide() {
        if (this.props.status == 2) {
            return (
                <TouchableOpacity onPress={() => { this.checkDone() }}>
                    <Icon name="check-square" size={25} color="#5A7587" />
                </TouchableOpacity>
            )
        }
    }
    render() {
        return (
            <TouchableOpacity style={{ margin: 0 }} onPress={this._toggleExpanded}>
                <View onPress={this._toggleExpanded} style={[styles.listItem, styles.borderBottom, { backgroundColor: '#eaf6f9' }]}>

                    <View style={[{ flex: 0.15, padding: 5, aspectRatio: 1 }, styles.centerImg, styles.horizontal]}>
                        <Image

                            source={{ uri: this.props.foodImg }}

                            style={[styles.foodThumnail, { flex: 1, width: 30 }]}
                        />
                    </View>

                    <View style={[{ justifyContent: 'center', flex: 0.4 }]}>
                        <View style={{ flex: 0.4, justifyContent: 'center' }}>
                            <Text style={{ fontWeight: "bold" }}>
                                {this.props.foodName}
                            </Text>
                        </View>
                        <View style={{ flex: 0.4, justifyContent: 'center' }}>
                            {(this.props.comment.length > 0) && <Icon name="message-square" size={13} color="#5A7587" />}
                        </View>
                    </View>

                    <View style={[styles.center, { flex: 0.3 }]}>
                        {this.renderStatus()}

                    </View>
                    <View style={[{ justifyContent: 'center', flex: 0.15 }]}>
                        <Text note>
                            x {this.props.amount}
                        </Text>
                    </View>

                </View>
                <Collapsible style={[{ flex: 1, padding: 5, backgroundColor: "#f9f9f9" }, styles.center]} collapsed={this.state.collapsed} align="center">
                    {!this.props.btnHide &&
                        <View style={[styles.horizontal, styles.center]}>
                            <View style={[{ flex: 1 / 3, }, styles.horizontal, styles.center]}>
                                {this.renderButton()}
                                <View style={[styles.center, { alignSelf: 'center', width: 10 }]}>

                                </View>
                                <TouchableOpacity onPress={() => { this.showDetail() }}>
                                    <Icon name="info" size={25} color="#5A7587" />
                                </TouchableOpacity>
                            </View>
                            <View style={[{ flex: 1 / 3, }]}>
                            </View>

                            <View style={[{ flex: 1 / 3, }, styles.horizontal, styles.center]}>

                            </View>
                        </View>
                    }
                    {this.props.btnHide &&
                        <View>
                            {this.renderButtonHide()}
                        </View>
                    }
                    { (this.props.status == 0)&&
                    <View style={{ alignSelf: 'stretch', marginTop: 5, borderRadius: 5, backgroundColor: 'lightgrey' }}>
                        <View style={styles.foodLabel}>
                            <View style={{ flex: 0.6 }}>
                                <Text style={{ margin: 5 }}>Note </Text>
                            </View>
                            {(this.state.comment != this.props.comment) &&
                                (<View style={[styles.horizontal, { flex: 0.4, justifyContent: 'flex-end' }]}>
                                    <View style={[{ backgroundColor: 'rgba(0,0,0,0.15)', margin: 1, marginRight: 3, borderRadius: 5, paddingHorizontal: 3 }, styles.center, styles.horizontal,]}>
                                        <TouchableOpacity onPress={() => {
                                            this.editComment()
                                        }}>
                                            <Icon name="check" size={20} color="#63915f" />
                                        </TouchableOpacity>
                                        <View style={[styles.center, { alignSelf: 'center', width: 10 }]}>

                                        </View>
                                        <TouchableOpacity onPress={() => { this.setState({ comment: this.props.comment }) }}>
                                            <Icon name="x" size={20} color="#b76161" />
                                        </TouchableOpacity>
                                    </View>
                                </View>)
                            }
                        </View>

                        <TextInput
                            style={[styles.formInput, { height: 60, marginHorizontal: 5, marginBottom: 5 }]}
                            multiline={true}
                            underlineColorAndroid="transparent"
                            numberOfLines={4}
                            textAlignVertical='top'
                            placeholder='Enter your comment on this...'
                            onChangeText={(comment) => this.setState({ comment })}
                            value={this.state.comment} />
                    </View>
                    }
                </Collapsible>
                {this.props.isPay && <Image style={[styles.paidIcon]} source={require('../assets/img/Paidicon.png')} />}
            </TouchableOpacity>
        )
    }
}