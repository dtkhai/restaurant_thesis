import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet, FlatList, Alert, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { } from 'native-base';
import styles from './styles';
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

@inject('notiStore')
@observer
export default class NotificationList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShow: false
        };
    }
    renderNoti = ({ item, index }) => {
        console.log(item)
        if(item.status==1)
        return (
            <TouchableOpacity onPress={()=>this.notiOnPress(item.id)} style={[styles.listItem, styles.borderBottom, { margin: 5 }]}>
                <Image style={{ height: 64, width: 64, borderRadius: 32 }} source={{uri: item.image}} />
                <View style={{ alignSelf: 'stretch', marginLeft: 10, justifyContent: 'center' }}>
                    <Text style={{ fontWeight: "bold" }}>{item.title}</Text>
                    <Text>{item.body}</Text>
                </View>
            </TouchableOpacity>
        )

    }

    renderNotiUnRead = ({ item, index }) => {
      
        if(item.status==0){
        return (
            <TouchableOpacity onPress={()=>this.notiOnPress(item.id)} style={[styles.listItem, styles.borderBottom, {backgroundColor: '#edf6f9', padding:5}]}>
                <Image style={{ height: 64, width: 64, borderRadius: 32 }} source={{uri: item.image}} />
                <View style={{ alignSelf: 'stretch', marginLeft: 10, justifyContent: 'center' }}>
                    <Text style={{ fontWeight: "bold" }}>{item.title}</Text>
                    <Text>{item.body}</Text>
                </View>
             
            </TouchableOpacity>
        )
        }
        else
        return(<View/>)
    }

    notiOnPress(id){
        this.props.notiStore.markRead(id)
    }
    renderUnread() {
        if (this.props.notiStore.haveUnread == true) {
            return (
                <View>
                    <View style={[{ backgroundColor: '#dddddd' }, styles.borderBottom]}>
                        <Text style={[{ paddingHorizontal: 7, paddingVertical: 3, color: 'gray', fontSize: 14 }]}>
                            UNREAD
                        </Text>
                    </View>
                    <FlatList data={this.props.notiStore.noti}
                        renderItem={this.renderNotiUnRead}
                        keyExtractor={(item, index) => index}
                        style={{ margin: 0, padding: 0 }} />
                </View>
            )
        }
        else
            return (<View />)
    }
    render() {

        if (this.props.notiStore.isShow) {
            return (
                <TouchableOpacity style={styles.overlay} onPress={() => { this.props.notiStore.toggleShow() }}>
                    <View style={{ zIndex: 10, backgroundColor: 'white', top: 40, right: 30, width: 450, elevation: 4 }}>
                        <View style={[styles.foodLabel, styles.borderBottom, { padding: 7, }]}>
                            <View style={{ flex: 0.5 }}>
                                <Text style={[styles.bold, { color: '#062E42', fontSize: 18 }]}>
                                    Notification
							</Text>
                            </View>
                            <View style={[{ flex: 0.5, }]}>
                                <TouchableOpacity onPress={()=>{this.props.notiStore.markReadAll()}}><Text style={styles.textRight}>Mark All as Read</Text></TouchableOpacity>
                            </View>
                        </View>
                        {this.renderUnread()}
                        <View style={[{ backgroundColor: '#dddddd' }, styles.borderBottom]}>
                            <Text style={[{ paddingHorizontal: 7, paddingVertical: 3, color: 'gray', fontSize: 14 }]}>
                                EARLIER
							</Text>
                        </View>
                        <FlatList data={this.props.notiStore.noti}
                            extraData={this.props.notiStore.noti}
                            renderItem={this.renderNoti}
                            keyExtractor={(item, index) => index}
                            style={{ margin: 0, padding: 0 }} />
                    </View>
                </TouchableOpacity>
            )
        }
        else {
            return (<View />)
        }
    }
}