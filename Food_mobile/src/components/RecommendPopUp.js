import React, { Component } from 'react';
import { Text, TouchableOpacity, View, StyleSheet, FlatList, Alert, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import RecommendItem from './RecommendItem'
import styles from './styles';

const testDB = [
  
]


export default class RecommendPopUp extends Component {
    constructor(props) {
        super(props);
        
        this.handler = this.handler.bind(this)
      
    }

    handler(e){
        this.props.handler(-1)
    }

   
    renderItem = ({ item }) => {
        return (
            <RecommendItem handler = {this.handler} food_id={item} />
        )
    }
    render() {
        return (

            <View style={[{ justifyContent: 'center', backgroundColor: 'white', borderTopWidth: 0.5, borderColor: 'grey', height: 80, elevation: 4 },
        (this.props.recomendedFood.length== 0)&&{height: 0}
        ]}>

                <Text style={{
                    marginHorizontal: 10,
                    textDecorationLine: "underline",
                    textDecorationStyle: "solid",
                    textDecorationColor: "grey",
                    fontWeight: 'bold'
                }}> Recommended Food </Text>
                
                  
                <FlatList data={this.props.recomendedFood}
                   
                    renderItem={this.renderItem}
                    numColumns={3}
                    keyExtractor={(item, index) => item.toString()}
                    style={{ margin: 0, padding: 0 }} />
                
            </View>

        )
    }
}