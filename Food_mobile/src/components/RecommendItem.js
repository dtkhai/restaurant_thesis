import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Image, Text, Button, TouchableHighlight } from 'react-native';

import Icon from 'react-native-vector-icons/Entypo';

import { getFoodByIdsServer } from '../networking/Server.js'
import styles from './styles';
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

@inject('orderStore')
@observer
export default class RecommendItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            food: {
                id: 1,
                name: "loading",
                main_image: "a",
                price: 0,
            },
            isLoaded: false,
        };
    }


    componentDidMount() {
        const temp = { "food_ids": this.props.food_id }

        getFoodByIdsServer(temp).then((data) => {
            if (data.length > 0) {
                this.setState({
                    food: data[0],
                })
            }
           
        })
    }
    increaseScore() {
        this.props.orderStore.Addfood(this.state.food.id, this.state.food.name, this.state.food.main_image);

        this.props.handler(-1)

    }

    renderPrice(){
        if(this.state.food.Discount){
          return(
            <View style={[styles.horizontal]}>
            <Text style={[{marginRight: 5,fontSize:12, fontWeight: 'bold', color: '#4a6184',textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} note>
                          {this.state.food.price} $
                          </Text>
            <Text style={[styles.textRight, styles.price, {color: '#e21d3b'}]} note>
                          {this.state.food.price - this.state.food.price * (this.state.food.Discount.percentage_value/100) - this.state.food.Discount.value}$
                          </Text>
              
            </View>
          )
        }
        else{
          return(<Text style={[styles.price, { fontSize: 14 }]}>
           {this.state.food.price} $
            </Text>)
        }
      }

    render() {

        return (
            <TouchableOpacity style={{ margin: 0, backgroundColor: 'white', height: 50, marginHorizontal: 10, flex: 1 / 3 }} onPress={this._toggleExpanded}>
                <View onPress={this._toggleExpanded} style={[styles.horizontal, this.state.number && styles.active]}>
                    <View style={[{ flex: 0.2, padding: 5, }, styles.centerImg, styles.horizontal]}>
                        <Image
                            source={{ uri: this.state.food.main_image }}
                            style={[styles.foodThumnail, { flex: 1, width: 60 }]}
                        />
                    </View>

                    <View style={[{ justifyContent: 'center', flex: 0.6 }]}>
                        <View style={{ flex: 0.4, justifyContent: 'center' }}>
                            <Text style={[styles.bold, { color: '#062E42', fontSize: 14 }]}>
                                {this.state.food.name}
                            </Text>
                        </View>
                        <View style={{ flex: 0.4, justifyContent: 'center' }}>
                       {this.renderPrice()}
                        </View>
                    </View>

                    <View style={[{ justifyContent: 'center', flex: 0.2 }]}>
                        <TouchableOpacity elevation={2} style={[styles.circleButton]} onPress={() => {
                            this.increaseScore()
                        }}>

                            <Icon name="plus" size={20} color="#5A7587" />

                        </TouchableOpacity>
                    </View>
                </View>

            </TouchableOpacity>
        )

    }
}