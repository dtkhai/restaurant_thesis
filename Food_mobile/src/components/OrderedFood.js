import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet} from 'react-native';
import { Content, Left, Icon, Right, Button, ListItem, Thumbnail, Text, Body } from 'native-base';
import FoodData from '../assets/data/FoodData.js' ;
import Collapsible from 'react-native-collapsible';
export default class Food extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      number: 0,
      collapsed: true
    };
  }
  increaseScore() {
    if ( this.state.number == 0 ) {
      this.props.handler( 1 );
    }
    this.setState( {
      number: this.state.number + 1
    } );
  }
  clearNum() {
    if ( this.state.number > 0 ) {
      this.props.handler( 0 );

      this.setState( {
        number: 0
      } );
    }
  }

  _toggleExpanded = () => {
    this.setState( {
      collapsed: !this.state.collapsed
    } );
  }

  render() {
    return (
    <TouchableOpacity style={ { margin: 0 } } onPress={ this._toggleExpanded }>
      <ListItem onPress={ this._toggleExpanded } style={ [ styles.normal, this.state.number && styles.active ] }>
        <Thumbnail square
                   size={ 80 }
                   source={ { uri: this.props.foodImg } } />
        <Body>
          <Text>
            { this.props.foodName }
          </Text>
          <Text note>
            { this.props.comment }
          </Text>
        </Body>
        <View>
          <Text>
            { this.props.amount }
          </Text>
          <Button onPress={ () => {
                              this.increaseScore()
                            } }>
            <Text>
              Edit
            </Text>
          </Button>
        </View>
      </ListItem>
      <Collapsible collapsed={ this.state.collapsed } align="center">
        <View>
          <Right>
            <Button transparent
                    dark
                    onPress={ () => {
                                this.clearNum()
                              } }>
              <Text>
                Clear
              </Text>
            </Button>
          </Right>
        </View>
      </Collapsible>
    </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create( {
  active: {
    backgroundColor: 'rgb(178, 255, 198)'
  },
  normal: {
    margin: 0
  }
}
);
