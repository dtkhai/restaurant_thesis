import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Image, ToastAndroid, Text, Button, TouchableHighlight, ImageBackground } from 'react-native';


import styles from './styles';
import Icon from 'react-native-vector-icons/Entypo';
import Collapsible from 'react-native-collapsible';

import DiscountBanner from './DiscountBanner'

import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

@inject('orderStore')
@observer

export default class Combo extends Component {

  showDetail() {
    this.props.handlerComboModal(this.props.id);
  }

  increaseScore() {
    this.props.orderStore.addCombo(this.props.id, this.props.name, this.props.main_image, this.props.foods);
    this.props.handler(-1);
  }


  decreaseScore() {
    this.props.orderStore.removeCombo(this.props.id);
    this.props.handler(-1);
  }

  clearNum() {
    this.props.orderStore.clearCombo(this.props.id);
    this.props.handler(-1);
  }



  renderDiscount = () => {
    if (this.props.discount) {
      return (
        <DiscountBanner discount={this.props.discount} />
      )
    }
  }
  renderPrice() {
    if (this.props.discount) {
      return (
        <View style={[styles.horizontal, { justifyContent: 'flex-end' }]}>
          <Text style={[{ marginRight: 5, fontSize: 12, fontWeight: 'bold', color: '#4a6184', textDecorationLine: 'line-through', textDecorationStyle: 'solid' }]} note>
            {this.props.price} $
                      </Text>
          <Text style={[styles.textRight, styles.price, { color: '#e21d3b' }]} note>
            {this.props.price - this.props.price * (this.props.discount.percentage_value / 100) - this.props.discount.value}$
                      </Text>

        </View>
      )
    }
    else {
      return (<Text style={[styles.textRight, styles.price]} note>
        {this.props.price} $
        </Text>)
    }
  }

  render() {
    const config = {
      velocityThreshold: 0.001,
      directionalOffsetThreshold: 1000
    };
    return (
      <View onPress={this._toggleExpanded} style={styles.cardContainer}>

        <View style={[styles.card, this.props.orderStore.currentOrder.combo_list.find(x => x.combo_id === this.props.id) && styles.active]}>
          <View style={{ flex: 0.7 }}>
            <TouchableOpacity style={[styles.centerImg, { borderRadius: 10, borderTopRightRadius: 10, overflow: 'hidden', }]}
              onPress={() => { this.showDetail() }}
            >
              <ImageBackground style={[styles.foodThumnail2, { justifyContent: 'space-between' }]} source={{ uri: this.props.main_image }} >
                <View style={{ flex: 0.7 }}>
                </View>

                <View style={[styles.foodLabel, { alignSelf: 'stretch', paddingHorizontal: 5, backgroundColor: 'rgba(255, 255, 255, 0.75)' }]}>
                  <View style={{ flex: 0.6 }}>
                    <Text style={[styles.bold, { color: '#062E42', fontSize: 18 }]}>
                      {this.props.name}
                    </Text>
                  </View>
                  <View style={[{ flex: 0.4 }]}>
                    {this.renderPrice()}
                  </View>
                </View>
                <Text style={{ position: 'absolute', left: 5, bottom: 20, elevation: 1, fontSize: 24, fontWeight: 'bold', color: 'red' }}>
                  COMBO
                      </Text>
              </ImageBackground>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 0.3, padding: 1 }}>
            <View style={styles.desciption}>
              <Text style={{ fontSize: 14, color: '#787f87' }}>
                {this.props.description}
              </Text>
            </View>
            <View style={[styles.center, styles.horizontal, styles.bottomButtonGroup]}>

              <View style={[{ flex: 2 / 3, }, styles.horizontal]}>
                <View style={[styles.buttonGroup1, styles.horizontal, styles.dropShadow]}>
                  <TouchableOpacity elevation={2} style={[styles.circleButton]} onPress={() => {
                    this.decreaseScore()
                  }}>

                    <Icon name="minus" size={20} color="#5A7587" />

                  </TouchableOpacity>
                  <View style={[styles.center, { alignSelf: 'center', width: 30 }]}>
                    <Text style={[styles.amountText, styles.center, { alignSelf: 'center' }]}>

                      {' '}{this.props.orderStore.currentOrder.combo_list.find(x => x.combo_id === this.props.id) ? this.props.orderStore.currentOrder.combo_list.find(x => x.combo_id === this.props.id).amount : 0} {' '}

                    </Text>
                  </View>
                  <TouchableOpacity elevation={2} style={[styles.circleButton]} onPress={() => {
                    this.increaseScore()
                  }}>

                    <Icon name="plus" size={20} color="#5A7587" />

                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flex: 1 / 3, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                <View style={{ flex: 1 }}>
                </View>
              
              </View>

            </View>
          </View>
        </View>



        {this.renderDiscount()}
        {
          (this.props.numOrdered > 0) &&
          (<ImageBackground style={styles.orderedFood} source={require('../assets/img/sticker.png')}>
            <Text style={{ color: 'white', fontWeight: 'bold' }}> x{this.props.numOrdered} Ordered </Text>

          </ImageBackground>)
        }
      </View>
    );
  }
}
