import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Text, Image, FlatList, TextInput,Alert } from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/Entypo';
import FoodDetailForm from './FoodDetailForm.js'
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";



@inject('orderStore')
@observer



export default class FoodDetail extends Component {
	constructor(props) {
		super(props);
		var temp = []
		temp.push(this.props.food.main_image) 
		this.state = {
			data: this.props.orderStore.foodListGet,
			numText: null,
			comment: null,
			allImgs: temp.concat(this.props.food.images),
			currentImg: 0,
		};
		this.handler = this.handler.bind(this)
	}
	componentDidMount() {
		var temp = 0
		var tempcom = null
		var a = this.state.data.find(x =>  {return((x.food_id == this.props.food.id) && (x.combo == null))})
		if (a) {
			temp = a.amount
			tempcom = a.comment
		}
		this.setState(
			{
				numText: temp,
				comment: tempcom,
			}
		)
	}


	handler(e, f) {
		if (e == 'num') {
			this.setState({
				numText: f
			})
		}
		else if (e == 'comment') {

			this.setState({
				comment: f
			})
		}
	}
	showDetail() {
		this.props.handlerModal(this.props.id);
	}
	saveDetail() {
		const a =''
		var tempComment = this.state.comment ? this.state.comment : a
		if (this.state.numText > 0) {
			this.props.orderStore.modifyFood(this.props.food.id, this.state.numText,tempComment ,this.props.food.name, this.props.food.main_image)
		}
		else {
			this.props.orderStore.clearFood(this.props.food.id);
		}
		this.props.handler(-1)
		this.props.handlerModal(this.props.id);
	}
	renderItem = ({ item, index }) => {
		return (
			<TouchableOpacity style={[{ padding: 5,alignSelf: 'stretch', flex: 1,aspectRatio: 1, backgroundColor: 'white' }]} onPress={()=>this.setState({currentImg: index})}>
				<Image style={[styles.foodThumnail, styles.foodImage]} source={{ uri: item }} />
			</TouchableOpacity>
		);
	}


	render() {
		return (
			<View style={styles.modalContent}>
				<View style={[styles.modalBody, { flex: 1 }]}>

					<View style={[styles.modalLeft, { borderColor: '#d6d7da', backgroundColor: 'white', borderRadius: 10, }]}>
						<View style={[{ padding: 5, margin: 5, backgroundColor: 'white', }, styles.centerImg]}>
							<Image style={styles.foodThumnail} source={{ uri: this.state.allImgs[this.state.currentImg] }} />
						</View>
						<View style={[{ flex: 1, margin: 5, alignSelf: 'stretch', backgroundColor: 'white', }, styles.centerImg]}>
							<FlatList data={this.state.allImgs}
								horizontal={true}
								renderItem={this.renderItem}

								keyExtractor={(item,index) => index}
								style={{ margin: 0, padding: 0 }} />
						</View>
					</View>
					<View style={[styles.modalRight, { paddingLeft: 20, paddingTop: 0, }]}>

						<FoodDetailForm handler={this.handler}
							numText={this.state.numText}
							comment={this.state.comment}
							name={this.props.food.name}
							price={this.props.food.price}
							description={this.props.food.description}
							discount={this.props.food.Discount}
						/>

						<View style={styles.modalFooter}>
						<TouchableOpacity style={[styles.modalButton2]} Full onPress={() => { this.showDetail() }}>
								<Text style={[styles.focusText, styles.white]}>DISCARD</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.modalButton} Full onPress={() => { this.saveDetail() }}>
								<Text style={[styles.focusText, styles.white]}>APPLY</Text>
							</TouchableOpacity>
							
						</View>
					</View>
				</View>
			</View>
		);
	}
}