/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry, Alert } from 'react-native';
import firebase from "react-native-firebase";
import { RemoteMessage } from 'react-native-firebase';
import bgMessaging from './src/bgMessaging';
import {notiStore} from './src/stores/index'

import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import App from "./src/App.js";
import Setup from "./src/boot/setup";


export default class Food_mobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
  }
  componentDidMount() {
   
    firebase.messaging().getToken()
      .then((token) => {
      
        console.log('TOKEEEEEEN: ' + token);
      });

    firebase.messaging().hasPermission()
      .then(enabled => {
        if (enabled) {
       
        } else {
         
          firebase.messaging().requestPermission()
            .then(() => {
              // User has authorised  
            })
            .catch(error => {
              // User has rejected permissions  
            });
        }
      });
      
      firebase.messaging().subscribeToTopic("All");

      this.notificationListener  = firebase.notifications().onNotification((notification) => {
        // You've received a notification that hasn't been displayed by the OS
        // To display it whilst the app is in the foreground, simply call the following
        notification.android.setChannelId('channelId')
        
        notification.android.setPriority(firebase.notifications.Android.Priority.High); 
        notification.android.setDefaults(firebase.notifications.Android.Defaults.All)
        notification.android.setVibrate(firebase.notifications.Android.Defaults.Vibrate)
        firebase.notifications().displayNotification(notification);
        console.log(notification)
        console.log('Data Cuanotifciontion' + JSON.stringify(notification.data))
        console.log('Data Cuanotifciontion' + JSON.stringify(notification.data.image))
        console.log('Data Cuanotifciontion' + JSON.stringify(notification.data.table))
        var test = {
          'id': notiStore.nextId,
          'title' : notification.title,
          'body' : notification.body,
          'table' : notification.data.id,
          "image" : notification.data.image,
          'status' : 0
        }
        
        notiStore.NewNoti(test)
      });
    this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
      // Process your token as required
    });
    this.messageListener = firebase.messaging().onMessage((message) => {
      Alert.alert('onMessage' + message);
    });
   
  }

  componentWillUnmount() {
    this.onTokenRefreshListener();
    this.messageListener();
    this.notificationListener();
  }
  render() {
    return (<Setup />);
  }
}

AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);