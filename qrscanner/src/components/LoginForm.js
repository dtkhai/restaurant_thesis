import React from 'react';
import { Alert, Platform, StyleSheet, Text, View, Button, TextInput, KeyboardAvoidingView } from 'react-native';
import {
    Hoshi,
} from 'react-native-textinput-effects';
class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chkbox_chk: false,
            email: 'cuong',
            password: '123456',
            isLoging: false,
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.card1}>
                    <Hoshi label={'Tên Ví điện tử/Số điện thoại'} style={styles.hoshiCustom}
                        inputStyle={{ color: 'white' }}
                        borderColor={'#85c5fa'} />
                </View>

                <View style={styles.card1}>
                    <Hoshi label={'Mật khẩu'} style={styles.hoshiCustom}
                        inputStyle={{ color: 'white' }}
                        borderColor={'#85c5fa'} />
                </View>

                <View style={styles.marginTop25}>
                    <Button
                        onPress={() => this.onLoginPress()}
                        title="Đăng nhập"
                        color="#9DC52F"
                    />
                </View>
            </View>
        );
    }

    onLoginPress() {
        this.props.handlerLogin();
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        width: 280,
    },

    marginTop25: {
        marginTop: 25
    },
    hoshiCustom: {
        borderColor: '#b76c94',
        color: 'white'
    },
    card1: {
        marginBottom: 10,
    },
});

export default LoginForm;
