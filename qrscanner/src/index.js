import { StackNavigator } from 'react-navigation';
import { Root } from 'native-base';
import Login from './screens/login.js';
import Home from './screens/home.js';
import React, { Component } from 'react';

const AppNavigator = StackNavigator(

    {
        Login: {
            screen: Login
        },
        Home: {
            screen: Home
        },
    },
    {
        initialRouteName: 'Login',
        headerMode: 'none',
        cardStyle: { backgroundColor: '#eaebed' },
    }
);

export default () => <Root>
    <AppNavigator />
</Root>;
