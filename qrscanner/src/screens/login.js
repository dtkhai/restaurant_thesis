import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ImageBackground, Dimensions } from 'react-native';
import LoginForm from '../components/LoginForm.js'

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.handlerLogin = this.handlerLogin.bind(this)
    }

    handlerLogin = () => this.props.navigation.navigate('Home');

    render() {

        return (
            <View style={styles.container}>
                    {/* <View style={styles.box}  > */}
                <ImageBackground source={require('../assets/img/loginbackground.png')}
                    style={styles.box}>

                    <View style={styles.formContainer}>
                        <LoginForm handlerLogin={this.handlerLogin} />
                    </View>
                    {/* </View> */}

                    <View style={styles.footer}>
                        <Text>Bạn chưa có ví điện tử ? Đăng ký ngay</Text>
                    </View>
                </ImageBackground>
            </View >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    logo: {
        width: 70,
        height: 70
    },
    title: {
        alignSelf: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#fff',
        marginTop: 35,
        textAlign: 'center',
    },
    box: {
        flex: 1,
        marginTop: 30,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%', height: '100%',
        backgroundColor: '#065597'
    },

    footer: {
        position: 'absolute',
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: '#065597',
        marginBottom: 30
    }
});

export default Login;
