import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ImageBackground, Dimensions } from 'react-native';
import { Header } from 'react-native-elements';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <View>
                <Header
                    placement="left"
                    leftComponent={
                    <Button
                        icon={
                            <Icon
                            name='arrow-right'
                            size={15}
                            color='white'
                          />
                        }
                      />

                    }
                    rightComponent={{ icon: 'heart', color: '#fff' }}
                />

                <View style={styles.container}>


                    <View style={styles.titleContainer}>
                    </View>
                    <View style={styles.box}  >
                        <View style={styles.formContainer}>
                            <Text>What are you doing?</Text>
                        </View>
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    logo: {
        width: 70,
        height: 70
    },
    title: {
        alignSelf: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#fff',
        marginTop: 35,
        textAlign: 'center',
    },
    box: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: 400,
        // backgroundColor: '#065597'
    },

    footer: {
        position: 'absolute',
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: '#065597',
        marginBottom: 30
    }
});

export default Home;