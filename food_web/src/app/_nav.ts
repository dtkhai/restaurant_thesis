export const navigation = [
  {
    name: 'Tables',
    url: '/tables',
    children: [
      {
        name: 'Floors',
        url: '/rooms',
        icon: 'icon-home',
      },
    ]
  },
  {
    name: 'Foods',
    url: '/foods',
    children: [
      {
        name: 'Categories',
        url: '/categories',
        icon: 'icon-layers',
      },
      {
        name: 'Foods',
        url: '/foods',
        icon: 'icon-book-open'
      },
      {
        name: 'Combo',
        url: '/combos',
        icon: 'icon-pencil'
      },
    ]
  },
  {
    name: 'Discounts',
    url: '/discounts',
    children: [
      {
        name: 'Discounts',
        url: '/discounts',
        icon: 'icon-diamond'
      },
      {
        name: 'New Discount',
        url: '/discounts/new',
        icon: 'icon-pencil'
      },
    ]
  },
  // {
  //   name: 'Order Histories',
  //   url: '/orders',
  //   icon: 'icon-wallet',
  // },
  {
    name: 'Users',
    url: '/users',
    icon: 'icon-user',
  },
  {
    name: 'Report',
    url: '/orders',
    icon: 'icon-chart',
  },
];

export const masterNavigation = [
  {
    name: 'Restaurants',
    url: '/restaurants',
    icon: 'icon-home',
  },
];
