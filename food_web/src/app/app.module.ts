import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {LocationStrategy, HashLocationStrategy, CommonModule} from '@angular/common';

import {AppComponent} from './app.component';

// Import containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

const APP_CONTAINERS = [
  FullLayoutComponent,
  SimpleLayoutComponent
]

// Import components
import {
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
} from './components';

const APP_COMPONENTS = [
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
]

// Import directives
import {
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
} from './directives';

const APP_DIRECTIVES = [
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
]

// Import routing module
import {AppRoutingModule} from './app.routing';

// Import 3rd party components
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {ChartsModule} from 'ng2-charts/ng2-charts';
import {FoodsComponent} from "./views/foods/foods.component";
import {OrdersComponent} from "./views/orders/orders.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BsDatepickerModule, ModalModule, TimepickerModule, TypeaheadModule} from "ngx-bootstrap";
import {RouterModule} from "@angular/router";
import {HttpClientModule} from '@angular/common/http';
import {ImageUploadModule} from "angular2-image-upload";
import {NgxPaginationModule} from "ngx-pagination";
import {ApiService} from "./common/api.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import {DiscountComponent} from "./views/discounts/discount.component";
import {AllUsersComponent} from "./views/users/all-users/all-users.component";
import {LoginComponent} from "./views/users/login/login.component";
import {AngularFireAuthModule} from "angularfire2/auth";
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AuthenticateService} from "./common/authenticate.service";
import {environment} from "../environments/environment";
import {AngularFireModule} from 'angularfire2';
import {KitchenComponent} from "./views/kitchen/kitchen.component";
import {TablesComponent} from "./views/tables/tables.component";
import {CompaniesComponent} from "./views/companies/companies.component";
import {QuillModule} from "ngx-quill";
import {EscapeHtmlPipe} from "./pipes/keep-html.pipe";
import {CategoriesComponent} from "./views/categories/categories.component";
import {NewDiscountComponent} from "./views/discounts/new_discount/new_discount.component";
import {AngularFireDatabase} from "angularfire2/database";
import {SetFoodsComponent} from "./views/foods/set_food/set_foods.component";
import {NewComboComponent} from "./views/foods/set_food/new_set/new_set_food.component";
import {EditComboComponent} from "./views/foods/set_food/edit_set/edit_set_food.component";
import {NgxSmoothDnDModule} from "../../libs/ngx-smooth-dnd/src/ngx-smooth-dnd.module";
import {TablePositionComponent} from "./views/tables/table_position.component";
import {RoomsComponent} from "./views/rooms/rooms.component";

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ImageUploadModule.forRoot(),
    TabsModule.forRoot(),
    NgxPaginationModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    QuillModule,
    TypeaheadModule.forRoot(),
    NgxSmoothDnDModule,
    ChartsModule
  ],
  declarations: [
    AppComponent,
    FoodsComponent,
    OrdersComponent,
    DiscountComponent,
    NewDiscountComponent,
    AllUsersComponent,
    LoginComponent,
    KitchenComponent,
    TablesComponent,
    CompaniesComponent,
    EscapeHtmlPipe,
    CategoriesComponent,
    SetFoodsComponent,
    NewComboComponent,
    EditComboComponent,
    TablePositionComponent,
    RoomsComponent,
    ...APP_CONTAINERS,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES
  ],
  providers: [ApiService, AuthenticateService, AngularFireDatabase],
  bootstrap: [AppComponent]
})
export class AppModule {
}
