import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';
import {FoodsComponent} from './views/foods/foods.component';
import {OrdersComponent} from './views/orders/orders.component';
import {DiscountComponent} from "./views/discounts/discount.component";
import {LoginComponent} from "./views/users/login/login.component";
import {AllUsersComponent} from "./views/users/all-users/all-users.component";
import {KitchenComponent} from "./views/kitchen/kitchen.component";
import {TablesComponent} from "./views/tables/tables.component";
import {CompaniesComponent} from "./views/companies/companies.component";
import {CategoriesComponent} from "./views/categories/categories.component";
import {NewDiscountComponent} from "./views/discounts/new_discount/new_discount.component";
import {SetFoodsComponent} from "./views/foods/set_food/set_foods.component";
import {NewComboComponent} from "./views/foods/set_food/new_set/new_set_food.component";
import {EditComboComponent} from "./views/foods/set_food/edit_set/edit_set_food.component";
import {TablePositionComponent} from "./views/tables/table_position.component";
import {RoomsComponent} from "./views/rooms/rooms.component";

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'kitchen',
    component: KitchenComponent,
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'categories',
        component: CategoriesComponent
      },
      {
        path: 'foods',
        component: FoodsComponent
      },
      {
        path: 'combos',
        component: SetFoodsComponent
      },
      {
        path: 'combos/new',
        component: NewComboComponent
      },
      {
        path: 'combos/:id/edit',
        component: EditComboComponent
      },
      {
        path: 'orders',
        component: OrdersComponent
      },
      {
        path: 'discounts',
        component: DiscountComponent,
      },
      {
        path: 'discounts/new',
        component: NewDiscountComponent
      },
      {
        path: 'users',
        component: AllUsersComponent
      },
      {
        path: 'rooms',
        component: RoomsComponent,
      },
      {
        path: 'rooms/:id',
        component: TablesComponent,
        children: [
          {path: 'tables', component: TablesComponent},
        ]
      },
      {
        path: 'rooms/:id/table-position',
        component: TablePositionComponent,
      },
      {
        path: 'restaurants',
        component: CompaniesComponent
      },
      {
        path: 'restaurants/:id/users',
        component: AllUsersComponent
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
