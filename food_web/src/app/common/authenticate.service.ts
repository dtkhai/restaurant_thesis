import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {SessionData} from '../models/session.data.model';
import {HttpRequest} from "@angular/common/http";

@Injectable()
export class AuthenticateService {
  cachedRequests: Array<HttpRequest<any>> = [];
  private _sessionObject = new BehaviorSubject<SessionData>(null);
  redirectUrl: string;

  constructor() {
    const token = localStorage.getItem('token');
    if (token != null) {
      const userId = +localStorage.getItem('userId'); // put + to convert string to number
      const full_name = localStorage.getItem('full_name');
      const username = localStorage.getItem('username');
      const role_id = +localStorage.getItem('role_id');
      const password = localStorage.getItem('password');
      const email = localStorage.getItem('email');
      const company_id = +localStorage.getItem('company_id');

      const sessionData = new SessionData(userId, username, full_name, token, role_id, password, email, company_id);
      this.setSession(sessionData);
    }
  }

  setSession(val: SessionData) {
    // save data into local storage
    localStorage.setItem('userId', val.userId.toString());
    localStorage.setItem('token', val.token);
    localStorage.setItem('full_name', val.full_name);
    localStorage.setItem('username', val.username);
    localStorage.setItem('role_id', val.role_id.toString());
    localStorage.setItem('password', val.password);
    localStorage.setItem('email', val.email);
    if (val.company_id) {
      localStorage.setItem('company_id', val.company_id.toString());
    }

    this._sessionObject.next(val);
  }

  getSession() {
    return this._sessionObject;
  }

  clearSession() {
    localStorage.clear();
    this._sessionObject.next(null);
  }

  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

  public retryFailedRequests(): void {
    // retry the requests. this method can
    // be called after the token is refreshed
  }
}
