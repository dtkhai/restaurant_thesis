export class AppConstants {
  public static API_URL = 'http://localhost:8765/';
  // public static API_URL = 'https://hidden-island-91165.herokuapp.com/';

  public static ROLES = {
    'master': 0,
    'admin': 1,
    'manager': 2,
    'accountant': 3,
    'receptionist': 4,
    'chef': 5,
  }

  public static DISPLAY_ROLES = {
    1: 'Administrator',
    2: 'Manager',
    3: 'Accountant',
    4: 'Receptionist',
    5: 'Chef'

  }

  public static FOOD_STATUS = {
    'WAITING': 0,
    'COOKING': 1,
  }
}
