import {Injectable} from '@angular/core';
import {URLSearchParams, Response, Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
import {AppConstants} from './const';
import {HttpClient} from '@angular/common/http';

/*
  Generated class for the Api provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ApiService {
  apiUrl = AppConstants.API_URL;

  private API_URL = this.apiUrl;

  constructor(private http: HttpClient) {
  }

  private createUrl(path: string): string {
    return `${this.API_URL}${path}`;
  }

  public get(path: string,  body?: any): Observable<any> {
    return this.http.get(this.createUrl(path));
  }

  public post(path: string, body?: any) {
    return this.http.post(this.createUrl(path), body);
  }

  public put(path: string, body?: any) {
    return this.http.put(this.createUrl(path), body);
  }

  public delete(path: string, body?: any) {
    return this.http
      .delete(this.createUrl(path), body);
  }
}
