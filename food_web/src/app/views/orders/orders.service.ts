import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from '../../common/api.service';

@Injectable()
export class OrdersService {
  path: string;

  constructor(private _api: ApiService) {
  }

  init(companyId) {
    this.path = 'restaurants/' + companyId + '/';
  }

  public getOrders(chosen_year, chosen_month = 0): Observable<any> {
    const params = '?chosen_year=' + chosen_year + '&chosen_month=' + chosen_month;
    const ordersUrl = this.path + 'order-histories';
    return this._api.get(ordersUrl + params);
  }
}
