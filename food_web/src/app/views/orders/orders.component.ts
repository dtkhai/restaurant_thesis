import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {OrdersService} from "../orders/orders.service";
import {AuthenticateService} from "../../common/authenticate.service";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {SessionData} from "../../models/session.data.model";

declare var $: any;

const columnNames = ['Lorem', 'Ipsum', 'Consectetur', 'Eiusmod'];

const cardColors = ['azure', 'beige', 'bisque', 'blanchedalmond', 'burlywood', 'cornsilk', 'gainsboro', 'ghostwhite', 'ivory', 'khaki'];

const pickColor = () => {
  // const rand = Math.floor((Math.random() * 10));
  return cardColors[4];
};

@Component({
  selector: 'app-orders',
  templateUrl: 'orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [OrdersService]
})

export class OrdersComponent implements OnInit {
  private session: SessionData;

  orders = [];
  currentTime = new Date();
  year = this.currentTime.getFullYear();
  month = this.currentTime.getMonth() + 1;
  years = [
    {key: 2018, value: "2018"},
  ];
  months = [
    {key: 0, value: "All"},
    {key: 1, value: "1"},
    {key: 2, value: "2"},
    {key: 3, value: "3"},
    {key: 4, value: "4"},
    {key: 5, value: "5"},
    {key: 6, value: "6"},
    {key: 7, value: "7"},
    {key: 8, value: "8"},
    {key: 9, value: "9"},
    {key: 10, value: "10"},
    {key: 11, value: "11"},
    {key: 12, value: "12"},
  ];

  constructor(private ordersService: OrdersService, private router: Router, private toastr: ToastrService,
              private _authService: AuthenticateService, private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.ordersService.init(session_data.company_id);
      this.buildChart();
    });
  }

  // barChart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
  public barChartType = 'line';
  public barChartLegend = true;

  public barChartData: any[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Total Price ($)'},
  ];

  buildChart() {
    var data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    this.ordersService.getOrders(this.year, this.month).subscribe(result => {
      if (result.success) {
        console.log(result);
        if (this.month == 0) {
          data = [];
          this.barChartLabels = [];
          for (var i = 0; i < 12; ++i) {
            data.push(0);
            this.barChartLabels.push((i + 1).toString());
          }
          this.cdRef.detectChanges();

          this.orders = result.data.orders;
          for (var order of this.orders) {
            var date = new Date(order.created_at);
            var month = date.getMonth();
            data[month] += order.total_price;
          }
          this.barChartData = [
            {data: data, label: 'Total Price ($)'},
          ];
        } else {
          data = [];
          this.barChartLabels = [];
          for (var i = 0; i < 30; ++i) {
            data.push(0);
            this.barChartLabels.push((i + 1).toString());
          }
          this.cdRef.detectChanges();

          this.orders = result.data.orders;
          for (var order of this.orders) {
            var date = new Date(order.created_at);
            var date_order = date.getDate();
            data[date_order] += order.total_price;
          }
          this.barChartData = [
            {data: data, label: 'Total Price ($)'},
          ];

        }

      } else {
        this.orders = [];
      }
    }, error => {
      console.log(error);
    });
  }
}
