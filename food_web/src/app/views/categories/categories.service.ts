import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from '../../common/api.service';

@Injectable()
export class CategoriesService {
  path: string;

  constructor(private _api: ApiService) {
  }

  init(companyId) {
    this.path = 'restaurants/' + companyId + '/';
  }

  public getGroups(currentPage: number = 1, pageSize: number = 5): Observable<any> {
    const params = '?current_page=' + currentPage + '&page_size=' + pageSize;
    const food_groupsUrl = 'groups';
    return this._api.get(this.path + food_groupsUrl + params);
  }

  public createGroup(body) {
    const food_groupsUrl = 'groups';
    return this._api.post(this.path + food_groupsUrl, body);
  }

  public deleteGroup(groupId) {
    const food_groupsUrl = 'groups/' + groupId;
    return this._api.delete(this.path + food_groupsUrl);
  }

  public getGroup(groupId): Observable<any> {
    const food_groupsUrl = 'groups/';
    return this._api.get(this.path + food_groupsUrl + groupId);
  }

  public updateGroup(groupId, body) {
    const food_groupsUrl = 'groups/' + groupId;
    return this._api.put(this.path + food_groupsUrl, body);
  }
}
