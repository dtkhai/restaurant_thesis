import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {CategoriesService} from './categories.service';
import {AuthenticateService} from '../../common/authenticate.service';
import {SessionData} from '../../models/session.data.model';

declare var $: any;

@Component({
  selector: 'app-companies',
  templateUrl: 'categories.component.html',
  providers: [CategoriesService]

})

export class CategoriesComponent implements OnInit {
  groups = [];
  group = {};
  totalItems = 0;
  searchForm: FormGroup;
  categoryForm: FormGroup;
  currentPage = 1;
  pageSize = 10;
  selectedCategoryId: number;
  private session: SessionData;

  @ViewChild('deleteModal') deleteModal: ModalDirective;
  @ViewChild('addNewGroupModal') addNewGroupModal: ModalDirective;
  @ViewChild('editGroupModal') editGroupModal: ModalDirective;


  constructor(private categoriesService: CategoriesService, private fb: FormBuilder, private router: Router,
              private toastr: ToastrService, private _authService: AuthenticateService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.categoriesService.init(session_data.company_id);
    });
  };

  ngOnInit() {
    this.createForm();
    this.getCategories(this.currentPage, this.pageSize);
  }

  createForm() {
    this.categoryForm = this.fb.group({
      name: [null, Validators.required],
    });

    this.searchForm = this.fb.group({
      'search_string': null
    });
  }

  addGroup(data) {
    this.categoriesService.createGroup(data).subscribe(result => {
        this.closeAddNewGroupModal();
        this.toastr.success('Created successfully', 'Category');
        this.getCategories(this.currentPage, this.pageSize);
      },
      error => {
        console.log(error);
      });
  }

  updateCategory(data) {
    this.categoriesService.updateGroup(this.selectedCategoryId, data).subscribe(result => {
      this.closeEditGroupModal();
      this.toastr.success('Updated successfully', 'Category');
      this.getCategories(this.currentPage, this.pageSize);
    });
  }

  getGroup(categoryId) {
    this.categoriesService.getGroup(categoryId).subscribe(result => {
      if (result.success) {
        this.group = result.data;
      } else {
        this.group = {};
      }
    }, error => {
      console.log(error);
    });
  }

  deleteGroup(id) {
    this.categoriesService.deleteGroup(id).subscribe(result => {
      this.closeDeleteModal();
      this.toastr.success('Deleted successfully', 'Category');
      this.getCategories(this.currentPage, this.pageSize);
    });
  }

  getCategories(currentPage, pageSize) {
    this.categoriesService.getGroups(currentPage, pageSize).subscribe(result => {
      if (result.success) {
        this.groups = result.data.groups;
      } else {
        this.groups = [];
      }
    }, error => {
      console.log(error);
    });
  }

// ----------------------------------------Open/ Close modal function---------------------------------


  openAddNewGroupModal() {
    this.categoryForm.reset();
    this.addNewGroupModal.show();
  }

  closeAddNewGroupModal() {
    this.addNewGroupModal.hide();
  }

  openEditGroupModal(categoryId) {
    this.selectedCategoryId = categoryId;
    this.getGroup(categoryId);
    this.editGroupModal.show();
  }

  closeEditGroupModal() {
    this.editGroupModal.hide();
    this.selectedCategoryId = null;
    this.group = {};
  }

  openDeleteModal(foodId) {
    this.selectedCategoryId = foodId;
    this.deleteModal.show();
  }

  closeDeleteModal() {
    this.deleteModal.hide();
  }
}
