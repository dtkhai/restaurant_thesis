import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {TablesService} from './tables.service';
import {ToastrService} from 'ngx-toastr';
import {SessionData} from '../../models/session.data.model';
import {AuthenticateService} from '../../common/authenticate.service';
import {RoomsService} from '../rooms/rooms.service';

declare var $: any;

@Component({
  selector: 'app-tables',
  templateUrl: 'tables.component.html',
  providers: [TablesService, RoomsService]
})

export class TablesComponent implements OnInit {
  tables = [];
  table = {};
  rooms = [];
  room = {};
  private currentPage = 1;
  private pageSize = 10;
  selectedTableId: number;
  selectedRoomId: number;
  totalTables: number;
  rForm: FormGroup;
  groupForm: FormGroup;
  searchForm: FormGroup;
  // frequentCapacity = 4;
  // prefix: string;
  @ViewChild('addNewTableModal') addNewTableModal: ModalDirective;
  @ViewChild('editTableModal') editTableModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;
  session: SessionData;

  constructor(private tablesService: TablesService, private fb: FormBuilder, private router: Router, private toastr: ToastrService,
              private _authService: AuthenticateService, private route: ActivatedRoute, private roomsService: RoomsService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.roomsService.init(session_data.company_id);
      this.tablesService.init(session_data.company_id);
    });
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getTables(this.currentPage, this.pageSize, +params.id);
      this.selectedRoomId = +params.id;
      this.getRoom(+params.id);
    });

    this.createFrom();
  }

  getRoom(roomId) {
    this.roomsService.getRoom(roomId).subscribe(result => {
      if (result.success) {
        this.room = result.data;
      } else {
        this.room = {};
      }
    }, error => {
      console.log(error);
    });
  }

  createFrom() {
    this.rForm = this.fb.group({
      'name': [null, Validators.required],
      'capacity': [null, Validators.required],
    });

    this.searchForm = this.fb.group({
      'search_string': [null]
    });
  }


  getTables(currentPage, pageSize, roomId) {
    this.tablesService.getTables(currentPage, pageSize, roomId).subscribe(result => {
      if (result.success) {
        this.tables = result.data.tables;
        // if (this.tables && this.tables.length > 0) {
        //   this.frequentCapacity = this.tables[this.tables.length - 1].capacity;
        // }
        this.totalTables = result.data.total_tables;
      } else {
        this.tables = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getTable(tableId) {
    this.tablesService.getTable(tableId).subscribe(result => {
      if (result.success) {
        this.table = result.data;
      } else {
        this.table = {};
      }
    }, error => {
      console.log(error);
    });
  }

  addTable(data) {
    data.room_id = this.selectedRoomId;
    this.tablesService.createTable(data).subscribe(result => {
        this.closeAddNewTableModal();
        this.toastr.success('Created successfully', 'Table');
        this.getTables(this.currentPage, this.pageSize, this.selectedRoomId);
      },
      error => {
        console.log(error);
      });
  }

  // addATable() {
  //   const data = {
  //     room_id: this.selectedRoom.id,
  //     total: 1,
  //     capacity: this.frequentCapacity
  //   };
  //   this.tablesService.createTable(data).subscribe(result => {
  //       this.closeAddNewTableModal();
  //       this.toastr.success('Created successfully', 'Table');
  //       this.getTables(this.currentPage, this.pageSize, this.selectedRoom.id);
  //     },
  //     error => {
  //       console.log(error);
  //     });
  // }

  deleteTable(id) {
    this.tablesService.deleteTable(id).subscribe(result => {
      this.closeDeleteModal();
      this.toastr.success('Deleted successfully', 'Table');
      this.getTables(this.currentPage, this.pageSize, this.selectedRoomId);
    });
  }

  updateTable(body) {
    this.tablesService.updateTable(this.selectedTableId, body).subscribe(result => {
      this.toastr.success('Updated successfully', 'Table');
      this.getTables(this.currentPage, this.pageSize, this.selectedRoomId);
      this.closeEditTableModal();
    });
  }

  // addGroup(data) {
  //   this.tablesService.createRoom(data).subscribe(result => {
  //       this.closeAddNewGroupModal();
  //       this.toastr.success('Created successfully', 'Room');
  //       // this.getFoods(this.currentPage, this.pageSize);
  //       this.getRooms();
  //     },
  //     error => {
  //       console.log(error);
  //     });
  // }

  // getRooms() {
  //   this.tablesService.getRooms().subscribe(result => {
  //     if (result.success) {
  //       this.rooms = result.data.rooms;
  //       this.selectedRoom = this.rooms[0];
  //       if (this.selectedRoom) {
  //         this.getTables(this.currentPage, this.pageSize, this.selectedRoom.id);
  //       }
  //     } else {
  //       this.rooms = [];
  //     }
  //   }, error => {
  //     console.log(error);
  //   });
  // }

  // onSelect(room) {
  //   this.selectedRoom = room;
  //   this.getTables(this.currentPage, this.pageSize, this.selectedRoom.id);
  // }

  navigateToTableArragement() {
    this.router.navigate(['rooms', this.selectedRoomId, 'table-position']);
  }

  // ----------------------------------------Open/ Close modal function---------------------------------

  openAddNewTableModal() {
    this.rForm.reset();
    this.addNewTableModal.show();
  }

  closeAddNewTableModal() {
    this.addNewTableModal.hide();
  }

  openEditTableModal(tableId) {
    this.selectedTableId = tableId;
    this.getTable(tableId);
    this.editTableModal.show();
  }

  closeEditTableModal() {
    this.editTableModal.hide();
    this.selectedTableId = null;
  }

  openDeleteModal(tableId) {
    this.selectedTableId = tableId;
    this.deleteModal.show();
  }

  closeDeleteModal() {
    this.deleteModal.hide();
  }


}
