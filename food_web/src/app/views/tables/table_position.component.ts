import {Component, OnInit} from '@angular/core';
import {applyDrag, generateItems} from './utils';
import {TablesService} from "../tables/tables.service";
import {AuthenticateService} from "../../common/authenticate.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {FormBuilder} from "@angular/forms";
import {SessionData} from "../../models/session.data.model";

declare var $: any;

const columnNames = ['Lorem', 'Ipsum', 'Consectetur', 'Eiusmod'];

const cardColors = ['azure', 'beige', 'bisque', 'blanchedalmond', 'burlywood', 'cornsilk', 'gainsboro', 'ghostwhite', 'ivory', 'khaki'];

const pickColor = () => {
  // const rand = Math.floor((Math.random() * 10));
  return cardColors[4];
};

@Component({
  selector: 'app-tables-position',
  templateUrl: 'table_position.component.html',
  styleUrls: ['./table_position.component.css'],
  providers: [TablesService]
})

export class TablePositionComponent implements OnInit {
  tables = [];
  totalTables: number;
  session: SessionData;
  line = 1;

  scene = {
    type: 'container',
    props: {
      orientation: 'horizontal'
    },
    children: generateItems(this.line, (i) => ({
      id: `column${i}`,
      type: 'container',
      name: columnNames[i],
      props: {
        orientation: 'horizontal',
        className: 'card-container'
      },
      children: []
    }))
  };

  // items = generateItems(50, i => ({data: 'Draggable ' + i}));

  constructor(private tablesService: TablesService, private fb: FormBuilder, private router: Router, private toastr: ToastrService,
              private _authService: AuthenticateService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.tablesService.init(session_data.company_id);
    });

    this.route.params.subscribe(params => {
      this.getTables(1, 10, +params.id);
    });
  }

  getTables(currentPage, pageSize, roomId) {
    this.tablesService.getTables(currentPage, pageSize, roomId).subscribe(result => {
      if (result.success) {
        this.tables = result.data.tables;
        this.totalTables = result.data.total_tables;

        this.scene = {
          type: 'container',
          props: {
            orientation: 'horizontal'
          },
          children: generateItems(this.line, (i) => ({
            id: `column${i}`,
            type: 'container',
            name: 'Line ' + (i + 1),
            props: {
              orientation: 'vertical',
              className: 'card-container'
            },
            children: generateItems(this.isFirstColumn(i), (j) => ({
              type: 'draggable',
              id: `${i}${j}`,
              props: {
                className: 'card',
                style: {backgroundColor: pickColor()}
              },
              tableName: this.tables[j].name,
              tableId: this.tables[j].id
            }))
          }))
        }
      } else {
        this.tables = [];
      }
    }, error => {
      console.log(error);
    });
  }

  createColumn() {
    this.scene = {
      type: 'container',
      props: {
        orientation: 'horizontal'
      },
      children: generateItems(this.line, (i) => ({
        id: `column${i}`,
        type: 'container',
        name: 'Line ' + (i + 1),
        props: {
          orientation: 'vertical',
          className: 'card-container'
        },
        children: generateItems(this.isFirstColumn(i), (j) => ({
          type: 'draggable',
          id: `${i}${j}`,
          props: {
            className: 'card',
            style: {backgroundColor: pickColor()}
          },
          tableName: this.tables[j].name,
          tableId: this.tables[j].id
        }))
      }))
    }
  }

  updateTablePostion() {
    let table_map = [];
    console.log(this.scene);
    for (let i = 0; i < this.scene.children.length; ++i) {
      for (let j = 0; j < this.scene.children[i].children.length; ++j) {
        const table = {
          'column': i,
          'row': j,
          'table_id': this.scene.children[i].children[j].tableId
        };

        table_map.push(table);
      }
    }

    let body = {
      'table_map': table_map
    };
    console.log(body);

    this.tablesService.updateTablePosition(body).subscribe(result => {
        this.toastr.success('Updated position successfully', 'Table');
      },
      error => {
        console.log(error);
      });
  }

  isFirstColumn(column) {
    if (column === 0) {
      return this.totalTables;
    } else {
      return 0;
    }
  }

  onColumnDrop(dropResult) {
    const scene = Object.assign({}, this.scene);
    scene.children = applyDrag(scene.children, dropResult);
    this.scene = scene;
  }

  onCardDrop(columnId, dropResult) {
    if (dropResult.removedIndex !== null || dropResult.addedIndex !== null) {
      const scene = Object.assign({}, this.scene);
      const column = scene.children.filter(p => p.id === columnId)[0];
      const columnIndex = scene.children.indexOf(column);

      const newColumn = Object.assign({}, column);
      newColumn.children = applyDrag(newColumn.children, dropResult);
      scene.children.splice(columnIndex, 1, newColumn);

      this.scene = scene;
    }
  }

  getCardPayload(columnId) {
    return (index) => {
      return this.scene.children.filter(p => p.id === columnId)[0].children[index];
    }
  }

  log(...params) {
    console.log(...params);
  }
}
