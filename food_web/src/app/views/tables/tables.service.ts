import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from '../../common/api.service';

@Injectable()
export class TablesService {
  path: string;

  constructor(private _api: ApiService) {
  }

  init(companyId) {
    this.path = 'restaurants/' + companyId + '/';
  }

  public getTables(currentPage: number = 1, pageSize: number = 5, roomId): Observable<any> {
    const params = '?current_page=' + currentPage + '&page_size=' + pageSize + '&room_id=' + roomId;
    return this._api.get(this.path + 'tables' + params);
  }

  public getTable(tableId): Observable<any> {
    const tablesUrl = this.path + 'tables/';
    return this._api.get(tablesUrl + tableId);
  }

  public createTable(body) {
    const tablesUrl = this.path + 'tables';
    return this._api.post(tablesUrl, body);
  }

  public updateTable(tableId, body) {
    const tablesUrl = this.path + 'tables/' + tableId;
    return this._api.put(tablesUrl, body);
  }

  public updatePrefixName(prefix, roomId) {
    const body = {
      prefix: prefix,
      room_id: roomId
    };

    const tablesUrl = this.path + 'tables/prefix';
    return this._api.post(tablesUrl, body);
  }

  public updateTablePosition(body) {
    const tablesUrl = this.path + 'tables/update-position';
    return this._api.post(tablesUrl, body);
  }


  public deleteTable(tableId) {
    const tablesUrl = this.path + 'tables/' + tableId;
    return this._api.delete(tablesUrl);
  }
}
