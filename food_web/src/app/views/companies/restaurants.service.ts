import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from '../../common/api.service';

@Injectable()
export class RestaurantsService {

  constructor(private _api: ApiService) {
  }

  public getRestaurants(currentPage: number = 1, pageSize: number = 5): Observable<any> {
    const params = '?current_page=' + currentPage + '&page_size=' + pageSize;
    const restaurantsUrl = 'restaurants';
    return this._api.get(restaurantsUrl + params);
  }

  public getRestaurant(restaurantId): Observable<any> {
    const restaurantsUrl = 'restaurants/';
    return this._api.get(restaurantsUrl + restaurantId);
  }

  public searchRestaurants(search_string, currentPage: number = 1, pageSize: number = 5): Observable<any> {
    const params = '?search_string=' + search_string + '&current_page=' + currentPage + '&page_size=' + pageSize;
    const restaurantsUrl = 'restaurants/search';
    return this._api.get(restaurantsUrl + params);
  }

  public createRestaurant(body) {
    const restaurantsUrl = 'restaurants';
    return this._api.post(restaurantsUrl, body);
  }

  public updateRestaurant(restaurantId, body) {
    const restaurantsUrl = 'restaurants/' + restaurantId;
    return this._api.put(restaurantsUrl, body);
  }

  public deleteRestaurant(restaurantId) {
    const restaurantsUrl = 'restaurants/' + restaurantId;
    return this._api.delete(restaurantsUrl);
  }
}
