import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {RestaurantsService} from './restaurants.service';

declare var $: any;

@Component({
  selector: 'app-companies',
  templateUrl: 'companies.component.html',
  providers: [RestaurantsService]

})

export class CompaniesComponent implements OnInit {
  companies = [];
  company = {};

  totalItems = 0;
  companyForm: FormGroup;
  searchForm: FormGroup;
  selectedRestaurantId;
  currentPage = 1;
  pageSize = 10;
  main_image_url: string;

  @ViewChild('addNewRestaurantModal') addNewRestaurantModal: ModalDirective;
  @ViewChild('editRestaurantModal') editRestaurantModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;

  constructor(private companiesService: RestaurantsService, private fb: FormBuilder, private router: Router,
              private toastr: ToastrService) {

    this.searchForm = this.fb.group({
      'search_string': null
    });
  };

  ngOnInit() {
    this.createForm();
    this.getRestaurants(this.currentPage, this.pageSize);
  }

  createForm() {
    this.companyForm = this.fb.group({
      company: this.fb.group({
        name: [null, Validators.required],
        description: null,
        image: null,
        country: null,
        district: null,
        address: null,
      })
    });
  }


  searchRestaurants(data) {
    this.companiesService.searchRestaurants(data.search_string).subscribe(result => {
      this.companies = result.data.companies;
      this.totalItems = result.data.total_companies;
    });
  }

  // filterRestaurant(group) {
  //   this.title = group.name;
  //   this.companiesService.filterRestaurants(group.id).subscribe(result => {
  //     this.companies = result.data.companies;
  //     this.totalItems = result.data.total_companies;
  //   });
  // }

  getRestaurants(currentPage, pageSize) {
    this.companiesService.getRestaurants(currentPage, pageSize).subscribe(result => {
      if (result.success) {
        this.companies = result.data.companies;
        this.totalItems = result.data.total_companies / 2;
      } else {
        this.companies = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getRestaurant(companyId) {
    this.companiesService.getRestaurant(companyId).subscribe(result => {
      if (result.success) {
        this.company = result.data;
        console.log(this.company);

      } else {
        this.company = {};
      }
    }, error => {
      console.log(error);
    });
  }

  addRestaurant(data) {
    console.log(data);
    this.companiesService.createRestaurant(data).subscribe(result => {
        this.closeAddNewRestaurantModal();
        this.toastr.success('Created successfully', 'Restaurant');
        this.getRestaurants(this.currentPage, this.pageSize);
      },
      error => {
        console.log(error);
      });
  }

  deleteRestaurant(id) {
    this.companiesService.deleteRestaurant(id).subscribe(result => {
        this.closeDeleteModal();
        this.toastr.success('Deleted successfully', 'Restaurant');
        this.getRestaurants(this.currentPage, this.pageSize);
      },
      error => {
        console.log(error);
      });
  }

  updateRestaurant(data) {
    this.companiesService.updateRestaurant(this.selectedRestaurantId, data).subscribe(result => {
      this.closeEditRestaurantModal();
      this.toastr.success('Updated successfully', 'Restaurant');
      this.getRestaurants(this.currentPage, this.pageSize);
    });
  }

// ----------------------------------------Open/ Close modal function---------------------------------


  openAddNewRestaurantModal() {
    this.companyForm.reset();
    this.addNewRestaurantModal.show();
  }

  closeAddNewRestaurantModal() {
    this.addNewRestaurantModal.hide();
  }

  openEditRestaurantModal(companieId) {
    this.selectedRestaurantId = companieId;
    this.getRestaurant(companieId);
    this.editRestaurantModal.show();
  }

  closeEditRestaurantModal() {
    this.editRestaurantModal.hide();
    this.selectedRestaurantId = null;
    this.company = {};
  }

  openDeleteModal(companyId) {
    this.selectedRestaurantId = companyId;
    this.deleteModal.show();
  }

  closeDeleteModal() {
    this.deleteModal.hide();
  }

}
