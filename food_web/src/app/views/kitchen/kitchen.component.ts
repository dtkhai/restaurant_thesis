import {Component, OnDestroy, OnInit} from '@angular/core';
import {SpeakSynthesis, SpeechRecognitionService} from '../../../services/speech-recognition.service';
import {FirebaseService} from '../../../services/firebase.service';
import {Observable} from 'rxjs/Observable';
import {AuthenticateService} from '../../common/authenticate.service';
import {SessionData} from '../../models/session.data.model';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-kitchen',
  templateUrl: 'kitchen.component.html',
  providers: [SpeechRecognitionService, FirebaseService, SpeakSynthesis],
})

export class KitchenComponent implements OnInit, OnDestroy {
  // waiting_foods = [{id: 1, name: 'Ga ran', time: '5m', note: 'it duong'}];
  showSearchButton: boolean;
  speechData: string;
  //isListening: boolean;
  waiting_foods: Observable<any[]>;
  cooking_foods: Observable<any[]>;
  totalWaitingFoods = 0;
  totalCookingFoods = 0;
  session: SessionData;

  keyWord = 'nghe';

  companyId = 'Restaurant';
  docId: Observable<any[]>;
  statusNames = ['Waiting', 'Đang nấu', 'Hoàn tất', 'Đã giao khách', 'Hết nguyên liệu', 'Hủy'];
  cookCommand = ['nấu', 'nóng', 'nâu'];
  cancelCommand = ['hủy', 'quỷ', 'hỷ', 'mỹ', 'thủy'];
  doneCommand = ['xong', 'song', 'sông'];
  outCommand = ['hết', 'hếch', 'hớt'];

  constructor(private db: FirebaseService, private speechRecognitionService: SpeechRecognitionService,
              private speakSynthesis: SpeakSynthesis, private http: HttpClient,
              private _authService: AuthenticateService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.companyId += this.session.company_id
      this.db.firebaseInit(this.companyId);
    });
  }

  ngOnInit() {
    console.log('Speech Recognition');
    //this.isListening = false;
    this.showSearchButton = true;
    this.speechData = '';
    this.waiting_foods = this.db.getWaitingFoods();
    this.waiting_foods.subscribe(result => {
      this.totalWaitingFoods = result.length;
    });
    this.cooking_foods = this.db.getCookingFoods();
    this.cooking_foods.subscribe(result => {
      this.totalCookingFoods = result.length;
    });
    this.activateSpeech();
  }

  ngOnDestroy() {
    this.speechRecognitionService.DestroySpeechObject();
  }

  activateSpeech(): void {
    this.showSearchButton = false;

    this.speechRecognitionService.record()
      .subscribe(
        (value) => {
          value = value.toLowerCase();
          let textToSpeech = '';
          {
            this.speechData = value;

            // 0: command
            // 1: food_id

            const splitted_value = value.split(' ', 2);

            // Check valid ID
            if (!isNaN(+splitted_value[1])) {
            }

            if ((splitted_value[0] == 'cook')) {
              this.updateStatusFood(splitted_value[1], 1);
              textToSpeech = `Dish ${splitted_value[1]} is cooking`;
            } else if ((splitted_value[0] == 'finish')) {
              this.updateStatusFood(splitted_value[1], 2);
              textToSpeech = `Dish ${splitted_value[1]} is done`
            } else if (splitted_value[0] == 'cancel') {
              this.updateStatusFood(splitted_value[1], 4);
              textToSpeech = `Dish ${splitted_value[1]} is out of ingredients`;
            } else {
              console.log(splitted_value[0]);
              textToSpeech = `Wrong command, please speak again`;
            }
          }
          this.speakSynthesis.speak(textToSpeech);
          this.showSearchButton = true;
        },
        (err) => {
          console.log(err);
          if (err.error === 'no-speech') {
            console.log('--restatring service--');
            this.activateSpeech();
          }
        },
        () => {
          this.showSearchButton = true;
          console.log('--complete--');
          this.activateSpeech();
        });
  }

  updateStatusFood(itemId, status) {
    this.docId = this.db.getItem(itemId);

    const sub = this.docId.subscribe(docs => {
      docs.forEach(doc => {
        this.db.updateItem(itemId, status);
        if (status == 2 || status == 4) {
          this.sendNoti(doc, status);
        }
        sub.unsubscribe();
      })
    })
  }

  deleteFood(id) {
    this.db.deleteItem(id);
  }

  sendNoti(food, status) {
    console.log('Start');
    const headers = new HttpHeaders()
      .set('cache-control', 'no-cache')
      .set('Content-Type', 'application/json')
      .set('Authorization', ' key=AAAAycc0sIg:APA91bFPerdgKI7sok75R3wRqZLtJwATgwb0FsK3pq5fM7nFbhnou1VD-5byhZaZ234ZNyVGu3of6BN_5USmurXBU6tPi210KJRPlB9tXh2t1clLxIT7EP5Cpv_vuYNR0IXbVk_Mez3c');

    const body = {
      "priority": "high",
      "notification": {
        "title": this.statusNames[status],
        "body": food.name + " for " + food.table_name,
        "click_action": "https://angularfirebase.com",
        "sound": "default"
      },
      "data": {
        "image": food.image,
        "table": food.table_id
      },
      "to": "/topics/All"
    }

    return this.http
      .post('https://fcm.googleapis.com/fcm/send', body, {headers: headers})
      .subscribe(res => console.log(res));
  }
}
