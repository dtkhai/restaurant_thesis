import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UsersService} from '../users.service';
import {BsModalService, ModalDirective} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppConstants} from '../../../common/const';
import {ToastrService} from 'ngx-toastr';
import {AuthenticateService} from "../../../common/authenticate.service";
import {SessionData} from "../../../models/session.data.model";
import {ActivatedRoute} from "@angular/router";

type AOA = any[][];

@Component({
  selector: 'app-users',
  templateUrl: './all-users.component.html',
  providers: [UsersService],
})
export class AllUsersComponent implements OnInit {
  selectedUserId: any;
  selectedUsername: string;

  rForm: FormGroup;
  importForm: FormGroup;
  @ViewChild('NewUserModal') NewUserModal: ModalDirective;
  @ViewChild('ImportUserModal') ImportUserModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;
  @ViewChild('inputFile') inputFile: ElementRef;

  emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  usernameRegex = /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/;
  roles_display = AppConstants.DISPLAY_ROLES;
  roles = AppConstants.ROLES;

  users = [];
  private currentPage = 1;
  private pageSize = 10;
  private total_users = 0;
  FIRST_PAGE = 1;
  MAX_PAGE_SIZE = 10;
  titleAlert: String = 'Please enter all field';

  data: AOA = [['Username', 'Full name', 'Password', 'Email', 'Age', 'Phone', 'Description', 'Role']];
  fileName = 'ImportUsers.xlsx';
  session: SessionData;
  isMaster: boolean;

  constructor(private fb: FormBuilder, private toastr: ToastrService,
              private userService: UsersService, private modalService: BsModalService, private _authService: AuthenticateService,
              private route: ActivatedRoute) {

    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.route.params.subscribe(params => {
        console.log(params);
        if (params.hasOwnProperty('id')) {
          this.isMaster = true;
          userService.init(params.id);
        } else {
          this.isMaster = false;
          userService.init(session_data.company_id);
        }
      });
    });


  }

  ngOnInit() {
    this.rForm = this.fb.group({
      'fname': [null, Validators.compose([
        Validators.required,
        Validators.pattern(this.usernameRegex)])],
      'lname': [null, Validators.required],
      'password': [null, Validators.required],
      'email': [null, Validators.compose([
        Validators.required,
        Validators.pattern(this.emailRegex)])],
      'role_id': [null, Validators.required],
      'company_id': [null],
      validate: '1'
    });

    this.importForm = this.fb.group({
      file: null,
    });

    if (this.isMaster == true) {
      this.getAdmins(this.FIRST_PAGE, this.MAX_PAGE_SIZE);
    } else {
      this.getAllUsers(this.FIRST_PAGE, this.MAX_PAGE_SIZE);
    }

  }

  getAdmins(currentPage, pageSize) {
    this.userService.getAdmins(currentPage, pageSize).subscribe(result => {
      if (result.success) {
        this.users = result.data.users;
        this.currentPage = currentPage;
        this.total_users = result.data.total_items
      } else {
        this.users = [];
      }
    }, error => {
      console.log(error);
    })
  }

  getAllUsers(currentPage, pageSize) {
    this.userService.getAllUsers(currentPage, pageSize).subscribe(result => {
      if (result.success) {
        this.users = result.data.users;
        this.currentPage = currentPage;
        this.total_users = result.data.total_items
      } else {
        this.users = [];
      }
    }, error => {
      console.log(error);
    })
  }

  // -------------------- Open / Close Modals -------------------

  openNewUserModal() {
    this.NewUserModal.show();
  }

  openImportUsers() {
    this.ImportUserModal.show();
  }

  closeNewUserModal() {
    this.NewUserModal.hide();
  }

  closeImportUsersModal() {
    this.ImportUserModal.hide();
    this.importForm.reset();
    this.inputFile.nativeElement.value = '';
  }

  openDeleteModal(user) {
    this.selectedUserId = user.id;
    this.selectedUsername = user.username;
    this.deleteModal.show();
  }

  closeDeleteModal() {
    this.selectedUserId = 0;
    this.selectedUsername = '';
    this.deleteModal.hide();
  }

  registerUser(data) {
    if (this.isMaster) {
      data.role_id = this.roles.admin;
    }
    this.userService.register(data).subscribe(result => {
        if (result['success']) {
          this.toastr.success('Created successfully!', 'User');
          this.closeNewUserModal();
          this.getAllUsers(this.FIRST_PAGE, this.MAX_PAGE_SIZE);
        } else {
          this.toastr.error('Failed', 'User');
        }
      },
      error => {
        this.toastr.error('Something wrong');
      }
    );
  }

  importUsers(officeFile) {
    this.userService.importUsers(officeFile).subscribe(result => {
      if (result['success']) {
        this.toastr.success('Created Successfully', 'Users');
        this.closeImportUsersModal();
        this.getAllUsers(this.FIRST_PAGE, this.MAX_PAGE_SIZE);
      } else {
        this.toastr.error(result['message'], 'Users');
      }
    });
  }


  deleteUser(id) {
    this.userService.delete(id).subscribe(data => {
      this.getAllUsers(this.FIRST_PAGE, this.MAX_PAGE_SIZE);
      this.closeDeleteModal();
      this.toastr.success('Deleted successfully!', 'User');
    });
  }
}
