import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from '../../common/api.service';
import * as firebase from 'firebase';

@Injectable()
export class UsersService {
  path: string;

  constructor(private _api: ApiService) {
  }

  init(companyId) {
    this.path = 'restaurants/' + companyId + '/users'
  }

  getAllUsers(currentPage: number = 1, pageSize: number = 20) {
    const params = '?current_page=' + currentPage + '&page_size=' + pageSize;
    return this._api.get(this.path + params);
  }

  getAdmins(currentPage: number = 1, pageSize: number = 20) {
    const params = '?current_page=' + currentPage + '&page_size=' + pageSize;
    console.log(this.path);
    return this._api.get(this.path + '/admins' + params);
  }

  getUsersOfCourse(currentPage: number = 1, pageSize: number = 20, roleId: number, course_id: number) {
    const params = '?course_id=' + course_id + '&current_page=' + currentPage + '&page_size=' + pageSize + '&role_id=' + roleId;
    return this._api.get(this.path + '/getUsersAssigned' + params);
  }

  getUsersNotOfCourse(currentPage: number = 1, pageSize: number = 20, roleId: number, course_id: number) {
    const params = '?course_id=' + course_id + '&current_page=' + currentPage + '&page_size=' + pageSize + '&role_id=' + roleId;
    return this._api.get(this.path + '/getUsersNotAssigned' + params);
  }

  register(body) {
    // firebase.auth().createUserWithEmailAndPassword(body.email, body.password)
    //   .then(user => {
        // Register realtime database firebase
        // this.users = this.db.list('/users/' + user.uid);
        // this.users.push({email: body.email, name: body.full_name, status: 'offline'});
        // firebase.database().ref(this.path + user.uid).set({
        //   name: body.full_name,
        //   email: body.email,
        //   status: 'offline'
        // });
    // })
    // .catch(function (error) {
    //   // Handle Errors here.
    //   console.log(error.code);
    //   console.log(error.message);
    // });

    const coursesUrl = this.path + '/register';
    return this._api.post(coursesUrl, body);
  }

  login(body): Observable<any> {

    const usersUrl = 'users/login';
    return this._api.post(usersUrl, body);
  }

  public delete(userId) {
    const usersUrl = 'users/' + userId;
    return this._api.delete(usersUrl);
  }

  public search(body): Observable<any> {
    const usersUrl = this.path + '/searchUserCourseAssgined';
    return this._api.post(usersUrl, body);
  }

  public searchInRestUsers(body): Observable<any> {
    const usersUrl = this.path + '/searchUserCourseNotAssgined';
    return this._api.post(usersUrl, body);
  }

  public importUsers(excelFile) {
    const usersUrl = 'restaurants/users/import';
    return this._api.post(usersUrl, excelFile);
  }
}
