import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../users.service';
import {ApiService} from '../../../common/api.service';
import {SessionData} from '../../../models/session.data.model';
import {AuthenticateService} from '../../../common/authenticate.service';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database-deprecated';
import {ToastrService} from 'ngx-toastr';
import {AppConstants} from "../../../common/const";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MessagingService} from "../../../messaging.service";
import {RequestOptions} from "@angular/http";

interface User {
  uid: string;
  name: string;
  status: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UsersService, ApiService, MessagingService]
})
export class LoginComponent implements OnInit {

  lForm: FormGroup;
  usernameRegex = /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/;
  titleAlert = '';
  private session: SessionData;
  users: FirebaseListObservable<User[]>;
  error = '';
  roles = AppConstants.ROLES;
  message;

  constructor(private fb: FormBuilder, private userService: UsersService,
              private router: Router, private _authService: AuthenticateService,
              private toast: ToastrService, private http: HttpClient, private msgService: MessagingService) {
    // this.msgService.getPermission()
    // this.msgService.receiveMessage()
    // this.message = this.msgService.currentMessage
  }

  ngOnInit() {
    this.lForm = this.fb.group({
      'email': [null, Validators.compose([
        Validators.required,
        Validators.pattern(this.usernameRegex)])],
      'password': [null, Validators.required],
      validate: '1'
    })
  }


  LoginUser(data) {
    this.userService.login(data).subscribe(result => {
      if (result.success) {
        console.log(result);
        this.session = new SessionData(result.data.id, data.username, result.data.full_name, result.data.token, result.data.role_id,
          data.password, result.data.email, result.data.company_id);
        this._authService.setSession(this.session);
        this.toast.success('Success!', 'Login');
        if (result.data.role_id == this.roles.master) {
          this.router.navigate(['restaurants']);
        } else if (result.data.role_id == this.roles.chef) {
          this.router.navigate(['kitchen']);
        } else {
          this.router.navigate(['rooms']);
        }
      } else {
        this.error = 'Username or password is incorrect';
        this.router.navigate(['/login']);
      }
    });
  }
}
