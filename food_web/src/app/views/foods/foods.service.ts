import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from '../../common/api.service';

@Injectable()
export class FoodsService {
  path: string;

  constructor(private _api: ApiService) {
  }

  init(companyId) {
    this.path = 'restaurants/' + companyId + '/';
  }

  public getFoods(currentPage: number = 1, pageSize: number = 5, company_id): Observable<any> {
    const params = '?current_page=' + currentPage + '&page_size=' + pageSize;
    const foodsUrl = 'foods';
    return this._api.get(this.path + foodsUrl + params);
  }

  public getFood(foodId): Observable<any> {
    const foodsUrl = 'foods/';
    return this._api.get(this.path + foodsUrl + foodId);
  }

  public filterFoods(groupId: number = 0, currentPage: number = 1, pageSize: number = 10): Observable<any> {
    const params = '?group_id=' + groupId + '&current_page=' + currentPage + '&page_size=' + pageSize;
    const foodsUrl = 'foods/filter';
    return this._api.get(this.path + foodsUrl + params);
  }

  public searchFoods(search_string, currentPage: number = 1, pageSize: number = 10): Observable<any> {
    const params = '?search_string=' + search_string + '&current_page=' + currentPage + '&page_size=' + pageSize;
    const foodsUrl = 'foods/search';
    return this._api.get(this.path + foodsUrl + params);
  }

  public createFood(body) {
    const foodsUrl = 'foods';
    return this._api.post(this.path +  foodsUrl, body);
  }

  public updateFood(foodId, body) {
    const foodsUrl = 'foods/' + foodId;
    return this._api.put(this.path +  foodsUrl, body);
  }

  public deleteFood(foodId) {
    const foodsUrl = 'foods/' + foodId;
    return this._api.delete(this.path +  foodsUrl);
  }

  public getGroups(): Observable<any> {
    const food_groupsUrl = 'groups';
    return this._api.get( this.path + food_groupsUrl);
  }
}
