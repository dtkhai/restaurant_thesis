import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap';
import {FoodsService} from './foods.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AuthenticateService} from '../../common/authenticate.service';
import {SessionData} from '../../models/session.data.model';

declare var $: any;

export class File {
  avatar: '';
}

export class Food {
  name: '';
  price: '';
  main_image: '';
  images: File[];
  food_group_id: '';
  description: '';
  note: ''
}

@Component({
  selector: 'app-foods',
  templateUrl: 'foods.component.html',
  providers: [FoodsService]

})

export class FoodsComponent implements OnInit {
  foods = [];
  food = {};
  groups = [];
  selectedGroup: any;
  totalItems = 0;
  rForm: FormGroup;
  searchForm: FormGroup;
  title = 'All Foods';
  private selectedFoodId;
  private currentPage = 1;
  private pageSize = 10;
  FIRST_PAGE = 1;
  main_image_url: string;
  private session: SessionData;
  selectedCategories = [];
  @ViewChild('addNewFoodModal') addNewFoodModal: ModalDirective;
  @ViewChild('editFoodModal') editFoodModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;
  @ViewChild('addNewGroupModal') addNewGroupModal: ModalDirective;
  image1: string;
  image2: string;
  image3: string;

  constructor(private foodsService: FoodsService, private fb: FormBuilder, private router: Router,
              private toastr: ToastrService, private _authService: AuthenticateService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.foodsService.init(session_data.company_id);
    });

    this.createForm();

    this.searchForm = this.fb.group({
      'search_string': [null]
    });
  };

  ngOnInit() {
    this.setImages();
    this.getGroups();
    this.getFoods(this.currentPage, this.pageSize);
  }

  createForm() {
    this.rForm = this.fb.group({
      name: [null, Validators.required],
      price: null,
      main_image: null,
      images: this.fb.array([]),
      food_group_ids: this.fb.array([]),
      description: null,
      note: null,
    });
  }

  updateChkbxArray(group, isChecked, key) {
    const chkArray = <FormArray>this.rForm.get(key);
    if (isChecked) {
      // sometimes inserts values already included creating double records for the same values -hence the defence
      if (chkArray.controls.indexOf(group.id) === -1) {
        chkArray.push(new FormControl(group.id));
      }
    } else {
      const idx = chkArray.controls.indexOf(group.id);
      chkArray.removeAt(idx);
    }
  }

  checkStatus(e, group) {
    if (e.target.checked) {
      this.selectedCategories.push({id: group.id});
      console.log(this.selectedCategories);
    } else {
      this.selectedCategories.splice(this.selectedCategories.findIndex(i => i.id === group.id), 1);
    }
  }

  searchFoods(data) {
    this.title = 'Search';
    this.foodsService.searchFoods(data.search_string).subscribe(result => {
      this.foods = result.data.foods;
      this.totalItems = result.data.total_foods;
    });
  }

  filterFood(group) {
    this.title = group.name;
    this.foodsService.filterFoods(group.id).subscribe(result => {
      this.foods = result.data.foods;
      this.totalItems = result.data.total_foods;
    });
  }

  getFoods(currentPage, pageSize) {
    this.title = 'All Foods';
    this.foodsService.getFoods(currentPage, pageSize, this.session.company_id).subscribe(result => {
      if (result.success) {
        this.foods = result.data.foods;
        this.totalItems = result.data.total_foods;
        this.currentPage = currentPage;
        this.selectedGroup = 0;
      } else {
        this.foods = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getFood(foodId) {
    const self = this;

    self.groups.forEach(function (group: any) {
      group.checked = false;
    });

    this.foodsService.getFood(foodId).subscribe(result => {
      if (result.success) {
        this.food = result.data;
        this.selectedCategories = result.data.categories;

        self.groups.forEach(function (group: any) {
          self.selectedCategories.forEach(function (category: any) {
            if (group.id == category.id) {
              group.checked = true;
            }
          })
        });
      } else {
        this.food = {};
      }
    }, error => {
      console.log(error);
    });
  }

  addFood(data) {
    console.log(data);
    data.company_id = this.session.company_id;
    this.foodsService.createFood(data).subscribe(result => {
        this.closeAddNewFoodModal();
        this.toastr.success('Created successfully', 'Food');
        this.getFoods(this.currentPage, this.pageSize);
      },
      error => {
        console.log(error);
      });
  }

  deleteFood(id) {
    this.foodsService.deleteFood(id).subscribe(result => {
      this.closeDeleteModal();
      this.toastr.success('Deleted successfully', 'Food');
      this.getFoods(this.currentPage, this.pageSize);
    });
  }

  updateFood(data) {
    data.food_group_ids = this.selectedCategories.map(food => food.id); // always set active when create
    this.foodsService.updateFood(this.selectedFoodId, data).subscribe(result => {
      this.closeEditFoodModal();
      this.toastr.success('Updated successfully', 'Food');
      this.getFoods(this.currentPage, this.pageSize);
    });
  }

  get images(): FormArray {
    return this.rForm.get('images') as FormArray;
  };

  setImages() {
    const file = {avatar: ''};
    const addressFormArray = this.fb.array([this.fb.group(file)]);
    this.rForm.setControl('images', addressFormArray);
  }

  onSubImagesChange(event, index) {
    console.log(this.rForm.get('images'));
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (e: any) => {
        this['url' + index] = e.target.result;
        this.rForm.get(['images', index, 'avatar']).setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        });
      };
    }
  }

  onMainImageChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (e: any) => {
        this.main_image_url = e.target.result;
        this.rForm.get('main_image').setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        });
      };
    }
  }

  addNewImage() {
    if (this.image1 && this.image2 && this.image3) {
      return;
    }
    const file = {avatar: ''};
    this.images.push(this.fb.group(file));
  }

  getGroups() {
    this.foodsService.getGroups().subscribe(result => {
      if (result.success) {
        this.groups = result.data.groups;
      } else {
        this.groups = [];
      }
    }, error => {
      console.log(error);
    });
  }

  onSelect(group) {
    this.selectedGroup = group;
    this.filterFood(group);
  }

// ----------------------------------------Open/ Close modal function---------------------------------

  openAddNewFoodModal() {
    this.rForm.reset();
    this.addNewFoodModal.show();
  }

  closeAddNewFoodModal() {
    this.addNewFoodModal.hide();
  }

  openEditFoodModal(foodId) {
    this.selectedFoodId = foodId;
    this.getFood(foodId);
    this.editFoodModal.show();
  }

  closeEditFoodModal() {
    this.editFoodModal.hide();
    this.selectedFoodId = null;
    this.food = {};
  }

  openDeleteModal(foodId) {
    this.selectedFoodId = foodId;
    this.deleteModal.show();
  }

  closeDeleteModal() {
    this.deleteModal.hide();
  }

}
