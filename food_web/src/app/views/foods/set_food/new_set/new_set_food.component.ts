import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDirective, TabsetConfig} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {SetFoodsService} from "../set_foods.service";
import {SessionData} from "../../../../models/session.data.model";
import {AuthenticateService} from "../../../../common/authenticate.service";
import {FoodsService} from "../../foods.service";
import {CategoriesService} from "../../../categories/categories.service";

declare var $: any;

export function getTabsetConfig(): TabsetConfig {
  return Object.assign(new TabsetConfig(), {type: 'pills'});
}

@Component({
  selector: 'app-new-discount',
  templateUrl: 'new_set_food.component.html',
  providers: [SetFoodsService, FoodsService, CategoriesService, {provide: TabsetConfig, useFactory: getTabsetConfig}]
})

export class NewComboComponent implements OnInit {
  title: string;
  totalItems: number;
  discount = {};
  foods = [];
  private currentPage = 1;
  private pageSize = 10;
  selectedDiscountId: number;
  rForm: FormGroup;
  searchForm: FormGroup;
  session: SessionData;
  selectedType: number;
  selectedFoods = [];
  finalSelectedFoods = [];

  groups = [];
  selectedGroup: any;

  @ViewChild('assignFoodModal') assignFoodModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;
  image_url: string;

  constructor(private setFoodsService: SetFoodsService, private foodsService: FoodsService, private fb: FormBuilder,
              private router: Router, private toastr: ToastrService, private cdRef: ChangeDetectorRef,
              private _authService: AuthenticateService, private categoriesService: CategoriesService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.setFoodsService.init(session_data.company_id);
      this.categoriesService.init(session_data.company_id);
      this.foodsService.init(session_data.company_id);
    });
  }

  ngOnInit() {
    this.createForm();
    this.getGroups();
    this.getFoods();
  }

  createForm() {
    this.rForm = this.fb.group({
      combo: this.fb.group({
        'name': [null, Validators.required],
        'image': null,
        'price': null,
        'discount_id': null,
        'discount_style_id': null
      })
    });

    this.searchForm = this.fb.group({
      'search_string': null
    });
  }

  // ------------------------------------------------ Categories/ foods ------------------------------------------------

  getGroups() {
    this.categoriesService.getGroups().subscribe(result => {
      if (result.success) {
        this.groups = result.data.groups;
      } else {
        this.groups = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getFoods(currentPage = this.currentPage, pageSize = this.pageSize) {
    const self = this;

    self.foods.forEach(function (food: any) {
      food.checked = false;
    });

    this.foodsService.getFoods(currentPage, pageSize, this.session.company_id).subscribe(result => {
      if (result.success) {
        this.foods = result.data.foods;
        this.totalItems = result.data.total_foods;
        this.selectedGroup = 0;

        self.foods.forEach(function (food: any) {
          self.selectedFoods.forEach(function (foodId: any) {
            if (food.id === foodId.id) {
              food.checked = true;
            }
          })
        })

      } else {
        this.foods = [];
      }
    }, error => {
      console.log(error);
    });
  }

  filterFood(group) {
    const self = this;

    self.foods.forEach(function (food: any) {
      food.checked = false;
    });

    this.title = group.name;
    this.foodsService.filterFoods(group.id).subscribe(result => {
      this.foods = result.data.foods;
      this.totalItems = result.data.total_foods;

      self.foods.forEach(function (food: any) {
        self.selectedFoods.forEach(function (foodId: any) {
          if (food.id === foodId.id) {
            food.checked = true;
          }
        })
      })
    });
  }

  searchFoods(data) {
    this.title = 'Search';
    this.foodsService.searchFoods(data.search_string).subscribe(result => {
      this.foods = result.data.foods;
      this.totalItems = result.data.total_foods;
    });
  }

  // -------------------------------------------------------------------------------------------------------------------

  addCombo(data) {
    data.food_ids = this.finalSelectedFoods.map(food => food.id); // always set active when create
    this.setFoodsService.createCombo(data).subscribe(result => {
        this.toastr.success('Created successfully', 'Combo');
        this.rForm.reset();
        this.router.navigate(['combos']);
      },
      error => {
        console.log(error);
      });
  }

  checkStatus(e, food) {
    if (e.target.checked) {
      this.selectedFoods.push({id: food.id, name: food.name});
    } else {
      this.selectedFoods.splice(this.selectedFoods.findIndex(i => i.id === food.id), 1);
    }
  }

  saveStatus() {
    this.finalSelectedFoods = this.selectedFoods.slice();
    this.closeAssignFoodModal();
  }

  onSelect(group) {
    this.selectedGroup = group;
    this.filterFood(group);
  }

  onMainImageChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (e: any) => {
        this.image_url = e.target.result;
        this.rForm.get(['combo', 'image']).setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        });
      };
    }
  }

  removeFoodFromList(foodId) {
    this.finalSelectedFoods.splice(this.finalSelectedFoods.findIndex(i => i.id === foodId), 1);
  }

  // ----------------------------------------Open/ Close modal function-------------------------------------------------
  openAssignFoodModal() {
    this.selectedFoods = this.finalSelectedFoods.slice();
    this.getGroups();
    this.getFoods();
    this.assignFoodModal.show();
  }

  closeAssignFoodModal() {
    this.assignFoodModal.hide();
  }
}
