import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from "../../../common/api.service";

@Injectable()
export class SetFoodsService {
  path: string;

  constructor(private _api: ApiService) {
  }

  init(companyId) {
    this.path = 'restaurants/' + companyId + '/';
  }

  public getSets(currentPage: number = 1, pageSize: number = 5): Observable<any> {
    const params = '?current_page=' + currentPage + '&page_size=' + pageSize;
    const setsUrl = 'combos';
    return this._api.get(this.path + setsUrl + params);
  }

  public deleteCombo(comboId) {
    const setsUrl = 'combos/' + comboId;
    return this._api.delete(this.path +  setsUrl);
  }

  public getSet(comboId): Observable<any> {
    const setsUrl = 'combos/';
    return this._api.get(this.path + setsUrl + comboId);
  }

  public updateSet(comboId, body) {
    const setsUrl = 'combos/' + comboId;
    return this._api.put(this.path + setsUrl, body);
  }

  public filterSets(groupId: number = 0, currentPage: number = 1, pageSize: number = 5): Observable<any> {
    const params = '?group_id=' + groupId + '&current_page=' + currentPage + '&page_size=' + pageSize;
    const discountsUrl = 'discounts/filter';
    return this._api.get(this.path + discountsUrl + params);
  }

  public searchSets(search_string, currentPage: number = 1, pageSize: number = 5): Observable<any> {
    const params = '?search_string=' + search_string + '&current_page=' + currentPage + '&page_size=' + pageSize;
    const discountsUrl = 'discounts/search';
    return this._api.get(this.path + discountsUrl + params);
  }

  public createCombo(body) {
    const discountsUrl = 'combos';
    return this._api.post(this.path + discountsUrl, body);
  }



  public changeStatusDiscount(discountId, isActive) {
    const params = '?active=' + isActive;
    const discountsUrl = 'discounts/' + discountId + '/change-status-discount';
    return this._api.put(this.path + discountsUrl + params);
  }
}
