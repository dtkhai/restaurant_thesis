import { Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective, TabsetConfig} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {SessionData} from '../../../models/session.data.model';
import {AuthenticateService} from '../../../common/authenticate.service';
import {SetFoodsService} from './set_foods.service';

declare var $: any;

export function getTabsetConfig(): TabsetConfig {
  return Object.assign(new TabsetConfig(), {type: 'pills'});
}

@Component({
  selector: 'app-set-foods',
  templateUrl: 'set_foods.component.html',
  providers: [SetFoodsService, {provide: TabsetConfig, useFactory: getTabsetConfig}]

})

export class SetFoodsComponent implements OnInit {
  title: string;
  totalItems: number;
  set_foods = [];
  discount = {};
  private currentPage = 1;
  private pageSize = 10;
  session: SessionData;
  appliedOptions = ['All', 'Selected Combos'];
  selectedOption = 0;

  @ViewChild('addNewSetModal') addNewSetModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;
  @ViewChild('editSetModal') editSetModal: ModalDirective;
  selectedComboId: number;


  constructor(private set_foodsService: SetFoodsService, private router: Router,
              private toastr: ToastrService, private _authService: AuthenticateService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.set_foodsService.init(session_data.company_id);
    });

  }

  ngOnInit() {
    this.getSets(this.currentPage, this.pageSize);
  }

  getSets(currentPage, pageSize) {
    this.title = 'All Sets';
    this.set_foodsService.getSets(currentPage, pageSize).subscribe(result => {
      if (result.success) {
        this.set_foods = result.data.combos;
        this.totalItems = result.data.total_combos;
      } else {
        this.set_foods = [];
      }
    }, error => {
      console.log(error);
    });
  }

  deleteCombo(id) {
    this.set_foodsService.deleteCombo(id).subscribe(result => {
      this.closeDeleteModal();
      this.toastr.success('Deleted successfully', 'Combo');
      this.getSets(this.currentPage, this.pageSize);
    });
  }

  // ----------------------------------------Open/ Close modal function---------------------------------

  openDeleteModal(comboId) {
    this.deleteModal.show();
    this.selectedComboId = comboId;
  }

  closeDeleteModal() {
    this.deleteModal.hide();
  }

}
