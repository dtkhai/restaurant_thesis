import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDirective, TabsetConfig, TypeaheadMatch} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {DiscountsService} from '../discounts.service';
import {AuthenticateService} from '../../../common/authenticate.service';
import {SessionData} from '../../../models/session.data.model';
import {FoodsService} from '../../foods/foods.service';
import {CategoriesService} from "../../categories/categories.service";
import {SetFoodsService} from "../../foods/set_food/set_foods.service";

declare var $: any;

export function getTabsetConfig(): TabsetConfig {
  return Object.assign(new TabsetConfig(), {type: 'pills'});
}

@Component({
  selector: 'app-new-discount',
  templateUrl: 'new_discount.component.html',
  providers: [DiscountsService, FoodsService, CategoriesService, SetFoodsService, {
    provide: TabsetConfig,
    useFactory: getTabsetConfig
  }]
})

export class NewDiscountComponent implements OnInit {
  title: string;
  totalItems: number;
  discount = {};
  foods = [];
  private currentPage = 1;
  private pageSize = 10;
  selectedDiscountId: number;
  rForm: FormGroup;
  searchForm: FormGroup;
  bsDateFrom: Date = new Date();
  bsTimeFrom: Date = new Date();
  bsTimeTo: Date = new Date();
  session: SessionData;
  appliedOptions = ['All', 'Selected Foods', 'Selected Combos'];
  discountTypes = ['Percentage discount', 'Fixed amount'];
  selectedType: number;
  selectedFoods = [];
  selectedCombos = [];
  groups = [];
  combos = [];
  selectedGroup: any;
  finalSelectedFoods = [];
  finalSelectedCombos = [];

  @ViewChild('assignFoodModal') assignFoodModal: ModalDirective;
  @ViewChild('assignComboModal') assignComboModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;

  constructor(private discountsService: DiscountsService, private foodsService: FoodsService, private categoriesService: CategoriesService,
              private fb: FormBuilder, private combosService: SetFoodsService,
              private router: Router, private toastr: ToastrService, private cdRef: ChangeDetectorRef,
              private _authService: AuthenticateService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.discountsService.init(session_data.company_id);
      this.categoriesService.init(session_data.company_id);
      this.foodsService.init(session_data.company_id);
      this.combosService.init(session_data.company_id);
    });
  }

  ngOnInit() {
    this.createForm();
    this.getFoods(this.currentPage, this.pageSize);
    this.bsTimeTo.setHours(12);
    this.bsTimeTo.setMinutes(0);
    this.bsTimeFrom.setHours(7);
    this.bsTimeFrom.setMinutes(0);
  }

  createForm() {
    this.rForm = this.fb.group({
      discount: this.fb.group({
        'name': [null, Validators.required],
        'price_requried': null,
        'selectedType': '0',
        'percentage_value': null,
        'value': null,
        'discount_style_id': '0',
        'date_from': null,
        'date_to': null,
        'time_from': null,
        'time_to': null,
      })
    });

    this.searchForm = this.fb.group({
      'search_string': null
    });
  }

  // ------------------------------------------------ Categories/ foods ------------------------------------------------
  getGroups() {
    this.foodsService.getGroups().subscribe(result => {
      if (result.success) {
        this.groups = result.data.groups;
      } else {
        this.groups = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getFoods(currentPage = this.currentPage, pageSize = this.pageSize) {
    const self = this;

    self.foods.forEach(function (food: any) {
      food.checked = false;
    });

    this.foodsService.getFoods(currentPage, pageSize, this.session.company_id).subscribe(result => {
      if (result.success) {
        this.foods = result.data.foods;
        this.totalItems = result.data.total_foods;
        this.selectedGroup = 0;

        self.foods.forEach(function (food: any) {
          self.selectedFoods.forEach(function (foodId: any) {
            if (food.id === foodId.id) {
              food.checked = true;
            }
          })
        })

      } else {
        this.foods = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getCombos() {
    const self = this;

    this.combosService.getSets().subscribe(result => {
      if (result.success) {
        this.combos = result.data.combos;
        this.totalItems = result.data.total_combos;

        self.combos.forEach(function (food: any) {
          self.selectedCombos.forEach(function (comboId: any) {
            if (food.id === comboId.id) {
              food.checked = true;
            }
          })
        })
      } else {
        this.combos = [];
      }
    }, error => {
      console.log(error);
    });
  }

  filterFood(group) {
    const self = this;

    self.foods.forEach(function (food: any) {
      food.checked = false;
    });

    this.title = group.name;
    this.foodsService.filterFoods(group.id).subscribe(result => {
      this.foods = result.data.foods;
      this.totalItems = result.data.total_foods;

      self.foods.forEach(function (food: any) {
        self.selectedFoods.forEach(function (foodId: any) {
          if (food.id === foodId.id) {
            food.checked = true;
          }
        })
      })
    });
  }

  searchFoods(data) {
    this.title = 'Search';
    this.foodsService.searchFoods(data.search_string).subscribe(result => {
      this.foods = result.data.foods;
      this.totalItems = result.data.total_foods;
    });
  }

  // -------------------------------------------------------------------------------------------------------------------

  addDiscount(data) {
    if (data.discount.discount_style_id == 1) {
      data.food_ids = this.finalSelectedFoods.map(food => food.id); // always set active when create
    } else if (data.discount.discount_style_id == 2) {
      data.combo_ids = this.finalSelectedCombos.map(combo => combo.id); // always set active when create
    }

    this.discountsService.createDiscount(data).subscribe(result => {
        this.toastr.success('Created successfully', 'Discount');
        this.rForm.reset();
        this.router.navigate(['discounts']);
      },
      error => {
        console.log(error);
      });
  }

  infoIsEntered() {
    return 1;
  }

  checkStatus(e, food) {
    if (e.target.checked) {
      this.selectedFoods.push({id: food.id, name: food.name});
    } else {
      this.selectedFoods.splice(this.selectedFoods.findIndex(i => i.id === food.id), 1);
    }
  }

  checkComboStatus(e, combo) {
    if (e.target.checked) {
      this.selectedCombos.push({id: combo.id, name: combo.name});
    } else {
      this.selectedCombos.splice(this.selectedFoods.findIndex(i => i.id === combo.id), 1);
    }
  }

  saveComboStatus() {
    this.finalSelectedCombos = this.selectedCombos.slice();
    this.closeAssignComboModal();
  }

  saveStatus() {
    this.finalSelectedFoods = this.selectedFoods.slice();
    this.closeAssignFoodModal();
  }

  onSelect(group) {
    this.selectedGroup = group;
    this.filterFood(group);
  }

  removeFoodFromList(foodId) {
    this.finalSelectedFoods.splice(this.finalSelectedFoods.findIndex(i => i.id === foodId), 1);
  }

  removeComboFromList(comboId) {
    this.finalSelectedCombos.splice(this.finalSelectedCombos.findIndex(i => i.id === comboId), 1);
  }


  // ----------------------------------------Open/ Close modal function-------------------------------------------------
  openAssignFoodModal() {
    this.selectedFoods = this.finalSelectedFoods.slice();
    this.getGroups();
    this.getFoods();
    this.assignFoodModal.show();
  }

  closeAssignFoodModal() {
    this.assignFoodModal.hide();
  }

  openAssignComboModal() {
    this.selectedCombos = this.finalSelectedCombos.slice();
    this.getCombos();
    this.assignComboModal.show();
  }

  closeAssignComboModal() {
    this.assignComboModal.hide();
  }

  openDeleteModal(discountId) {
    this.selectedDiscountId = discountId;
    this.deleteModal.show();
  }

  closeDeleteModal() {
    this.deleteModal.hide();
  }
}
