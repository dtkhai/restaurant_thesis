import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from '../../common/api.service';

@Injectable()
export class DiscountsService {
  path: string;

  constructor(private _api: ApiService) {
  }

  init(companyId) {
    this.path = 'restaurants/' + companyId + '/';
  }

  public getDiscounts(currentPage: number = 1, pageSize: number = 5, active: boolean = false): Observable<any> {
    const params = '?current_page=' + currentPage + '&page_size=' + pageSize + '&active=' + active;
    const discountsUrl = 'discounts';
    return this._api.get(this.path + discountsUrl + params);
  }

  public getDiscount(discountId): Observable<any> {
    const discountsUrl = 'discounts/';
    return this._api.get(this.path + discountsUrl + discountId);
  }

  public filterDiscounts(groupId: number = 0, currentPage: number = 1, pageSize: number = 5): Observable<any> {
    const params = '?group_id=' + groupId + '&current_page=' + currentPage + '&page_size=' + pageSize;
    const discountsUrl = 'discounts/filter';
    return this._api.get(this.path + discountsUrl + params);
  }

  public searchDiscounts(search_string, currentPage: number = 1, pageSize: number = 5): Observable<any> {
    const params = '?search_string=' + search_string + '&current_page=' + currentPage + '&page_size=' + pageSize;
    const discountsUrl = 'discounts/search';
    return this._api.get(this.path + discountsUrl + params);
  }

  public createDiscount(body) {
    const discountsUrl = 'discounts';
    return this._api.post(this.path + discountsUrl, body);
  }

  public updateDiscount(discountId, body) {
    const discountsUrl = 'discounts/' + discountId;
    return this._api.put(this.path + discountsUrl, body);
  }

  public changeStatusDiscount(discountId, isActive) {
    const params = '?active=' + isActive;
    const discountsUrl = 'discounts/' + discountId + '/change-status-discount';
    return this._api.put(this.path + discountsUrl + params);
  }

  public changeEnableStatus(discountId, isActive) {
    const params = '?enable=' + isActive;
    const discountsUrl = 'discounts/' + discountId + '/enable';
    return this._api.put(this.path + discountsUrl + params);
  }
}
