import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDirective, TabsetConfig} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ToastrService} from "ngx-toastr";
import {DiscountsService} from "./discounts.service";
import {DatePipe} from '@angular/common';
import {FirebaseService} from "../../../services/firebase.service";
import {Observable} from "rxjs/Observable";
import {Food} from "../../models/food";
import {AuthenticateService} from "../../common/authenticate.service";
import {SessionData} from "../../models/session.data.model";

declare var $: any;

export function getTabsetConfig(): TabsetConfig {
  return Object.assign(new TabsetConfig(), {type: 'pills'});
}

@Component({
  selector: 'app-discounts',
  templateUrl: 'discounts.component.html',
  styleUrls: ['./discounts.component.css'],
  providers: [DiscountsService, {provide: TabsetConfig, useFactory: getTabsetConfig}, FirebaseService]

})

export class DiscountComponent implements OnInit {
  title: string;
  totalItems: number;
  discounts = [];
  discount = {};
  private currentPage = 1;
  private pageSize = 10;
  FIRST_PAGE = 1;
  selectedDiscountId: number;
  rForm: FormGroup;
  bsDateFrom: Date = new Date();
  bsDateTo: Date = new Date();
  bsTimeFrom: Date = new Date();
  bsTimeTo: Date = new Date();
  enabled = true;
  session: SessionData;
  appliedOptions = ['All', 'Selected Foods'];
  selectedOption = 0;

  @ViewChild('addNewDiscountModal') addNewDiscountModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;
  @ViewChild('editDiscountModal') editDiscountModal: ModalDirective;


  constructor(private discountsService: DiscountsService, private fb: FormBuilder, private router: Router,
              private toastr: ToastrService, private cdRef: ChangeDetectorRef, private _authService: AuthenticateService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.discountsService.init(session_data.company_id);
    });

  }

  ngOnInit() {
    this.createForm();
    this.getDiscounts(this.currentPage, this.pageSize, this.enabled);
  }

  createForm() {
    this.rForm = this.fb.group({
      discount: this.fb.group({
        'name': [null, Validators.required],
        'price_requried': null,
        'selectedOption': 0,
        'percentage_value': null,
        'value': null,
        'date_from': null,
        'date_to': null,
        'time_from': null,
        'time_to': null,
      })
    });
  }

  ngAfterViewChecked() {
    this.bsDateFrom = new Date();
    this.cdRef.detectChanges();
  }

  getDiscounts(currentPage, pageSize, active) {
    this.title = 'All Discounts';
    this.discountsService.getDiscounts(currentPage, pageSize, active).subscribe(result => {
      if (result.success) {
        this.discounts = result.data.discounts;
        this.totalItems = result.data.total_discounts;
      } else {
        this.discounts = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getDiscount(discountId) {
    this.discountsService.getDiscount(discountId).subscribe(result => {
      if (result.success) {
        this.discount = result.data;
      } else {
        this.discount = {};
      }
    }, error => {
      console.log(error);
    });
  }

  disableDiscount(id, isActive) {

    this.discountsService.changeStatusDiscount(id, isActive).subscribe(result => {
      this.closeDeleteModal();
      this.toastr.show('Disabled successfully', 'Discount');
      this.getDiscounts(this.currentPage, this.pageSize, this.enabled);
    });
  }

  changeEnableStatus(discountId, isEnabled) {
    if (!isEnabled) {
      isEnabled = true;
    } else {
      isEnabled = false;
    }

    this.discountsService.changeEnableStatus(discountId, isEnabled).subscribe(result => {
      if (result['data'].enable) {
        this.toastr.success('Enabled', 'Discount');
      } else {
        this.toastr.show('Disabled', 'Discount');
      }
    });
  }

  updateDiscount(data) {
    this.discountsService.updateDiscount(this.selectedDiscountId, data).subscribe(result => {
      this.closeEditDiscountModal();
      this.toastr.show('Updated successfully', 'Discount');
      this.getDiscounts(this.currentPage, this.pageSize, this.enabled);
    });
  }

  // ----------------------------------------Open/ Close modal function---------------------------------

  openAddNewDiscountModal() {
    this.addNewDiscountModal.show();
  }

  closeAddNewDiscountModal() {
    this.addNewDiscountModal.hide();
  }

  openDeleteModal(discountId) {
    this.selectedDiscountId = discountId;
    this.deleteModal.show();
  }

  closeDeleteModal() {
    this.deleteModal.hide();
  }

  openEditDiscountModal(discountId) {
    this.selectedDiscountId = discountId;
    this.getDiscount(discountId);
    this.editDiscountModal.show();
  }

  closeEditDiscountModal() {
    this.editDiscountModal.hide();
    this.selectedDiscountId = null;
    this.discount = {};
  }
}
