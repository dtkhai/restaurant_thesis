import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective, TabsetConfig} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {RoomsService} from './rooms.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SessionData} from '../../models/session.data.model';
import {AuthenticateService} from '../../common/authenticate.service';

declare var $: any;

export function getTabsetConfig(): TabsetConfig {
  return Object.assign(new TabsetConfig(), {type: 'pills'});
}

@Component({
  selector: 'app-set-rooms',
  templateUrl: 'rooms.component.html',
  providers: [RoomsService, {provide: TabsetConfig, useFactory: getTabsetConfig}]

})

export class RoomsComponent implements OnInit {
  totalItems: number;
  rooms = [];
  room = {};
  private currentPage = 1;
  private pageSize = 10;
  session: SessionData;
  rForm: FormGroup;
  selectedRoomId: number;

  @ViewChild('addNewRoomModal') addNewRoomModal: ModalDirective;
  @ViewChild('editRoomModal') editRoomModal: ModalDirective;
  @ViewChild('deleteModal') deleteModal: ModalDirective;

  constructor(private roomsService: RoomsService, private router: Router, private fb: FormBuilder,
              private toastr: ToastrService, private _authService: AuthenticateService) {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.roomsService.init(session_data.company_id);
    });
  }

  ngOnInit() {
    this._authService.getSession().subscribe(session_data => {
      this.session = session_data;
      this.roomsService.init(session_data.company_id);
    });
    this.createFrom();
    this.getRooms();
  }

  createFrom() {
    this.rForm = this.fb.group({
      room: this.fb.group({
        'name': [null, Validators.required],
      })
    });
  }

  getRooms() {
    this.roomsService.getRooms().subscribe(result => {
      if (result.success) {
        this.rooms = result.data.rooms;
        this.totalItems = result.data.total_rooms;
      } else {
        this.rooms = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getRoom(roomId) {
    this.roomsService.getRoom(roomId).subscribe(result => {
      if (result.success) {
        this.room = result.data;
      } else {
        this.room = {};
      }
    }, error => {
      console.log(error);
    });
  }

  addRoom(data) {
    this.roomsService.createRoom(data).subscribe(result => {
        this.closeAddNewRoomModal();
        this.toastr.success('Created successfully', 'Floor');
        this.getRooms();
      },
      error => {
        console.log(error);
      });
  }

  updateRoom(data) {
    this.roomsService.updateRoom(this.selectedRoomId, data).subscribe(result => {
      this.closeEditRoomModal();
      this.toastr.success('Updated successfully', 'Floor');
      this.getRooms();
    });
  }


  deleteRoom(roomId) {
    this.roomsService.deleteRoom(roomId).subscribe(result => {
      this.closeDeleteModal();
      this.toastr.success('Deleted successfully', 'Table');
      this.getRooms();
    });
  }

  // ----------------------------------------Open/ Close modal function---------------------------------

  openAddNewRoomModal() {
    this.rForm.reset();
    this.addNewRoomModal.show();
  }

  closeAddNewRoomModal() {
    this.addNewRoomModal.hide();
  }

  openEditRoomModal(roomId) {
    this.selectedRoomId = roomId;
    this.getRoom(roomId);
    this.editRoomModal.show();
  }

  closeEditRoomModal() {
    this.editRoomModal.hide();
    this.selectedRoomId = null;
    this.room = {};
  }

  openDeleteModal(roomId) {
    this.deleteModal.show();
    this.selectedRoomId = roomId;
  }

  closeDeleteModal() {
    this.deleteModal.hide();
  }
}
