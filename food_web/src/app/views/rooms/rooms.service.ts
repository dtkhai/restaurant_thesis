import {Observable} from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {ApiService} from '../../common/api.service';

@Injectable()
export class RoomsService {
  path: string;

  constructor(private _api: ApiService) {
  }

  init(companyId) {
    this.path = 'restaurants/' + companyId + '/';
  }

  public getRooms(): Observable<any> {
    const roomsUrl = this.path + 'rooms';
    return this._api.get(roomsUrl);
  }

  public getRoom(roomId): Observable<any> {
    const roomsUrl = 'rooms/';
    return this._api.get(this.path + roomsUrl + roomId);
  }

  public createRoom(body) {
    const roomsUrl = this.path + 'rooms';
    return this._api.post(roomsUrl, body);
  }

  public deleteRoom(tableId) {
    const roomsUrl = this.path + 'rooms/' + tableId;
    return this._api.delete(roomsUrl);
  }

  public updateRoom(roomId, body) {
    const roomsUrl = this.path + 'rooms/' + roomId;
    return this._api.put(roomsUrl, body);
  }
}
