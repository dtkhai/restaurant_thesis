export class SessionData {
  userId: number;
  username: string;
  token: string;
  full_name: string;
  role_id: number;
  password: string;
  email: string;
  company_id: number;

  constructor(id: number, username: string, full_name: string, token: string, role_id: number,
              password: string, email: string, company_id: number) {
    this.userId = id;
    this.username = username;
    this.token = token;
    this.full_name = full_name;
    this.role_id = role_id;
    this.password = password;
    this.email = email;
    this.company_id = company_id;
  }

  updateToken(newToken: string) {
    this.token = newToken;
  }
}
