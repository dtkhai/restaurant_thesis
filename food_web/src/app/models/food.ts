export class Food {
  id: string;
  name: string;
  image: string;
  time: Date;
  note: string;
  amount: number;
  status: number;
}
