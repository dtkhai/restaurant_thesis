import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticateService} from "../../common/authenticate.service";

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html'
})
export class AppHeaderComponent {
  constructor(private _authService: AuthenticateService, private router: Router) {
  }

  logout() {
    this.router.navigate(['login']);
    this._authService.clearSession();
  }
}
