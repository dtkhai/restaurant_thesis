// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDRvSogPLRb0S3XDkE1-K76iWBIo-rekvQ",
    authDomain: "fir-example-c1682.firebaseapp.com",
    databaseURL: "https://fir-example-c1682.firebaseio.com",
    projectId: "fir-example-c1682",
    storageBucket: "fir-example-c1682.appspot.com",
    messagingSenderId: "866630545544"
  }
};
