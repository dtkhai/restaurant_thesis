import {Injectable, OnInit} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from "rxjs/Observable";
import {Food} from "../app/models/food";
import {AppConstants} from "../app/common/const";
import * as firebase from "firebase";

@Injectable()
export class FirebaseService implements OnInit {
  private itemsCollection: AngularFirestoreCollection<Food>;
  private itemDoc: AngularFirestoreDocument<Food>;
  private foodRef: AngularFirestoreCollection<Food>;
  docId: Observable<any[]>;
  FOOD_STATUS = AppConstants.FOOD_STATUS;
  companyId: string;

  constructor(private afAuth: AngularFireAuth, public afs: AngularFirestore) {
  }

  ngOnInit() {
  }

  firebaseInit(companyId) {
    this.companyId = companyId;
    this.itemsCollection = this.afs.collection<Food>(this.companyId);

  }

  getWaitingFoods() {
    return this.afs.collection(this.companyId, ref => ref.where('status', '==', this.FOOD_STATUS.WAITING)).valueChanges();
  }

  getCookingFoods() {
    return this.afs.collection(this.companyId, ref => ref.where('status', '==', this.FOOD_STATUS.COOKING)).valueChanges();
  }

  getItem(foodId) {
    this.foodRef = this.afs.collection(this.companyId, ref => ref.where('id', '==', foodId.toString()));
    return this.docId = this.foodRef.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Food;
        const id = a.payload.doc.id;
        return {id, ...data};
      });
    });
  }

  addItem(name: string) {
    const status = 0;
    // Persist a document id
    // const id = this.afs.createId();
    const item: Food = {
      id: '5',
      name: name,
      amount: 3,
      image: '',
      note: 'nhieu tieu',
      time: new Date(),
      status: status
    };
    this.itemsCollection.doc('5').set(item);
  }

  updateItem(itemId, status) {
    this.itemDoc = this.afs.doc<Food>(this.companyId + '/' + itemId);
    this.itemDoc.update({status: status});
  }

  deleteItem(id) {
    this.itemDoc = this.afs.doc<Food>(this.companyId + '/' + id);
    this.itemDoc.delete();
  }
}
