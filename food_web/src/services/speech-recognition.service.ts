import {Injectable, NgZone} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import * as _ from 'lodash';

interface IWindow extends Window {
  webkitSpeechRecognition: any;
  webkitSpeechGrammarList: any;
  SpeechRecognition: any;
  SpeechSynthesisUtterance: any;
  speechSynthesis: any;
}

@Injectable()
export class SpeechRecognitionService {
  speechRecognition: any;
  isStarted = false;

  constructor(private zone: NgZone) {
  }

  record(): Observable<string> {

    return Observable.create(observer => {

      const {webkitSpeechRecognition}: IWindow = <IWindow>window;
      const {webkitSpeechGrammarList}: IWindow = <IWindow>window;

      this.speechRecognition = new webkitSpeechRecognition();

      let words1 = ['cook', 'cancel', 'finish'];
      let granmmar = '#JSGF V1.0; grammar actions; public <actions> = ' + words1.join(' | ') + ';';
      let speechRecognitionList = new webkitSpeechGrammarList();
      speechRecognitionList.addFromString(granmmar, 1);
      this.speechRecognition.grammars = speechRecognitionList;
      this.speechRecognition.continuous = false;
      this.speechRecognition.interimResults = false;
      this.speechRecognition.lang = 'en-US';
      this.speechRecognition.maxAlternatives = 1;


      if (this.isStarted == false) {
        this.speechRecognition.start();
        this.isStarted = true;
      }
      this.speechRecognition.onresult = speech => {
        let term: string = '';
        if (speech.results) {
          let result = speech.results[speech.resultIndex];
          let transcript = result[0].transcript;
          if (result.isFinal) {
            if (result[0].confidence < 0.5) {
              console.log("Unrecognized result - Please try again");
            }
            else {
              term = _.trim(transcript);
              console.log("Did you said? -> " + term + " , If not then say something else...");
            }
          }
        }
        this.zone.run(() => {
          observer.next(term);
        });
      };

      this.speechRecognition.onerror = error => {
        observer.error(error);
      };

      this.speechRecognition.onend = () => {


        observer.complete();
        setTimeout(() => {
          this.speechRecognition.start()
        }, 500);
      };


      //this.speechRecognition.start();
      console.log('Say something - We are listening !!!');
    });
  }

  DestroySpeechObject() {
    if (this.speechRecognition)
      this.speechRecognition.stop();
  }

}

export class SpeakSynthesis {
  constructor() {
    if ('speechSynthesis' in window) {
      console.log('Your browser supports speech synthesis.');
      // speak('hi');
    } else {
      alert('Sorry your browser does not support speech synthesis.' +
        ' Try this in <a href="https://www.google.com/chrome/browser/desktop/index.html">Google Chrome</a>.');
    }
  }

  speak(input: String) {
    const {SpeechSynthesisUtterance}: IWindow = <IWindow>window;
    const {speechSynthesis}: IWindow = <IWindow>window;

    // Create a new instance of SpeechSynthesisUtterance.
    var msg = new SpeechSynthesisUtterance();
    var voices = window.speechSynthesis.getVoices();

    // Set the text.
    msg.text = input;
    // Set the attributes.
    msg.lang = 'en-US';
    // msg.voice = 'native'; msg.voice = 'Google US English'; //  'Google UK English Female'
    msg.voice = voices[0];
    msg.volume = 1;
    msg.rate = 1;
    msg.pitch = 1;
    //  msg.onend = function(event) { console.log('Speech complete'); }
    // Queue this utterance.
    // let talk = new speechSynthesis();
    speechSynthesis.speak(msg);
  }
}

