var express = require('express');
var log4js = require('log4js');
var cors = require('cors');

var foods_routes = require('./routes/foods');
var discounts_routes = require('./routes/discounts');
var users_routes = require('./routes/users');
var order_histories_routes = require('./routes/order_histories');
var tables_routes = require('./routes/tables');
var rooms_routes = require('./routes/rooms');
var restaurants_routes = require('./routes/restaurants');
var smart_discount_routes = require('./routes/smart_discount');

var app = express();

/**
 * Enable CORS for all routes
 */
// app.use(cors());
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    
  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
};
app.use(allowCrossDomain);
/**
 * Enable the use of body parser to get POST params
 */
var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

/**
 * Setup routes
 */
app.use('/restaurants', restaurants_routes);
app.use('/foods', foods_routes);
app.use('/discounts', discounts_routes);
app.use('/users', users_routes);
app.use('/order-histories', order_histories_routes);
app.use('/tables', tables_routes);
app.use('/rooms', rooms_routes);
app.use('/smart-discount', smart_discount_routes);

app.use(express.static('public'));


/**
 * Setup logger
 */
var logger = log4js.getLogger("http");
app.use(log4js.connectLogger(logger));

/**
 * Catch 404 and forward to error handler
 */
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/**
 * Error handlers
 */
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
    .json({
      success: false,
      message: err.message,
      code: err
    });
});

module.exports = app;