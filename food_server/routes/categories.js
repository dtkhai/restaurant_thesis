var express = require('express');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");
var categoryServices = require('../services/category_services');
var router = express.Router();


router.get('/', function (req, res) {
  var currentPage = req.query.current_page;
  var pageSize = req.query.page_size;
  var company_id = req.restaurantId;

  categoryServices.GetGroups(currentPage, pageSize, company_id, function (error, groups) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, groups);
    }
  });
});

router.get('/:id', function (req, res) {
  var categoryId = req.params.id

  categoryServices.GetGroup(categoryId, function (error, group) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, group);
    }
  });
});

router.post('/', function (req, res) {
  var name = req.body.name;
  var company_id = req.restaurantId;
  var group = { 'name': name, 'company_id': company_id }


  categoryServices.CreateGroup(group, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.put('/:id', function (req, res) {
  var name = req.body.name;
  var categoryId = req.params.id;
  var group = { 'id': categoryId, 'name': name };

  categoryServices.UpdateGroup(group, function (error, groups) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, groups);
    }
  });
});


router.delete('/:id', function (req, res) {
  var categoryId = req.params.id

  categoryServices.DeleteGroup(categoryId, function (error, groups) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, groups);
    }
  });
});

module.exports = router;