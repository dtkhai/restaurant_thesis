var express = require('express');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");
var foodServices = require('../services/food_services');
var router = express.Router();

/**
 * @api {get} /foods/ Get All Foods
 * @apiDescription Get all foods from database and return list of foods based pagenation setting.
 * @apiVersion 1.0.0
 * @apiName GetFoods
 * @apiPermission everyone
 * @apiGroup Food
 *
 * @apiParam {Number} [current_page = 1] Current page value.
 * @apiParam {Number} [page_size = 20] Number of foods per page value.
 *
 * @apiSuccess {Boolean} success TRUE if succeed / FALSE if failed.
 * @apiSuccess {Object} data Foods list with pagenation info.
 * @apiSuccess {Number} data.total_items Total number of foods.
 * @apiSuccess {Object[]} data.foods List of food accounts of requested page (array of object).
 * @apiSuccess {Number} data.foods.id Food id.
 * @apiSuccess {String} data.foods.name Food name.
 * @apiSuccess {String} data.foods.avatar Food avatar.
 * @apiSuccess {String} data.foods.description Description of the food.
 * @apiSuccess {Number} data.foods.price Food price.

 * @apiSuccess {Object[]} data.foods.roles List of role ids that food belongs. If there is no roles, this food is in Employee role.
 * @apiSuccess {Number} data.foods.roles.role_id Role id that food belongs to.
 * @apiSuccess {Datetime} data.foods.last_updated Timestamps of last synced from .
 *
 * @apiSuccessExample Success Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "data": {
 *          "total_items": 15,
 *          "foods": [{
 *              "id": 5,
 *              "name": "BÚN BÒ HUẾ",
 *              "avatar": "['http://product.hstatic.net/1000205756/product/bun-bo-hue_c53c0d359aac47e8b758000462db99ee_master.png']",
 *              "description": "Bún bò món Huế của Nhà hàng món Huế được làm từ nguyên liệu chính là: Thịt bò, thịt heo, chả cua, huyết heo, chả thẻ, sợi bún lớn và...",
 *              "price": "65,000",
 *            }]
 *       }
 *     }
 * @apiSuccessExample Failed Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "message": "You are not authorized to access this page!",
 *       "code": "AUTHORIZE_01"
 *     }
 */
router.get('/', function (req, res) {
  var currentPage = req.query.current_page;
  var pageSize = req.query.page_size;
  var company_id = req.restaurantId;

  foodServices.GetFoods(currentPage, pageSize, company_id, function (error, foods) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, foods);
    }
  });
});

router.post('/getByIds', function (req, res) {
  var food_ids = req.body.food_ids;
  console.log(food_ids);

  foodServices.GetFoodByIds(food_ids, function (error, foods) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, foods);
    }
  });
});


/**
 * @api {post} /foods Create New Food
 * @apiDescription Create a new food.
 * @apiVersion 1.0.0
 * @apiName CreateFood
 * @apiPermission manager_account
 * @apiGroup Food
 *
 * @apiParam {String} name Food name.
 * @apiParam {String} main_image Food Image.
 * @apiParam {String} images Food images.
 * @apiParam {Number} price Food price.
 * @apiParam {Number} discount id Food discount id.
 * @apiParam {String} description Food description.
 * @apiParam {String} note Food note.
 * @apiParam {String} food group id Food food group id.
 *
 * @apiSuccess {Boolean} success TRUE if succeed / FALSE if failed.
 * @apiSuccess {Object} data Created section data.
 * @apiSuccess {String} data.section.name Food name.
 * @apiSuccess {String} data.section.description Food description.
 * @apiSuccess {String} data.section.duration Food duration.

 *
 * @apiSuccessExample Success Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "data": {
 *          "section": {
 *              "id": 1,
 *              "name": "Bun bo hue"",
 *              "main_image": "https://www.w3schools.com/w3css/img_fjords.jpg",
 *              "images": ["https://www.w3schools.com/w3css/img_fjords.jpg", "https://imagejournal.org/wp-content/uploads/bb-plugin/cache/23466317216_b99485ba14_o-panorama.jpg"],
 *              "price": 50000,
 *              "discount id": 0,
 *              "description": "is a popular Vietnamese soup containing rice vermicelli",
 *              "note": "it bo",
 *              "food_group_id": 1,
 *          }
 *       }
 *     }
 * @apiSuccessExample Failed Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "message": "You are not authorized to access this page!",
 *     }
 */

router.post('/', function (req, res) {
  var company_id = req.restaurantId;
  var name = req.body.name;
  var price = req.body.price;
  var main_image = req.body.main_image;
  var images = req.body.images;
  var description = req.body.description;
  var note = req.body.note;
  var food_group_ids = req.body.food_group_ids;

  console.log(food_group_ids);

  foodServices.CreateFood(name, price, main_image, images, description, note, food_group_ids, company_id, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

/**
 * @api {delete} /foods/:id Delete Food
 * @apiDescription Teaching and study managing department delete a food
 * @apiVersion 1.0.0
 * @apiName DeleteFood
 * @apiPermission manager_account
 * @apiGroup Food
 *
 * @apiParam {String} token Food token string to authorize.
 * @apiParam {Number} id Food id.
 *
 * @apiSuccess {Boolean} success TRUE if succeed / FALSE if failed.
 *
 * @apiSuccessExample Success Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true
 *     }
 * @apiSuccessExample Failed Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "message": "Cannot delete this food",
 *     }
 */
router.delete('/:id', function (req, res) {
  var foodId = req.params.id;
  foodServices.DeleteFood(foodId, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

/**
 * @api {put} /foods/:id Update Food
 * @apiDescription Update information of foods.
 * @apiVersion 1.0.0
 * @apiName UpdateFood
 * @apiPermission manager_account
 * @apiGroup Food
 *
 * @apiSuccess {Boolean} success TRUE if succeed / FALSE if failed.
 *
 * @apiParam {String} token User token string to authorize.
 * @apiParam {String} id Food id.
 * @apiParam {String} name Food name.
 * @apiParam {String} main_image Food Image.
 * @apiParam {String} images Food images.
 * @apiParam {Number} price Food price.
 * @apiParam {Number} discount id Food discount id.
 * @apiParam {String} description Food description.
 * @apiParam {String} note Food note.
 * @apiParam {String} food group id Food food group id.
 *
 * @apiSuccessExample Success Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *     }
 ** @apiSuccessExample Failed Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "message": "You are not authorized to access this page!",
 *     }
 */
router.put('/:id', function (req, res) {
  var foodId = req.params.id;
  var name = req.body.name;
  var price = req.body.price;
  var main_image = req.body.main_image;
  var images = req.body.images;
  var description = req.body.description;
  var note = req.body.note;
  var food_group_ids = req.body.food_group_ids;

  foodServices.UpdateFood(foodId, name, price, main_image, images, description, note, food_group_ids, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.post('/:id/food-available', function (req, res) {
  var foodId = req.params.id;

  foodServices.UpdateFoodStatus(foodId, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

/**
 * @api {post} /foods/search Search Foods
 * @apiDescription Search foods via account and fullname and get list of foods with pagenation setting.
 * @apiVersion 1.0.0
 * @apiName SearchFoods
 * @apiPermission everyone
 * @apiGroup Food
 *
 * @apiParam {String} token User token string to authorize.
 * @apiParam {String} search_string Search text (food account or fullname).
 * @apiParam {Number} [current_page = 1] Current page value.
 * @apiParam {Number} [page_size = 20] Number of foods per page value.
 *
 * @apiSuccess {Boolean} success TRUE if succeed / FALSE if failed.
 * @apiSuccess {Object} data Foods list with pagenation info.
 * @apiSuccess {Number} data.total_items Total number of foods.

 * @apiSuccessExample Success Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "data": {
 *          "total_items": 45,
 *          "foods": [{
 *              "id": 5,
 *              "name": "Bun bo hue"",
 *              "main_image": "https://www.w3schools.com/w3css/img_fjords.jpg",
 *              "images": ["https://www.w3schools.com/w3css/img_fjords.jpg", "https://imagejournal.org/wp-content/uploads/bb-plugin/cache/23466317216_b99485ba14_o-panorama.jpg"],
 *              "price": 50000,
 *              "discount id": 0,
 *              "description": "is a popular Vietnamese soup containing rice vermicelli",
 *              "note": "it bo",
 *              "food_group_id": 1,
 *            }]
 *       }
 *     }
 * @apiSuccessExample Failed Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "message": "You are not authorized to access this page!",
 *       "code": "AUTHORIZE_01"
 *     }
 */
router.get('/filter', function (req, res) {
  var groupId = req.query.group_id;
  var currentPage = req.query.current_page;
  var pageSize = req.query.page_size;

  foodServices.FilterFoods(groupId, currentPage, pageSize, function (error, foods) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, foods);
    }
  });
});


router.get('/search', function (req, res) {
  var search_string = req.query.search_string;
  var currentPage = req.query.current_page;
  var pageSize = req.query.page_size;

  foodServices.SearchFoods(search_string, currentPage, pageSize, function (error, foods) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, foods);
    }
  });
});


router.get('/:id', function (req, res) {
  var foodId = req.params.id;

  foodServices.GetFood(foodId, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});


router.post('/recommendation', function (req, res) {
  var user_id = req.body.user_id;
  var company_id = req.restaurantId;

  foodServices.Recommendation(user_id, company_id).then(
      resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
      error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
  );
});

module.exports = router;