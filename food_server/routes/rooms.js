var express = require('express');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");
var RoomServices = require('../services/room_services');
var router = express.Router();


router.get('/', function (req, res) {
  var company_id = req.restaurantId;

  RoomServices.GetRooms(company_id, function (error, rooms) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, rooms);
    }
  });
});

router.get('/:id', function (req, res) {
  var roomId = req.params.id;

  RoomServices.GetRoom(roomId, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});


router.post('/', function (req, res) {
  var room = req.body.room;
  room.company_id = req.restaurantId;
  RoomServices.CreateRoom(room, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.put('/:id', function (req, res) {
  var roomId = req.params.id;
  var room = req.body.room;
  
  RoomServices.UpdateRoom(roomId, room, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.delete('/:id', function (req, res) {
  var roomId = req.params.id;

  RoomServices.DeleteRoom(roomId).then(
      resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
      error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
  );
});

module.exports = router;