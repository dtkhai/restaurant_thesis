var express = require('express');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");
var tableServices = require('../services/table_services');
var router = express.Router();

router.get('/', function (req, res) {
    var currentPage = req.query.current_page;
    var pageSize = req.query.page_size;
    var roomId = req.query.room_id;

    tableServices.GetTables(currentPage, pageSize, roomId).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.get('/:id', function (req, res) {
    var tableId = req.params.id;

    tableServices.GetTable(tableId).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.post('/', function (req, res) {
    var name = req.body.name;
    var capacity = req.body.capacity;
    var room_id = req.body.room_id;

    tableServices.CreateTable(name, capacity, room_id).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.put('/:id', function (req, res) {
    var tableId = req.params.id;
    var name = req.body.name;
    var capacity = req.body.capacity;

    tableServices.UpdateTable(tableId, name, capacity).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.post('/prefix', function (req, res) {
    var prefix = req.body.prefix;
    var roomId = req.body.room_id;

    tableServices.UpdatePrefix(prefix, roomId).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.post('/update-position', function (req, res) {
    var tableMap = req.body.table_map;
    console.log(tableMap);

    tableServices.UpdateTablePosition(tableMap).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.delete('/:id', function (req, res) {
    var tableId = req.params.id;

    tableServices.DeleteTable(tableId).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

module.exports = router;