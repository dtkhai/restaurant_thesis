var express = require('express');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");
var orderHistoryServices = require('../services/order_history_services');
var router = express.Router();
var Sequelize = require('sequelize');

router.get('/', function (req, res) {
  var year = req.query.chosen_year;
  var month = req.query.chosen_month;
  var company_id = req.restaurantId;


  orderHistoryServices.GetOrderHistories(company_id, year, month).then(
    resolved => utils.ResponseWithToken(req, res, resolved),
    error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
  );
});

router.get('/search', function (req, res) {
  var currentPage = req.query.current_page;
  var pageSize = req.query.page_size;
  var order_date = req.query.order_date;

  orderHistoryServices.SearchOrderHistories(order_date, currentPage, pageSize).then(
    resolved => utils.ResponseWithToken(req, res, resolved),
    error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
  );
});

router.post('/', function (req, res) {
  var order_list = req.body.order_list;
  var order_parent = req.body.order_parent;
  var canceled = req.body.canceled;
  var total_price = req.body.total_price;
  var company_id = req.restaurantId;
  
  orderHistoryServices.CreateOrderHistory(order_list, order_parent, canceled, company_id, total_price).then(
    resolved => utils.ResponseWithToken(req, res, resolved),
    error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
  );
});

module.exports = router;