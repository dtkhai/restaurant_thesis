var express = require('express');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");
var discountServices = require('../services/discount_services');
var router = express.Router();

router.get('/', function (req, res) {
  var company_id = req.restaurantId;

  discountServices.GetDiscountSmartly(company_id, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

module.exports = router;

