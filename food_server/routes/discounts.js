var express = require('express');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");
var discountServices = require('../services/discount_services');
var router = express.Router();

router.get('/', function (req, res) {
  var currentPage = req.query.current_page;
  var pageSize = req.query.page_size;
  var isActive = req.query.active;
  var company_id = req.restaurantId;

  discountServices.GetDiscounts(currentPage, pageSize, isActive, company_id, function (error, discounts) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, discounts);
    }
  });
});


router.get('/:id', function (req, res) {
  var discountId = req.params.id;

  discountServices.GetDiscount(discountId, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});


router.post('/bill-discount', function (req, res) {
  var company_id = req.restaurantId;

  discountServices.GetDiscountAllBill(company_id, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.post('/', function (req, res) {
  var discount = req.body.discount;
  discount.active = true;
  discount.enable = true;
  discount.company_id = req.restaurantId;
  var food_ids = req.body.food_ids;
  var combo_ids = req.body.combo_ids;

  console.log('router');
  console.log(combo_ids);

  discountServices.CreateDiscount(discount, food_ids, combo_ids).then(
    resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess()),
    error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
  );
});


router.put('/:id', function (req, res) {
  var discountId = req.params.id;
  var discount = req.body.discount;

  discountServices.UpdateDiscount(discountId, discount, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.put('/:id/change-status-discount', function (req, res) {
  var discountId = req.params.id;
  var isActive = req.query.active;

  discountServices.changeStatusDiscount(discountId, isActive, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.put('/:id/enable', function (req, res) {
  var discountId = req.params.id;
  var isActive = req.query.enable;

  discountServices.changeEnableStatus(discountId, isActive, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

module.exports = router;