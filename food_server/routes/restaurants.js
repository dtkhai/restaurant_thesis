var express = require('express');
var router = express.Router();
const foods = require('./foods');
const categories = require('./categories');
const rooms = require('./rooms');
const tables = require('./tables')
const discounts = require('./discounts');
const smartDiscount = require('./smart_discount');
const order_histories = require('./order_histories');
const users = require('./users');
const combos = require('./combos');
var companyServices = require('../services/restaurant_services');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");

router.get('/', function (req, res) {
    var currentPage = req.query.current_page;
    var pageSize = req.query.page_size;

    companyServices.GetCompanies(currentPage, pageSize).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.post('/', function (req, res) {
    // var name = req.body.name;
    // var main_image = req.body.main_image;
    // var description = req.body.description;
    // var country = req.body.country;
    // var district = req.body.district;
    // var address = req.body.address;
    // var user_id = req.body.user_id;
    var company = req.body.company;

    companyServices.CreateCompany(company).then(
        resolved => utils.ResponseWithToken(req, res, resolved),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.delete('/:id', function (req, res) {
    var companyId = req.params.id;
    companyServices.DeleteCompany(companyId).then(
        resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
        error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
    );
});

router.put('/:id', function (req, res) {
    var companyId = req.params.id;
    var company = req.body.company;

    companyServices.UpdateCompany(companyId, company, function (error, result) {
        if (error) {
            utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
        }
        else {
            utils.ResponseWithToken(req, res, result);
        }
    });
});

router.get('/search', function (req, res) {
    var search_string = req.query.search_string;
    var currentPage = req.query.current_page;
    var pageSize = req.query.page_size;

    companyServices.SearchCompanies(search_string, currentPage, pageSize, function (error, companys) {
        if (error) {
            utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
        }
        else {
            utils.ResponseWithToken(req, res, companys);
        }
    });
});

// Update profile
router.get('/:id', function (req, res) {
    var companyId = req.params.id;

    companyServices.GetCompany(companyId, function (error, result) {
        if (error) {
            utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
        }
        else {
            utils.ResponseWithToken(req, res, result);
        }
    });
});

// ------------------------------- Nested Routers -------------------------------
router.use('/:restaurantId/foods', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, foods);

router.use('/:restaurantId/groups', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, categories);

router.use('/:restaurantId/rooms', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, rooms);

router.use('/:restaurantId/tables', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, tables);

router.use('/:restaurantId/discounts', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, discounts);

router.use('/:restaurantId/smart-discount', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, smartDiscount);

router.use('/:restaurantId/order-histories', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, order_histories);

router.use('/:restaurantId/users', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, users);

router.use('/:restaurantId/combos', function (req, res, next) {
    req.restaurantId = req.params.restaurantId;
    next()
}, combos);

module.exports = router;
