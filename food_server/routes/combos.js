var express = require('express');
var utils = require("../lib/utils");
var auth_utils = require("../lib/auth_utils");
var comboServices = require('../services/combo_services');
var router = express.Router();

router.get('/', function (req, res) {
  var currentPage = req.query.current_page;
  var pageSize = req.query.page_size;
  var company_id = req.restaurantId;

  comboServices.GetCombos(currentPage, pageSize, company_id, function (error, combos) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, combos);
    }
  });
});


router.get('/:id', function (req, res) {
  var comboId = req.params.id;

  comboServices.GetCombo(comboId, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.post('/', function (req, res) {
  var combo = req.body.combo;
  var food_ids = req.body.food_ids;
  combo.company_id = req.restaurantId;

  console.log('vao');
  comboServices.CreateCombo(combo, food_ids, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});


router.put('/:id', function (req, res) {
  var comboId = req.params.id;
  var combo = req.body.combo;
  var food_ids = req.body.food_ids;

  comboServices.UpdateCombo(comboId, combo, food_ids, function (error, result) {
    if (error) {
      utils.ResponseWithToken(req, res, utils.ResultServiceError(error));
    }
    else {
      utils.ResponseWithToken(req, res, result);
    }
  });
});

router.delete('/:id', function (req, res) {
  var comboId = req.params.id;

  comboServices.DeleteTable(comboId).then(
    resolved => utils.ResponseWithToken(req, res, utils.ResultSuccess(resolved)),
    error => utils.ResponseWithToken(req, res, utils.ResultServiceError(error))
  );
});



module.exports = router;