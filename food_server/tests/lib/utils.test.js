var assert = require('chai').assert;
var utils = require('../../lib/utils');

describe("Utils", function () {

    describe("#GetTotalPages", function () {
        it("should return correct value when page size is not devided by total records", function () { 
            assert.equal(utils.GetTotalPages(10, 3), 4);
        });

        it("should return correct value when page size is devided by total records", function () { 
            assert.equal(utils.GetTotalPages(4, 2), 2);
        });

        it("should return 1 when total records is less than page size", function () { 
            assert.equal(utils.GetTotalPages(5, 10), 1);
        });

        it("should return 0 when total records is 0", function () { 
            assert.equal(utils.GetTotalPages(0, 10), 0);
        });

        it("should return 0 when page size is 0", function () { 
            assert.equal(utils.GetTotalPages(5, 0), 0);
        });
    });

    describe("#FormatSearchString", function () {
        it("should return string with '%' characters in first and last places if string does not have any space characters", function () { 
            assert.equal(utils.FormatSearchString('message_with_out_space'), '%message_with_out_space%');
        });

        it("should return string with '%' characters in first, last and space places if string has space characters", function () { 
            assert.equal(utils.FormatSearchString('message with space characters'), '%message%with%space%characters%');
        });

        it("should return string with multiple '%' characters if there are multiple space characters in string", function () { 
            assert.equal(utils.FormatSearchString('test  message'), '%test%%message%');
        });

        it("should return '%%' when the input is empty string", function () { 
            assert.equal(utils.FormatSearchString(''), '%%');
        });

        it("should return '%%' when the input is null", function () { 
            assert.equal(utils.FormatSearchString(null), '%%');
        });
    });

    describe("#ConvertParamToArray", function () {
        it("should return array of Number if the input is array without space", function () { 
            assert.isArray(utils.ConvertParamToArray('[1,2,3,4]', Number));
        });

        it("should return array of Number if the input is array contains space characters", function () { 
            assert.isArray(utils.ConvertParamToArray('[ 1,  2,   3]', Number));
        });
    });

    describe("#ResultError", function () {
        it("should return error json data format when message and code are defined", function () { 
            var errorResponse = utils.ResultError('test message', 'CODE_01');
            var expectedResponse = {
                "success": false,
                "message": 'test message',
                "code": 'CODE_01'
            };
            assert.deepEqual(errorResponse, expectedResponse);
        });

        it("should return error json data format when message and code are null or empty", function () { 
            var errorResponse = utils.ResultError(null, '');
            var expectedResponse = {
                "success": false,
                "message": null,
                "code": ''
            };
            assert.deepEqual(errorResponse, expectedResponse);
        });
    });

    describe("#ResultSuccess", function () {
        it("should return success json data format without data when there is no input parameter", function () { 
            var successResponse = utils.ResultSuccess();
            var expectedResponse = { "success": true };
            assert.deepEqual(successResponse, expectedResponse);
        });

        it("should return success json data format without data when data is null", function () { 
            var successResponse = utils.ResultSuccess(null);
            var expectedResponse = { "success": true };
            assert.deepEqual(successResponse, expectedResponse);
        });

        it("should return success json data format with data when data is a string", function () { 
            var successResponse = utils.ResultSuccess('test data');
            var expectedResponse = {
                "success": true,
                "data": 'test data'
            };
            assert.deepEqual(successResponse, expectedResponse);
        });

        it("should return success json data format with data when data is a json object", function () { 
            var data = {
                "user": {
                    "first_name": "Tuan",
                    "last_name": "Ngo",
                    "age": 18
                }
            };
            var successResponse = utils.ResultSuccess(data);
            var expectedResponse = {
                "success": true,
                "data": {
                    "user": {
                        "first_name": "Tuan",
                        "last_name": "Ngo",
                        "age": 18
                    }
                }
            };
            assert.deepEqual(successResponse, expectedResponse);
        });
    });
});