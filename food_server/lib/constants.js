module.exports = {
    DEFAULT_PAGE_SIZE: 10,
    SECRET: 'thesis_f_b',
    AUTHENTICATE_EXPIRE: '60d', // using rauchg/ms.js
    AUTHENTICATE_RESIGN_TOKEN: 86400, // second -- 86400s = 1d
    WEB_HOST_NAME: 'http://10.0.3.2:8765',
    DEFAULT_TABLE_CAPACITY: 4,
}
