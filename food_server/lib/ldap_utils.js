var ldap = require('ldapjs');
var async = require('async');
var log4js = require('log4js');

var utils = require("../lib/utils");
var models = require('../models');

var logger = log4js.getLogger("user_service");

exports.LDAPAuthenticate = function (username, password, callback) {
	var client = ldap.createClient({
		url: 'ldap://192.168.100.65'
	});

	client.on('error', function (err) {
		logger.error(err);
		return callback(err);
	});

	var formatedUsername = "uid=" + username + ",ou=Users,dc=imttool,dc=com";

	client.bind(formatedUsername, password, function (err) {
		logger.info(err);
		if (err === null) {
			return callback(null);
		}
		else {
			client.unbind();
			return callback(err);
		}
	});
}

exports.GetUsersFromLDAP = function (callback) {
	var client = ldap.createClient({
		url: 'ldap://192.168.100.65'
	});

	var opts = {
		filter: '(mail=*@imt-soft.com)',
		scope: 'sub'
		//attributes: ['dn', 'sn', 'cn']
	};

	var users = [];

	client.search('ou=Users,dc=imttool,dc=com', opts, function (err, res) {
		if (err !== null) {
			return callback(err, null);
		}

		res.on('searchEntry', function (entry) {
			users.push(entry.object);
			console.log('entry: ' + JSON.stringify(entry.object));
		});

		res.on('searchReference', function (referral) {
			console.log('referral: ' + referral.uris.join());
		});

		res.on('error', function (err) {
			console.error('error: ' + err.message);
			return callback(null, utils.ResultError(err.message, err.code));
		});

		res.on('end', function (result) {
			console.log('status: ' + result.status);
			return callback(null, utils.ResultSuccess(users));
		});
	});

}

exports.syncFromLDAP = function (users, callback) {
	var new_users = 0;
	var error_users = 0;
	var deactivated_users = 0;

	// get all users from db and filter the deleted from LDAP
	models.User.findAll()
		.then(function (dbUsers) {
			// then we will have the list of users that has been removed from LDAP but still exists in our db
			for (var i = dbUsers.length - 1; i >= 0; i--) {
				for (var j = 0; j < users.length; j++) {
					if (dbUsers[i] && (dbUsers[i].account === users[j].mail)) {
						dbUsers.splice(i, 1);
					}
				}
			}

			// then we will have the list of users that has been removed from LDAP but still exists in our db
			dbUsers.forEach(function (dbUser) {
				dbUser.updateAttributes({
					'active': false
				})
					.then(function () {
						deactivated_users += 1;
					})
					.catch(function (err) {
						error_users += 1;
					});
			});

			async.forEach(users, function (user, next) {
				models.User.findOne({
					where: {
						account: user.mail
					}
				})
					.then(function (foundedUser) {
						if (foundedUser) {
							// current user exists
							foundedUser.updateAttributes({
								'fullname': user.displayName,
								'active': true,
								'updated_at': new Date()
							}).catch(function (err) {
								error_users += 1;
							});
						} else {
							// user do not exists
							models.User.create({
								'account': user.mail,
								'fullname': user.displayName,
								'active': true, // TODO: need to check
								'last_login': null,
								'created_at': new Date(),
								'updated_at': new Date()
							}).then(function (instance) {
								new_users += 1;
							}).catch(function (err) {
								error_users += 1;
							});
						}
						next(new_users, error_users);
					});
			}, function (err) {
				var data = {
					"new_users": new_users,
					"deactivated_users": deactivated_users,
					"error_users": error_users
				};
				return callback(null, utils.ResultSuccess(data));
			});

		})
		.catch(function (error) {
			logger.error(error.message);
			return callback(error.message, null);
		});
}