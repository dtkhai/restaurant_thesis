module.exports = {
    PA_REQUEST_STATUS: {
        NEW: 1,
        APPRAISER_SUBMITED: 2,
        PA_SUBMITED: 3
    },
    PA_FEEDBACK_STATUS: {
        NEW: 1,
        SUBMITED: 2
    },
    PA_INFORMATION: {
        NEW: 1,
        STARTED: 2,
        COMLETED: 3
    }
}