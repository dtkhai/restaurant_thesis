'use strict';

module.exports = {
  up: function (migration, DataTypes, done) {
    migration.createTable('combos', {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
      name: DataTypes.STRING,
      image: DataTypes.STRING,
      price: DataTypes.INTEGER,
      discount_id: DataTypes.INTEGER,
      company_id: DataTypes.INTEGER,
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    }).then(function () {
      migration.createTable('combo_foods', {
        id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
        combo_id: DataTypes.INTEGER,
        food_id: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
      }).then(function () {
        return done();
      });
    });
  },

  down: function (migration, DataTypes, done) {
    migration.dropTable('combos').then(function () {
      migration.dropTable('combo_foods').then(function () {
        return done();
      });
    });
  }
};