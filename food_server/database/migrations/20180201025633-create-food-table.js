'use strict';

module.exports = {
  up: function (migration, DataTypes, done) {
    migration.createTable('food_groups', {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
      name: DataTypes.STRING,
      company_id: DataTypes.INTEGER,          
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    }).then(function () {
      migration.createTable('foods', {
        id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
        name: DataTypes.STRING,
        main_image: DataTypes.STRING,
        images: DataTypes.ARRAY(DataTypes.JSON),
        description: DataTypes.TEXT,
        price: DataTypes.INTEGER,
        discount_id: DataTypes.INTEGER,          
        company_id: DataTypes.INTEGER,          
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
      }).then(function () {
        migration.createTable('rooms', {
          id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
          name: { type: DataTypes.STRING, unique: true, allowNull: false },
          company_id: DataTypes.INTEGER,
          available: DataTypes.INTEGER,
          created_at: DataTypes.DATE,
          updated_at: DataTypes.DATE
        }).then(function () {
          migration.createTable('tables', {
            id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
            name: DataTypes.STRING,
            capacity: DataTypes.INTEGER,
            room_id: { type: DataTypes.INTEGER, allowNull: false, references: { model: 'rooms', key: 'id', onDelete: 'cascade' } },
            company_id: DataTypes.INTEGER,
            available: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
          }).then(function () {
            return done();
          });
        });
      });
    });
  },

  down: function (migration, DataTypes, done) {
    migration.dropTable('foods').then(function () {
      migration.dropTable('food_groups').then(function () {
        migration.dropTable('tables').then(function () {
          migration.dropTable('rooms').then(function () {
            return done();
          });
        });
      });
    });
  }
};