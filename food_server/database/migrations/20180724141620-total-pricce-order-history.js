'use strict';

module.exports = {

  up: function (migration, DataTypes, done) {
    migration.addColumn('order_histories', 'total_price', DataTypes.INTEGER)
      .then(function () {
        return done();
      });
  },

  down: function (migration, DataTypes, done) {
    migration.removeColumn('order_histories', 'total_price').then(function () {
      return done();
    });
  }
};