'use strict';

module.exports = {

  up: function (migration, DataTypes, done) {
    migration.addColumn('tables', 'prefix', DataTypes.STRING)
      .then(function () {
        migration.addColumn('tables', 'column', DataTypes.INTEGER)
      }).then(function () {
        migration.addColumn('tables', 'row', DataTypes.INTEGER)
      }).then(function () {
        return done();
      });
  },

  down: function (migration, DataTypes, done) {
    migration.removeColumn('tables', 'prefix').then(function () {
      migration.removeColumn('tables', 'column').then(function () {
        migration.removeColumn('tables', 'row').then(function () {
          return done();
        });
      });
    });
  }
};