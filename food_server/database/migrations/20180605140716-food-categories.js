'use strict';

module.exports = {
  up: function (migration, DataTypes, done) {
    migration.createTable('food_categories', {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
      category_id: DataTypes.INTEGER,
      food_id: DataTypes.INTEGER,
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    }).then(function () {
      migration.createTable('categories', {
        id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
        name: DataTypes.STRING,
        company_id: DataTypes.INTEGER,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
      }).then(function () {
        return done();
      });
    });
  },

  down: function (migration, DataTypes, done) {
    migration.dropTable('food_categories').then(function () {
      migration.dropTable('categories').then(function () {
        return done();
      });
    });
  }
};