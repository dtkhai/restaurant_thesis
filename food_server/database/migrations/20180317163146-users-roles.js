'use strict';

module.exports = {
  up: function (migration, DataTypes, done) {
    migration.createTable('companies', {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      image: DataTypes.STRING,
      country: DataTypes.STRING,
      district: DataTypes.STRING,
      address: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    }).then(function () {
      migration.createTable('users', {
        id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
        fname: { type: DataTypes.STRING, unique: true, allowNull: false },
        lname: DataTypes.STRING,
        password: DataTypes.STRING,
        age: DataTypes.INTEGER,
        description: DataTypes.STRING,
        email: DataTypes.STRING,
        phone: DataTypes.STRING,
        role_id: DataTypes.INTEGER,
        company_id: { type: DataTypes.INTEGER, allowNull: true, references: { model: 'companies', key: 'id', onDelete: 'cascade' } },
        last_login_at: DataTypes.DATE,          
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
      }).then(function () {
        migration.createTable('roles', {
          id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
          name: DataTypes.STRING,
          role_type: DataTypes.INTEGER,
          created_at: DataTypes.DATE,
          updated_at: DataTypes.DATE
        }).then(function () {
          return done();
        });
      });
    });
  },

  down: function (migration, DataTypes, done) {
    migration.dropTable('users').then(function () {
      migration.dropTable('roles').then(function () {
        migration.dropTable('companies').then(function () {
          return done();
        });
      });
    });
  }
};