'use strict';

module.exports = {

  up: function (migration, DataTypes, done) {
    migration.addColumn('foods', 'available', DataTypes.BOOLEAN)
      .then(function () {
        return done();
      });
  },

  down: function (migration, DataTypes, done) {
    migration.removeColumn('foods', 'available').then(function () {
      return done();
    });
  }
};