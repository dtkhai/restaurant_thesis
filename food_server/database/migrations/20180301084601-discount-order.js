'use strict';

module.exports = {
  up: function (migration, DataTypes, done) {
    migration.createTable('discounts', {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
      name: DataTypes.STRING,
      price_requried: DataTypes.INTEGER,
      percentage_value: DataTypes.INTEGER,
      value: DataTypes.INTEGER,
      date_from: DataTypes.DATE,
      date_to: DataTypes.DATE,
      time_from: DataTypes.DATE,
      time_to: DataTypes.DATE,
      enable: DataTypes.BOOLEAN,
      active: DataTypes.BOOLEAN,
      discount_style_id: DataTypes.INTEGER,    
      company_id: DataTypes.INTEGER,          
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    }).then(function () {
      return done();
    });
  },

  down: function (migration, DataTypes, done) {
    migration.dropTable('discounts').then(function () {
      return done();
    });
  }
};