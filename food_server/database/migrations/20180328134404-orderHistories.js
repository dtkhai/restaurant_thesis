'use strict';

module.exports = {
  up: function (migration, DataTypes, done) {
    migration.createTable('order_histories', {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
      order_list: DataTypes.ARRAY(DataTypes.JSON),
      order_parent: DataTypes.INTEGER,
      canceled: DataTypes.BOOLEAN,
      company_id: DataTypes.INTEGER,          
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    }).then(function () {
      return done();
    });
  },

  down: function (migration, DataTypes, done) {
    migration.dropTable('order_histories').then(function () {
      return done();
    });
  }
};