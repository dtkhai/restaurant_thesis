'use strict';

module.exports = {
  up: (migration, Sequelize, done) => {
    var t = new Date();

    var roles = [
      { name: 'Administrator', role_type: 0, created_at: t, updated_at: t },
      { name: 'Manager', role_type: 1, created_at: t, updated_at: t },
      { name: 'Accountant', role_type: 2, created_at: t, updated_at: t },
      { name: 'Receptionist', role_type: 3, created_at: t, updated_at: t },
    ];

    var companies = [
      { name: 'Gem Center', description: 'Inspiring and innovative, our venue is fitted with the latest technology and state-of-the-art facilities to make your event a memorable one. Offering nearly 10.000 m', created_at: t, updated_at: t },
      { name: 'White Palace', description: 'White Palace là một trung tâm hội nghị hàng đầu với lối kiến trúc sang trọng, tinh tế, những tiêu chuẩn đẳng cấp cùng chất lượng dịch vụ chuyên nghiệp bậc nhất tại Thành Phố Hồ Chí Minh. Tổng diện tích sử dụng trên 6,500 mét vuông mang lại cho White Palace không gian mở rộng rãi, phù hợp nhiều loại hình hội nghị, sự kiện khác nhau', created_at: t, updated_at: t },
    ];

    var users = [
      { fname: 'admin', lname: '', password: 'e10adc3949ba59abbe56e057f20f883e', email: 'admin', role_id: 0, created_at: t, updated_at: t },
      { fname: 'quoc', lname: 'lam', password: 'e10adc3949ba59abbe56e057f20f883e', email: 'quoclv@gmail.com', role_id: 1, company_id: 2, created_at: t, updated_at: t },
      { fname: 'tu', lname: 'huynh', password: 'e10adc3949ba59abbe56e057f20f883e', email: 'hmtu@gmail.com', role_id: 1, company_id: 2, created_at: t, updated_at: t },
      { fname: 'cuong', lname: 'nguyen', password: 'e10adc3949ba59abbe56e057f20f883e', email: 'cuong', role_id: 1, company_id: 1, created_at: t, updated_at: t },
      { fname: 'ngoc', lname: 'dao', password: 'e10adc3949ba59abbe56e057f20f883e', email: 'ngoc', role_id: 2, company_id: 1, created_at: t, updated_at: t },
    ];

    migration.bulkInsert('companies', companies).then(function () {
      migration.bulkInsert('roles', roles).then(function () {
        migration.bulkInsert('users', users).then(function () {
          return done();
        })
      })
    })
  },

  down: (migration, Sequelize, done) => {
    migration.bulkDelete('roles', null).then(function () {
      migration.bulkDelete('users', null).then(function () {
        migration.bulkDelete('companies', null).then(function () {
          return done();
        })
      })
    })
  }
};
