var log4js = require('log4js');
var constants = require("../lib/constants");
var utils = require("../lib/utils");
var errors = require("../lib/errors");
var auth_utils = require("../lib/auth_utils");
var ldap_utils = require("../lib/ldap_utils");
var models = require('../models');
var logger = log4js.getLogger("restaurant_service");
var Apriori = require('../lib/apriori');
var fs = require("fs");
var uuid = require('node-uuid');

exports.GetCompanies = function (currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE) {
    var offset = (currentPage - 1) * pageSize;

    return new Promise((resolve, reject) =>
        models.Company.findAndCountAll(
            {
                include: [
                    { 
                        model: models.User,
                        attributes: ['email'] 
                    }
                ],
                limit: pageSize,
                offset: offset,
                order: ['created_at']
            }
        ).then(function (companies) {
            var data = {
                "total_companies": companies.count,
                "companies": companies.rows
            };
            resolve(data);
        }).catch(function (error) {
            logger.error(error.message);
            reject(error.message);
        })
    );
}

exports.GetCompany = function (companyId, callback) {
    return new Promise((resolve, reject) =>
        models.Company.find(
            {
                where: {
                    id: companyId
                }
            }
        ).then(function (company) {
            resolve(utils.ResultSuccess(company));
        }).catch(function (error) {
            logger.error(error.message);
            reject(error.message);
        })
    );
}

exports.CreateCompany = function (company) {
    return new Promise((resolve, reject) => {
        models.Company.create(company)
            .then(function (savedCompany) {
                resolve(utils.ResultSuccess(savedCompany));
            }).catch(function (error) {
                logger.error(error.message);
                reject(error.message);
            });
    });
}

exports.UpdateCompany = function (companyId) {
    return new Promise((resolve, reject) =>
        models.Company.find({
            where: {
                id: companyId
            }
        })
            .then(function (foundCompany) {
                if (foundCompany) {
                    var dataInfo = {
                        available: foundCompany.available == 0 ? 1 : 0
                    }

                    foundCompany.updateAttributes(dataInfo)
                        .then(function (data) {
                            resolve(data);
                        })
                        .catch(function (error) {
                            logger.error(error.message);
                            reject(error.message);
                        });
                }
            })
            .catch(function (error) {
                logger.error(error.message);
                return callback(error.message, null);
            })
    );
}

exports.DeleteCompany = function (companyId) {
    return new Promise((resolve, reject) =>
        models.Company.destroy({
            where: {
                id: companyId
            }
        })
            .then(function (deletedCompany) {
                resolve(deletedCompany);
            })
            .catch(function (error) {
                logger.error(error.message);
                reject(error.message);
            })
    );
}

exports.SearchCompanies = function (searchString, currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE, callback) {
    var offset = (currentPage - 1) * pageSize;
    searchString = utils.FormatSearchString(searchString);

    console.log(123);
    console.log(searchString);

    models.Company.findAndCountAll({
        where: {
            name: {
                $iLike: searchString
            },
        },
        limit: pageSize,
        offset: offset
    })
        .then(function (companies) {
            var data = {
                "total_companies": companies.count,
                "companies": companies.rows
            };
            return callback(null, utils.ResultSuccess(data));
        })
        .catch(function (error) {
            logger.error(error.message);
            return callback(error.message, null);
        });
}