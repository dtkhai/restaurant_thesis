var log4js = require('log4js');
var constants = require("../lib/constants");
var utils = require("../lib/utils");
var errors = require("../lib/errors");
var auth_utils = require("../lib/auth_utils");
var ldap_utils = require("../lib/ldap_utils");
var models = require('../models');
var logger = log4js.getLogger("category_service");


exports.GetGroups = function (currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE, company_id, callback) {
    var offset = (currentPage - 1) * pageSize;

    models.Category.findAndCountAll(
        {
            attributes: ['id', 'name'],
            where: {
                company_id: company_id
            },
            order: [['name']],
            limit: pageSize,
            offset: offset
        }
    ).then(function (groups) {
        var data = {
            "total_groups": groups.count,
            "groups": groups.rows
        };
        return callback(null, utils.ResultSuccess(data));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.CreateGroup = function (group, callback) {
    models.Category.create(group)
        .then(function (saved_group) {

            return callback(null, utils.ResultSuccess(saved_group));
        }).catch(function (error) {
            console.log(error.message);

            logger.error(error.message);
            return callback(error.message, null);
        });
}

exports.GetGroup = function (categoryId, callback) {
    models.Category.find(
        {
            attributes: ['id', 'name'],
            where: {
                id: categoryId
            }
        }
    ).then(function (group) {
        return callback(null, utils.ResultSuccess(group));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.DeleteGroup = function (categoryId, callback) {    
    models.Category.destroy({
        where: {
            id: categoryId
        }
    }).then(function () {
        return callback(null, utils.ResultSuccess());
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.UpdateGroup = function (group, callback) {
    models.Category.find({
        where: {
            id: group.id
        }
    }).then(function (found_group) {
        if (found_group) {
            found_group.updateAttributes(group).then(function (data) {
                return callback(null, utils.ResultSuccess(data));
            }).catch(function (error) {
                logger.error(error.message);
                return callback(error.message, null);
            });
        }
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}