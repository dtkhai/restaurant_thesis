var log4js = require('log4js');
var constants = require("../lib/constants");
var utils = require("../lib/utils");
var errors = require("../lib/errors");
var auth_utils = require("../lib/auth_utils");
var ldap_utils = require("../lib/ldap_utils");
var models = require('../models');
var logger = log4js.getLogger("discount_service");
var Apriori = require('../lib/apriori');
var fs = require("fs");
var uuid = require('node-uuid');
var lodash = require('lodash');
var Promise = require('bluebird');

exports.GetDiscounts = function (currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE, isActive, company_id, callback) {
    var offset = (currentPage - 1) * pageSize;

    models.Discount.findAndCountAll(
        {
            limit: pageSize,
            offset: offset,
            order: [['updated_at', 'DESC']],
            where: {
                active: isActive,
                company_id: company_id
            },
        }
    ).then(function (discounts) {
        var data = {
            "total_discounts": discounts.count,
            "discounts": discounts.rows
        };
        return callback(null, utils.ResultSuccess(data));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.GetDiscount = function (discountId, callback) {
    models.Discount.find(
        {
            where: {
                id: discountId
            }
        }
    ).then(function (discount) {
        return callback(null, utils.ResultSuccess(discount));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.GetDiscountAllBill = function (company_id, callback) {
    var today = new Date();

    models.Discount.find(
        {
            where: {
                date_from: { $lte: today },
                date_to: { $gte: today },
                company_id: company_id,
                discount_style_id: 0,
                enable: true,
                active: true
            }
        }
    ).then(function (discount) {
        console.log(discount);
        return callback(null, utils.ResultSuccess(discount));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}


exports.CreateDiscount = function (discount, food_ids, combo_ids) {
    var promisesAll = [];

    return new Promise((resolve, reject) =>
        models.Discount.create(discount).then(function (saved_discount) {
            if (discount.discount_style_id == 1) {
                if (saved_discount && food_ids && food_ids.length > 0) {
                    // Update associations
                    for (var index in food_ids) {
                        try {
                            var p = models.Food.update(
                                { discount_id: saved_discount.id },
                                {
                                    where: {
                                        id: parseInt(food_ids[index])
                                    }
                                }
                            )
                            promisesAll.push(p);
                        } catch (e) {
                            console.log(e);
                        }
                    }
                }
            } else if (discount.discount_style_id == 2) {
                if (saved_discount && combo_ids && combo_ids.length > 0) {
                    for (var index in combo_ids) {
                        try {
                            var p = models.Combo.update(
                                { discount_id: saved_discount.id },
                                {
                                    where: {
                                        id: parseInt(combo_ids[index])
                                    }
                                }
                            )
                            promisesAll.push(p);
                        } catch (e) {
                            console.log(e);
                        }
                    }
                }
            }


            Promise.all(promisesAll).spread(function (assigned_foods) {
                if (assigned_foods.every((assigned_food) => assigned_food != null)) {
                    resolve();
                } else {
                    reject('Loi roi');
                }
            }).catch(function (error) {
                reject('Something Wrong');
            })
        }))
}


exports.UpdateDiscount = function (discount_id, discount, callback) {
    models.Discount.find({
        where: {
            id: discount_id
        }
    })
        .then(function (found_discount) {
            if (found_discount) {
                found_discount.updateAttributes(discount)
                    .then(function (data) {
                        return callback(null, utils.ResultSuccess(data));
                    })
                    .catch(function (error) {
                        logger.error(error.message);
                        return callback(error.message, null);
                    });
            }
        })
        .catch(function (error) {
            logger.error(error.message);
            return callback(error.message, null);
        });
}

exports.changeStatusDiscount = function (discount_id, isActive, callback) {
    models.Discount.find({
        where: {
            id: discount_id
        }
    })
        .then(function (found_discount) {
            if (found_discount) {
                found_discount.updateAttributes({ active: isActive })
                    .then(function (data) {
                        return callback(null, utils.ResultSuccess(data));
                    })
                    .catch(function (error) {
                        logger.error(error.message);
                        return callback(error.message, null);
                    });
            }
        })
        .catch(function (error) {
            logger.error(error.message);
            return callback(error.message, null);
        });
}


exports.changeEnableStatus = function (discount_id, isEnabled, callback) {
    models.Discount.find({
        where: {
            id: discount_id
        }
    }).then(function (found_discount) {
        if (found_discount) {
            found_discount.updateAttributes({ enable: isEnabled })
                .then(function (data) {
                    return callback(null, utils.ResultSuccess(data));
                })
                .catch(function (error) {
                    logger.error(error.message);
                    return callback(error.message, null);
                });
        }
    })
        .catch(function (error) {
            logger.error(error.message);
            return callback(error.message, null);
        });
}