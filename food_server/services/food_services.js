var log4js = require('log4js');
var constants = require("../lib/constants");
var utils = require("../lib/utils");
var errors = require("../lib/errors");
var auth_utils = require("../lib/auth_utils");
var ldap_utils = require("../lib/ldap_utils");
var models = require('../models');
var logger = log4js.getLogger("food_service");
var Apriori = require('../lib/apriori');
var fs = require("fs");
var uuid = require('node-uuid');

exports.Recommendation = function (userId, companyId) {
    console.log(userId);
    console.log(companyId);
    var filename = 'out' + companyId + '.csv';

    return new Promise((resolve, reject) =>
        models.User.findOne({
            attributes: ['last_login_at'],
            where: {
                id: userId
            }
        }).then(function (user) {
            var today = new Date();

            models.User.update(
                { last_login_at: new Date() },
                {
                    where: { id: userId }
                }
            ).then(() => console.log('happy new year'));

            new Apriori.Algorithm(0.15, 0.6, false).showAnalysisResultFromFile(filename, function (a) {
                console.log(a);
                resolve(a);
            });

            // Update once time each day
            // if (!user.last_login_at || user.last_login_at.getDate() != today.getDate()) {
            //     new Apriori.Algorithm(0.15, 0.6, false).showAnalysisResultFromFile('out.csv', function (a) {
            //         console.log(a);
            //         resolve(a);
            //     });
            // } else {
            //     resolve([]);
            // }
        })
    )
}

exports.GetFoods = function (currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE, company_id, callback) {
    var today = new Date();
    var offset = (currentPage - 1) * pageSize;
    models.Food.findAndCountAll(
        {
            where: {
                company_id: company_id
            },
            include: [{
                model: models.Discount,
                attributes: ['id', 'name', 'price_requried', 'percentage_value', 'value'],
                where: {
                    date_from: { $lte: today },
                    date_to: { $gte: today },
                },
                paranoid: false,
                required: false
            }],
            limit: pageSize,
            offset: offset
        }
    ).then(function (foods) {
        foodImagePipe(foods);

        var data = {
            "total_foods": foods.count,
            "foods": foods.rows
        };
        return callback(null, utils.ResultSuccess(data));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.GetFoodByIds = function (food_ids, callback) {
    var today = new Date();

    models.Food.findAndCountAll(
        {
            where: {
                id: food_ids
            },
            include: [
                {
                    model: models.Discount,
                    attributes: ['id', 'name', 'price_requried', 'percentage_value', 'value'],
                    where: {
                        date_from: { $lte: today },
                        date_to: { $gte: today },
                        enable: true
                    },
                    paranoid: false,
                    required: false
                }],
        }
    ).then(function (foods) {
        foodImagePipe(foods);

        var data = {
            "total_foods": foods.count,
            "foods": foods.rows
        };

        return callback(null, utils.ResultSuccess(data));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.GetFood = function (foodId, callback) {
    models.Food.find(
        {
            where: {
                id: foodId
            },
            include: {
                model: models.Category,
                as: 'categories',
                attributes: ['id'],
                through: {
                    attributes: [],
                }
            }
        }
    ).then(function (food) {
        if (food.main_image) {
            food.main_image = constants.WEB_HOST_NAME + '/food_images/' + food.main_image;
        }
        return callback(null, utils.ResultSuccess(food));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.CreateFood = function (name, price, main_image, images, description, note, category_ids, company_id, callback) {
    var mainImageName = '';
    var imageNames = [];

    if (main_image) {
        var buff = new Buffer(main_image.value, 'base64');
        fs.writeFileSync("./public/food_images/" + main_image.filename, buff);
        mainImageName = main_image.filename;
    }


    if (images.length > 0 && images[0].avatar) {
        for (var i = 0; i < images.length; ++i) {
            var buff = new Buffer(images[i].avatar.value, 'base64');
            fs.writeFileSync("./public/food_images/" + images[i].avatar.filename, buff);
            imageNames.push(images[i].avatar.filename);
        }
    } else {
        imageNames.push({});
    }

    models.Food.create({
        name: name,
        price: price,
        description: description,
        note: note,
        main_image: mainImageName,
        images: imageNames,
        company_id: company_id,
        available: true
    }).then(function (saved_food) {
        saved_food.setCategories(category_ids);

        return callback(null, utils.ResultSuccess(saved_food));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.UpdateFood = function (food_id, name, price, main_image, images, description, note, category_ids, callback) {
    models.Food.find({
        where: {
            id: food_id
        }
    }).then(function (found_food) {
        if (found_food) {
            var mainImageName = '';

            if (main_image) {
                var buff = new Buffer(main_image.value, 'base64');
                fs.writeFileSync("./public/food_images/" + main_image.filename, buff);
                mainImageName = main_image.filename;
            } else {
                mainImageName = found_food.main_image;
            }

            found_food.updateAttributes({
                name: name,
                price: price,
                description: description,
                note: note,
                main_image: mainImageName,
            })
                .then(function (updated_food) {
                    updated_food.setCategories(category_ids);

                    return callback(null, utils.ResultSuccess(updated_food));
                })
                .catch(function (error) {
                    logger.error(error.message);
                    return callback(error.message, null);
                });
        }
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}


exports.UpdateFoodStatus = function (food_id, callback) {
    var status = true;

    models.Food.find({
        where: {
            id: food_id
        }
    }).then(function (found_food) {
        if (found_food) {
            found_food.available == true || found_food.available == null ? status = false : status = true;

            found_food.updateAttributes({
                'available': status
            }).then(function (updated_food) {

                return callback(null, utils.ResultSuccess(updated_food));
            }).catch(function (error) {
                logger.error(error.message);
                return callback(error.message, null);
            });
        }
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.DeleteFood = function (food_id, callback) {
    models.Food.find({
        where: {
            id: food_id
        }
    }).then(food => {
        food.setCategories([]).then(associatedCategories => {
            food.destroy().then(function () {
                return callback(null, utils.ResultSuccess());
            }).catch(function (error) {
                logger.error(error.message);
                return callback(error.message, null);
            });
        })
    })


}

exports.FilterFoods = function (group_id, currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE, callback) {
    var offset = (currentPage - 1) * pageSize;
    var today = new Date();

    models.Food.findAndCountAll(
        {
            include: [{
                model: models.Category,
                as: 'categories',
                attributes: [],
                where: {
                    id: group_id,
                },
                through: {
                    attributes: [],
                }
            },
            {
                model: models.Discount,
                attributes: ['id', 'name', 'price_requried', 'percentage_value', 'value'],
                where: {
                    date_from: { $lte: today },
                    date_to: { $gte: today },
                    enable: true
                },
                paranoid: false,
                required: false
            }],

            limit: pageSize,
            offset: offset
        }).then(function (foods) {
            foodImagePipe(foods);

            console.log(1);
            console.log(foods);

            var data = {
                "total_foods": foods.count,
                "foods": foods.rows
            };

            console.log(2);
            console.log(data);

            return callback(null, utils.ResultSuccess(data));
        }).catch(function (error) {
            logger.error(error.message);
            return callback(error.message, null);
        });
}

exports.SearchFoods = function (searchString, currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE, callback) {
    var offset = (currentPage - 1) * pageSize;
    searchString = utils.FormatSearchString(searchString);

    models.Food.findAndCountAll({
        where: {
            name: {
                $iLike: searchString
            },
        },
        limit: pageSize,
        offset: offset
    })
        .then(function (foods) {
            foodImagePipe(foods);

            var data = {
                "total_foods": foods.count,
                "foods": foods.rows
            };
            return callback(null, utils.ResultSuccess(data));
        })
        .catch(function (error) {
            logger.error(error.message);
            return callback(error.message, null);
        });
}


// Private function

function foodImagePipe(foods) {
    for (var i = 0; i < foods.rows.length; ++i) {
        var food = foods.rows[i];
        if (food.main_image) {
            food.main_image = constants.WEB_HOST_NAME + '/food_images/' + food.main_image;
        }

        if (food.images) {
            for (var j = 0; j < food.images.length; ++j) {
                food.images[j] = constants.WEB_HOST_NAME + '/food_images/' + food.images[j];
            }
        }
    }

}