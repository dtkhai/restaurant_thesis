var log4js = require('log4js');
var constants = require("../lib/constants");
var utils = require("../lib/utils");
var errors = require("../lib/errors");
var auth_utils = require("../lib/auth_utils");
var ldap_utils = require("../lib/ldap_utils");
var models = require('../models');
var logger = log4js.getLogger("combo_service");
var Apriori = require('../lib/apriori');
var fs = require("fs");
var uuid = require('node-uuid');

exports.GetCombos = function (currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE, company_id, callback) {
    var offset = (currentPage - 1) * pageSize;
    var today = new Date();

    models.Combo.findAndCountAll(
        {
            limit: pageSize,
            offset: offset,
            order: [['updated_at', 'DESC']],
            where: {
                company_id: company_id
            },
            include: [
                {
                    model: models.Food,
                    as: 'foods',
                    attributes: ['id', 'name', 'main_image'],
                    through: {
                        attributes: [],
                    }
                },
                {
                    model: models.Discount,
                    attributes: ['id', 'name', 'price_requried', 'percentage_value', 'value'],
                    where: {
                        date_from: { $lte: today },
                        date_to: { $gte: today },
                        enable: true
                    },
                    paranoid: false,
                    required: false
                }
            ]
        }
    ).then(function (combos) {
        foodImagePipe(combos);

        var data = {
            "total_combos": combos.count,
            "combos": combos.rows
        };
        return callback(null, utils.ResultSuccess(data));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.GetCombo = function (comboId, callback) {
    models.Combo.find(
        {
            where: {
                id: comboId
            },
            include: [
                {
                    model: models.Food,
                    as: 'foods',
                    attributes: ['id', 'name', 'main_image'],
                    through: {
                        attributes: [],
                    }
                }
            ]
        }
    ).then(function (combo) {
        if (combo.image) {
            combo.image = constants.WEB_HOST_NAME + '/combo_images/' + combo.image;
        }

        for (var j = 0; j < combo.foods.length; ++j) {
            combo.foods[j].main_image = constants.WEB_HOST_NAME + '/food_images/' + combo.foods[j].main_image;
        }

        return callback(null, utils.ResultSuccess(combo));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}


exports.CreateCombo = function (combo, food_ids, callback) {
    if (combo.image) {
        var buff = new Buffer(combo.image.value, 'base64');
        fs.writeFileSync("./public/combo_images/" + combo.image.filename, buff);
        combo.image = combo.image.filename;
    }

    models.Combo.create(combo)
        .then(function (saved_combo) {
            if (food_ids.length <= 0) {
                return callback(true);
            }

            var data = new Array();
            for (var i in food_ids) {
                data.push({ food_id: food_ids[i], combo_id: saved_combo.id });
            }

            models.ComboFoods.bulkCreate(data)
                .then(function () {
                    return callback(null, utils.ResultSuccess(saved_combo));
                }).catch(function (error) {
                    logger.error(error.message);
                    return callback(error.message, null);
                });
        }).catch(function (error) {
            logger.error(error.message);
            return callback(error.message, null);
        });
}

exports.UpdateCombo = function (combo_id, combo, food_ids, callback) {
    if (combo.image) {
        var buff = new Buffer(combo.image.value, 'base64');
        fs.writeFileSync("./public/combo_images/" + combo.image.filename, buff);
        combo.image = combo.image.filename;
    }

    models.Combo.find({
        where: {
            id: combo_id
        }
    }).then(function (found_combo) {
        if (found_combo) {
            var mainImageName = '';

            if (combo.image) {
                var buff = new Buffer(combo.image.value, 'base64');
                fs.writeFileSync("./public/combo_images/" + combo.image.filename, buff);
                combo.image = combo.image.filename;
            } else {
                combo.image = found_combo.image;
            }

            found_combo.updateAttributes(combo)
                .then(function (updated_combo) {
                    models.ComboFoods.destroy({
                        where: {
                            combo_id: combo_id
                        },
                    }).then(function () {
                        if (food_ids.length <= 0) {
                            return callback(true);
                        }

                        var data = new Array();
                        for (var i in food_ids) {
                            data.push({ food_id: food_ids[i], combo_id: combo_id });
                        }

                        models.ComboFoods.bulkCreate(data)
                            .then(function () {
                                return callback(null, utils.ResultSuccess(updated_combo));
                            }).catch(function (error) {
                                logger.error(error.message);
                                return callback(error.message, null);
                            });
                    }).catch(function (error) {
                        logger.error(error.message);
                        return callback(error.message, null);
                    });
                }).catch(function (error) {
                    logger.error(error.message);
                    return callback(error.message, null);
                });
        }
    })
}


exports.DeleteTable = function (comboId) {
    return new Promise((resolve, reject) =>
        models.Combo.destroy({
            where: {
                id: comboId
            }
        })
            .then(function (deletedCombo) {
                resolve(deletedCombo);
            })
            .catch(function (error) {
                logger.error(error.message);
                reject(error.message);
            })
    );
}

// Private function

function foodImagePipe(combos) {
    for (var i = 0; i < combos.rows.length; ++i) {
        var foods = combos.rows[i].foods;

        combos.rows[i].image = constants.WEB_HOST_NAME + '/combo_images/' + combos.rows[i].image;


        for (var j = 0; j < foods.length; ++j) {
            combos.rows[i].foods[j].main_image = constants.WEB_HOST_NAME + '/food_images/' + combos.rows[i].foods[j].main_image;
        }

        // if (food.images) {
        //     for (var j = 0; j < food.images.length; ++j) {
        //         food.images[j] = constants.WEB_HOST_NAME + '/combo_images/' + food.images[j];
        //     }
        // }
    }
}