var log4js = require('log4js');
var constants = require("../lib/constants");
var utils = require("../lib/utils");
var errors = require("../lib/errors");
var auth_utils = require("../lib/auth_utils");
var ldap_utils = require("../lib/ldap_utils");
var models = require('../models');
var logger = log4js.getLogger("table_service");
var Apriori = require('../lib/apriori');
var fs = require("fs");
var uuid = require('node-uuid');
var lodash = require('lodash');
var Promise = require('bluebird');

exports.GetTables = function (currentPage = 1, pageSize = constants.DEFAULT_PAGE_SIZE, roomId) {
    var offset = (currentPage - 1) * pageSize;

    return new Promise((resolve, reject) =>
        models.Table.findAndCountAll(
            {
                limit: pageSize,
                offset: offset,
                where: {
                    room_id: roomId
                },
                order: [['created_at']]

            }
        ).then(function (tables) {
            var data = {
                "total_tables": tables.count,
                "tables": tables.rows
            };
            resolve(data);
        }).catch(function (error) {
            logger.error(error.message);
            reject(error.message);
        })
    );
}

exports.GetTable = function (tableId, callback) {
    return new Promise((resolve, reject) =>
        models.Table.find(
            {
                where: {
                    id: tableId
                }
            }
        ).then(function (table) {
            resolve(table);
        }).catch(function (error) {
            logger.error(error.message);
            reject(error.message);
        })
    );
}

exports.CreateTable = function (name, capacity = constants.DEFAULT_TABLE_CAPACITY, roomId) {
    return new Promise((resolve, reject) => {
        // models.Table.findOne({
        //     where: {
        //         room_id: roomId
        //     },
        //     order: [['created_at', 'DESC']],
        // }).then(function (theLastTable) {
        //     var data = new Array();

        //     var quantity;
        //     if (theLastTable) {
        //         var i = parseInt(theLastTable.name) + 1;
        //         quantity = i + parseInt(total) - 1;
        //     } else {
        //         i = 1;
        //         quantity = parseInt(total);
        //     }

        //     for (i; i <= quantity; i++) {
        //         var today = new Date();
        //         today.setSeconds(today.getSeconds() + i);
        //         data.push({ name: i, capacity: capacity, room_id: roomId, available: 0, created_at_filter: today, created_at: today });
        //     }

        //     models.Table.bulkCreate(data)
        //         .then(function (saved_table) {
        //             resolve(utils.ResultSuccess(saved_table));
        //         }).catch(function (error) {
        //             logger.error(error.message);
        //             reject(error.message);
        //         })
        // })

        models.Table.create({
            'name': name,
            'capacity': capacity,
            'room_id': roomId
        }).then(function (saved_table) {
            resolve(utils.ResultSuccess(saved_table));
        }).catch(function (error) {
            logger.error(error.message);
            reject(error.message);
        })
    });
}

exports.UpdateTable = function (tableId, name, capacity) {
    return new Promise((resolve, reject) =>
        models.Table.find({
            where: {
                id: tableId
            }
        }).then(function (foundTable) {
            if (foundTable) {
                foundTable.updateAttributes({
                    'name': name,
                    'capacity': capacity
                }).then(function (data) {
                    resolve(data);
                }).catch(function (error) {
                    logger.error(error.message);
                    reject(error.message);
                });
            }
        }).catch(function (error) {
            logger.error(error.message);
            return callback(error.message, null);
        })
    );
}

exports.UpdatePrefix = function (prefix, roomId) {
    return new Promise((resolve, reject) =>
        models.Table.update(
            { prefix: prefix },
            { where: { room_id: roomId } }
        ).then(function () {
            resolve();
        }).catch(function (error) {
            logger.error(error.message);
            reject(error.message);
        })
    )
}

exports.UpdateTablePosition = function (tableMap) {
    var promisesAll = [];

    return new Promise((resolve, reject) => {
        for (var i = 0; i < tableMap.length; ++i) {
            console.log('Hello World');
            console.log(tableMap[i].column);
            console.log(tableMap[i].row);

            try {
                var p = models.Table.update(
                    {
                        column: tableMap[i].column,
                        row: tableMap[i].row,
                    },
                    {
                        where: {
                            id: parseInt(tableMap[i].table_id)
                        }
                    }
                )
                promisesAll.push(p);
            } catch (e) {
                console.log(e);
            }
        }

        Promise.all(promisesAll).spread(function (updated_tables) {
            if (updated_tables.every((updated_table) => updated_table != null)) {
                resolve();
            } else {
                reject('Khong the update');
            }
        }).catch(function (error) {
            reject('Something Wrong');
        })
    });
}

exports.DeleteTable = function (tableId) {
    return new Promise((resolve, reject) =>
        models.Table.destroy({
            where: {
                id: tableId
            }
        })
            .then(function (deletedTable) {
                resolve(deletedTable);
            })
            .catch(function (error) {
                logger.error(error.message);
                reject(error.message);
            })
    );
}