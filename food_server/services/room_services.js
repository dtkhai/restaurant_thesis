var log4js = require('log4js');
var constants = require("../lib/constants");
var utils = require("../lib/utils");
var errors = require("../lib/errors");
var auth_utils = require("../lib/auth_utils");
var ldap_utils = require("../lib/ldap_utils");
var models = require('../models');
var logger = log4js.getLogger("room_service");


exports.GetRooms = function (company_id, callback) {

    models.Room.findAndCountAll(
        {
            where: {
                company_id: company_id
            },
            attributes: ['id', 'name'],
        }
    ).then(function (rooms) {
        var data = {
            "total_rooms": rooms.count,
            "rooms": rooms.rows
        };
        return callback(null, utils.ResultSuccess(data));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.GetRoom = function (roomId, callback) {
    models.Room.find(
        {
            where: {
                id: roomId
            }
        }
    ).then(function (room) {
        return callback(null, utils.ResultSuccess(room));
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}

exports.UpdateRoom = function (roomId, room, callback) {
    models.Room.find({
        where: {
            id: roomId
        }
    }).then(function (found_room) {
        if (found_room) {

            found_room.updateAttributes(room).then(function (updated_room) {
                return callback(null, utils.ResultSuccess(updated_room));
            }).catch(function (error) {
                logger.error(error.message);
                return callback(error.message, null);
            });
        }
    }).catch(function (error) {
        logger.error(error.message);
        return callback(error.message, null);
    });
}


exports.CreateRoom = function (room, callback) {
    models.Room.create(room)
        .then(function (saved_room) {

            return callback(null, utils.ResultSuccess(saved_room));
        }).catch(function (error) {
            console.log(error.message);

            logger.error(error.message);
            return callback(error.message, null);
        });
}

exports.DeleteRoom = function (roomId) {
    return new Promise((resolve, reject) =>
        models.Room.destroy({
            where: {
                id: roomId
            }
        })
            .then(function (deletedRoom) {
                resolve(deletedRoom);
            })
            .catch(function (error) {
                logger.error(error.message);
                reject(error.message);
            })
    );
}
