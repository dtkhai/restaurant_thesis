var log4js = require('log4js');
var utils = require("../lib/utils");
var models = require('../models');
var logger = log4js.getLogger("order_history_service");
if (typeof require !== 'undefined') XLSX = require('xlsx');

exports.GetOrderHistories = function (company_id, year = 2018, month = 0) {
    var start_date = new Date();
    var end_date = new Date();

    console.log(year, month);

    return new Promise((resolve, reject) => {
        if (month == 0) {
            start_date.setFullYear(year, 1, 1);
            end_date.setFullYear(year, 12, 31);

            models.OrderHistory.findAndCountAll({
                attributes: ['id', 'total_price', 'created_at'],
                where: {
                    created_at: {
                        $and:
                            [
                                { $gte: start_date },
                                { $lt: end_date }
                            ]
                    },
                    company_id: company_id,
                },
                order: [['updated_at', 'DESC']]
            }).then(function (orders) {
                var data = {
                    "total_orders": orders.count,
                    "orders": orders.rows
                };
                return resolve(utils.ResultSuccess(data));
            }).catch(function (error) {
                logger.error(error.message);
                return reject(error.message);
            })
        } else {
            start_date.setFullYear(year, month - 1, 1);
            end_date.setFullYear(year, month - 1, 31);

            models.OrderHistory.findAndCountAll({
                attributes: ['id', 'total_price', 'created_at'],
                where: {
                    created_at: {
                        $and:
                            [
                                { $gte: start_date },
                                { $lt: end_date }
                            ]
                    },
                    company_id: company_id,
                },
                order: [['updated_at', 'DESC']]
            }).then(function (orders) {
                var data = {
                    "total_orders": orders.count,
                    "orders": orders.rows
                };
                return resolve(utils.ResultSuccess(data));
            }).catch(function (error) {
                logger.error(error.message);
                return reject(error.message);
            })
        }

    });
}

exports.CreateOrderHistory = function (order_list, order_parent, canceled, company_id, total_price) {
    return new Promise((resolve, reject) =>
        models.OrderHistory.create(
            {
                order_list: order_list,
                order_parent: order_parent,
                canceled: canceled,
                total_price: total_price,
                company_id: company_id
            }
        ).then(function (result) {
            order_ids = order_list.map(x => x.food_id);
            var filename = 'out' + company_id + '.csv';
            console.log(order_ids);
            try {
                var wb = XLSX.readFile(filename);

                var first_sheet_name = wb.SheetNames[0];
                var ws = wb.Sheets[first_sheet_name];
                XLSX.utils.sheet_add_aoa(ws, [order_ids], { origin: -1 });

            } catch (e) {
                var wb = { SheetNames: ['Sheet1'], Sheets: { Sheet1: {} } }
                var first_sheet_name = wb.SheetNames[0];
                var ws = wb.Sheets[first_sheet_name];
                XLSX.utils.sheet_add_aoa(ws, [[''], order_ids]);
            }

            XLSX.utils.book_append_sheet(wb, ws, 'out');

            XLSX.writeFile(wb, filename);

            resolve(result);
        }).catch(function (error) {
            reject(error.message);
        })
    );
}