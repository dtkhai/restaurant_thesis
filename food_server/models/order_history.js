'use strict';
module.exports = function (sequelize, DataTypes) {
    var OrderHistory = sequelize.define('OrderHistory',
        {
            order_list: DataTypes.ARRAY({'food_id': DataTypes.INTEGER, 'amount': DataTypes.INTEGER}),
            order_list: DataTypes.ARRAY(DataTypes.JSON),
            canceled: DataTypes.BOOLEAN,
            company_id: DataTypes.INTEGER,          
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                }
            },
            underscored: true,
            tableName: "order_histories"
        });
    return OrderHistory;
};