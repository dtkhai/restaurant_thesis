'use strict';
module.exports = function (sequelize, DataTypes) {
    var Table = sequelize.define('Table',
        {
            name: DataTypes.STRING,
            capacity: DataTypes.INTEGER,
            room_id: DataTypes.INTEGER,
            company_id: DataTypes.INTEGER,
            available: DataTypes.INTEGER,
            prefix: DataTypes.STRING,
            column: DataTypes.INTEGER,
            row: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    Table.belongsTo(models.Room, { foreignKey: 'room_id' });
                }
            },
            underscored: true,
            tableName: "tables",
        });
    return Table;
};