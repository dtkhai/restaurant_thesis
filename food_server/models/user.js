'use strict';
module.exports = function (sequelize, DataTypes) {
  var User = sequelize.define('User',
    {
      fname: DataTypes.STRING,
      lname: DataTypes.STRING,
      password: DataTypes.STRING,
      age: DataTypes.INTEGER,
      description: DataTypes.STRING,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      role_id: DataTypes.INTEGER, 
      company_id: DataTypes.INTEGER,
      last_login_at: DataTypes.DATE,          
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    },
    {
      classMethods: {
        associate: function (models) {
            User.belongsTo(models.Role, { foreignKey: 'role_id', as: 'roles' });
            User.belongsTo(models.Company, { foreignKey: 'company_id', as: 'company' });
        }
      },
      underscored: true,
      tableName: "users"
    });
  return User;
};