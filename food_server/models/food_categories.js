'use strict';
module.exports = function (sequelize, DataTypes) {
    var FoodCategories = sequelize.define('FoodCategories',
        {
            category_id: DataTypes.INTEGER,
            food_id: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    FoodCategories.belongsTo(models.Food);
                    FoodCategories.belongsTo(models.Category);
                }
            },
            underscored: true,
            tableName: "food_categories"
        });
    return FoodCategories;
};