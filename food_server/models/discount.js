'use strict';
module.exports = function (sequelize, DataTypes) {
    var Discount = sequelize.define('Discount',
        {
            name: DataTypes.STRING,
            price_requried: DataTypes.INTEGER,
            percentage_value: DataTypes.INTEGER,
            value: DataTypes.INTEGER,
            date_from: DataTypes.DATE,
            date_to: DataTypes.DATE,
            time_from: DataTypes.DATE,
            time_to: DataTypes.DATE,
            enable: DataTypes.BOOLEAN,
            active: DataTypes.BOOLEAN,
            discount_style_id: DataTypes.INTEGER,
            company_id: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    Discount.hasMany(models.Food, { foreignKey: 'discount_id' });
                    Discount.hasMany(models.Combo, { foreignKey: 'discount_id' });
                }
            },
            underscored: true,
            tableName: "discounts"
        });
    return Discount;
};