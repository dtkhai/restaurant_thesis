'use strict';
module.exports = function (sequelize, DataTypes) {
    var Food = sequelize.define('Food',
        {
            name: DataTypes.STRING,
            main_image: DataTypes.STRING,
            images: DataTypes.ARRAY(DataTypes.JSON),
            description: DataTypes.TEXT,
            price: DataTypes.INTEGER,
            available: DataTypes.BOOLEAN,
            discount_id: DataTypes.INTEGER,
            company_id: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    Food.belongsTo(models.Discount, { foreignKey: 'discount_id' });
                    Food.belongsToMany(models.Combo, { as: 'combos', through: models.ComboFoods, foreignKey: 'food_id' });
                    Food.belongsToMany(models.Category, { as: 'categories', through: models.FoodCategories, foreignKey: 'food_id'});
                }
            },
            underscored: true,
            tableName: "foods"
        });
    return Food;
};