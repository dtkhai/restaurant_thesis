'use strict';
module.exports = function (sequelize, DataTypes) {
    var ComboFoods = sequelize.define('ComboFoods',
        {
            combo_id: DataTypes.INTEGER,
            food_id: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    ComboFoods.belongsTo(models.Food);
                    ComboFoods.belongsTo(models.Combo);
                }
            },
            underscored: true,
            tableName: "combo_foods"
        });
    return ComboFoods;
};