'use strict';
module.exports = function (sequelize, DataTypes) {
    var Company = sequelize.define('Company',
        {
            name: DataTypes.STRING,
            description: DataTypes.TEXT,
            image: DataTypes.STRING,
            country: DataTypes.STRING,
            district: DataTypes.STRING,
            address: DataTypes.STRING,
            user_id: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    Company.hasMany(models.User, { foreignKey: 'company_id' });
                    Company.hasMany(models.Room, { foreignKey: 'company_id' });
                    Company.hasMany(models.Discount, { foreignKey: 'company_id' });
                    Company.hasMany(models.OrderHistory, { foreignKey: 'company_id' });
                    Company.hasMany(models.Category, { foreignKey: 'company_id' });
                    Company.hasMany(models.Food, { foreignKey: 'company_id' });
                }
            },
            underscored: true,
            tableName: "companies"
        });
    return Company;
};