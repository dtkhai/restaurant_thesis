'use strict';
module.exports = function (sequelize, DataTypes) {
    var Category = sequelize.define('Category',
        {
            name: DataTypes.STRING,
            company_id: DataTypes.INTEGER,          
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    Category.belongsToMany(models.Food, { as: 'foods', through: models.FoodCategories, foreignKey: 'category_id'});
                }
            },
            underscored: true,
            tableName: "categories"
        });
    return Category;
};