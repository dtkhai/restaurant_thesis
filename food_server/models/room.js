'use strict';
module.exports = function (sequelize, DataTypes) {
    var Room = sequelize.define('Room',
        {
            name: DataTypes.STRING,
            company_id: DataTypes.INTEGER,
            available: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    Room.hasMany(models.Table, { foreignKey: 'room_id' });
                }
            },
            underscored: true,
            tableName: "rooms"
        });
    return Room;
};