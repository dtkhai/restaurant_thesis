'use strict';
module.exports = function (sequelize, DataTypes) {
    var Combo = sequelize.define('Combo',
        {
            name: DataTypes.STRING,
            image: DataTypes.STRING,
            price: DataTypes.INTEGER,
            discount_id: DataTypes.INTEGER,
            company_id: DataTypes.INTEGER,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE
        },
        {
            classMethods: {
                associate: function (models) {
                    Combo.belongsToMany(models.Food, { as: 'foods', through: models.ComboFoods, foreignKey: 'combo_id'});
                    Combo.belongsTo(models.Discount, { foreignKey: 'discount_id' });
                }
            },
            underscored: true,
            tableName: "combos"
        });
    return Combo;
};