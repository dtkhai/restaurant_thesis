define({ "api": [
  {
    "type": "post",
    "url": "/foods",
    "title": "Create New Food",
    "description": "<p>Create a new food.</p>",
    "version": "1.0.0",
    "name": "CreateFood",
    "permission": [
      {
        "name": "manager_account"
      }
    ],
    "group": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "main_image",
            "description": "<p>Food Image.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "images",
            "description": "<p>Food images.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Food price.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "discount",
            "description": "<p>id Food discount id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Food description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>Food note.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food",
            "description": "<p>group id Food food group id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Created section data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.section.name",
            "description": "<p>Food name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.section.description",
            "description": "<p>Food description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.section.duration",
            "description": "<p>Food duration.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"data\": {\n     \"section\": {\n         \"id\": 1,\n         \"name\": \"Bun bo hue\"\",\n         \"main_image\": \"https://www.w3schools.com/w3css/img_fjords.jpg\",\n         \"images\": [\"https://www.w3schools.com/w3css/img_fjords.jpg\", \"https://imagejournal.org/wp-content/uploads/bb-plugin/cache/23466317216_b99485ba14_o-panorama.jpg\"],\n         \"price\": 50000,\n         \"discount id\": 0,\n         \"description\": \"is a popular Vietnamese soup containing rice vermicelli\",\n         \"note\": \"it bo\",\n         \"food_group_id\": 1,\n     }\n  }\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"You are not authorized to access this page!\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./routes/foods.js",
    "groupTitle": "Food"
  },
  {
    "type": "delete",
    "url": "/foods/:id",
    "title": "Delete Food",
    "description": "<p>Teaching and study managing department delete a food</p>",
    "version": "1.0.0",
    "name": "DeleteFood",
    "permission": [
      {
        "name": "manager_account"
      }
    ],
    "group": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Food token string to authorize.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Food id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"Cannot delete this food\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./routes/foods.js",
    "groupTitle": "Food"
  },
  {
    "type": "get",
    "url": "/foods/",
    "title": "Get All Foods",
    "description": "<p>Get all foods from database and return list of foods based pagenation setting.</p>",
    "version": "1.0.0",
    "name": "GetFoods",
    "permission": [
      {
        "name": "everyone"
      }
    ],
    "group": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "current_page",
            "defaultValue": "1",
            "description": "<p>Current page value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page_size",
            "defaultValue": "20",
            "description": "<p>Number of foods per page value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Foods list with pagenation info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.total_items",
            "description": "<p>Total number of foods.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.foods",
            "description": "<p>List of food accounts of requested page (array of object).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.foods.id",
            "description": "<p>Food id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.foods.name",
            "description": "<p>Food name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.foods.avatar",
            "description": "<p>Food avatar.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.foods.description",
            "description": "<p>Description of the food.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.foods.price",
            "description": "<p>Food price.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.foods.food_group_id",
            "description": "<p>Food Group id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.foods.roles",
            "description": "<p>List of role ids that food belongs. If there is no roles, this food is in Employee role.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.foods.roles.role_id",
            "description": "<p>Role id that food belongs to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.foods.last_updated",
            "description": "<p>Timestamps of last synced from .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"data\": {\n     \"total_items\": 15,\n     \"foods\": [{\n         \"id\": 5,\n         \"name\": \"BÚN BÒ HUẾ\",\n         \"avatar\": \"['http://product.hstatic.net/1000205756/product/bun-bo-hue_c53c0d359aac47e8b758000462db99ee_master.png']\",\n         \"description\": \"Bún bò món Huế của Nhà hàng món Huế được làm từ nguyên liệu chính là: Thịt bò, thịt heo, chả cua, huyết heo, chả thẻ, sợi bún lớn và...\",\n         \"price\": \"65,000\",\n         \"food_group_id\": 0,\n       }]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"You are not authorized to access this page!\",\n  \"code\": \"AUTHORIZE_01\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./routes/foods.js",
    "groupTitle": "Food"
  },
  {
    "type": "post",
    "url": "/foods/search",
    "title": "Search Foods",
    "description": "<p>Search foods via account and fullname and get list of foods with pagenation setting.</p>",
    "version": "1.0.0",
    "name": "SearchFoods",
    "permission": [
      {
        "name": "everyone"
      }
    ],
    "group": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>User token string to authorize.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search_string",
            "description": "<p>Search text (food account or fullname).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "current_page",
            "defaultValue": "1",
            "description": "<p>Current page value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page_size",
            "defaultValue": "20",
            "description": "<p>Number of foods per page value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Foods list with pagenation info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.total_items",
            "description": "<p>Total number of foods.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"data\": {\n     \"total_items\": 45,\n     \"foods\": [{\n         \"id\": 5,\n         \"name\": \"Bun bo hue\"\",\n         \"main_image\": \"https://www.w3schools.com/w3css/img_fjords.jpg\",\n         \"images\": [\"https://www.w3schools.com/w3css/img_fjords.jpg\", \"https://imagejournal.org/wp-content/uploads/bb-plugin/cache/23466317216_b99485ba14_o-panorama.jpg\"],\n         \"price\": 50000,\n         \"discount id\": 0,\n         \"description\": \"is a popular Vietnamese soup containing rice vermicelli\",\n         \"note\": \"it bo\",\n         \"food_group_id\": 1,\n       }]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"You are not authorized to access this page!\",\n  \"code\": \"AUTHORIZE_01\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./routes/foods.js",
    "groupTitle": "Food"
  },
  {
    "type": "put",
    "url": "/foods/:id",
    "title": "Update Food",
    "description": "<p>Update information of foods.</p>",
    "version": "1.0.0",
    "name": "UpdateFood",
    "permission": [
      {
        "name": "manager_account"
      }
    ],
    "group": "Food",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"You are not authorized to access this page!\",\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>User token string to authorize.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Food id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "main_image",
            "description": "<p>Food Image.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "images",
            "description": "<p>Food images.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Food price.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "discount",
            "description": "<p>id Food discount id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Food description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>Food note.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food",
            "description": "<p>group id Food food group id.</p>"
          }
        ]
      }
    },
    "filename": "./routes/foods.js",
    "groupTitle": "Food"
  },
  {
    "type": "post",
    "url": "/users/login",
    "title": "Login User",
    "description": "<p>Request authenticate a user with username and password</p> <ul> <li>Step 1: server verify username &amp; password</li> <li>Step 2: query database to check if user is active. If no, return failed.</li> <li>Step 3: execute LDAP authenticate. If succeed, goto Step 4</li> <li>Step 4: Update last authenticate time and return data</li> </ul>",
    "version": "1.0.0",
    "name": "AuthenticateUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username to authenticate.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of user.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>User authentication information.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>User id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.fullname",
            "description": "<p>User fullname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data.roles",
            "description": "<p>List of roles that user belongs (array of integer). 1: Admin account; 2: HR account</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.token",
            "description": "<p>Authentication token string generated from server.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"data\": {\n     \"fullname\": \"Michael Sumakher\",\n     \"roles\": [2],\n     \"token\": \"abcdxfvz1234567--*324\",\n  }\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"Invalid username or password\",\n  \"code\": \"AUTHENTICATE_01\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/register",
    "title": "Register",
    "description": "<p>Create a new user.</p>",
    "version": "1.0.0",
    "name": "CreateUser",
    "permission": [
      {
        "name": "AS_account"
      }
    ],
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "full_name",
            "description": "<p>User full_name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "role_id",
            "description": "<p>User role_id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Created section data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.section.username",
            "description": "<p>User username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.section.full_name",
            "description": "<p>User full_name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.section.password",
            "description": "<p>User password.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.section.role_id",
            "description": "<p>User role_id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.section.age",
            "description": "<p>User age.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.section.description",
            "description": "<p>User description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.section.phone",
            "description": "<p>User phone.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.section.updated_at",
            "description": "<p>User updated_at.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.section.created_at",
            "description": "<p>User created_at.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"data\": {\n     \"section\": {\n         \"id\": 1,\n         \"name\": \"Technical Expertise\"\",\n         \"description\": \"\",\n         \"duration\": 60,\n     }\n  }\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"You are not authorized to access this page!\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/",
    "title": "Get All Users",
    "description": "<p>Get all users from database and return list of users based pagenation setting.</p>",
    "version": "1.0.0",
    "name": "GetUsers",
    "permission": [
      {
        "name": "hr_account"
      }
    ],
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>User token string to authorize.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "current_page",
            "defaultValue": "1",
            "description": "<p>Current page value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page_size",
            "defaultValue": "20",
            "description": "<p>Number of users per page value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Users list with pagenation info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.total_items",
            "description": "<p>Total number of users.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.users",
            "description": "<p>List of user accounts of requested page (array of object).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.users.id",
            "description": "<p>User id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.users.account",
            "description": "<p>User account name (login name).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.users.fullname",
            "description": "<p>User fullname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.users.status",
            "description": "<p>Status of user (active / deactive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.users.last_login",
            "description": "<p>Timestamps of last login of user. If user did not login to the system, then this value is <code>null</code>.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.users.roles",
            "description": "<p>List of role ids that user belongs. If there is no roles, this user is in Employee role.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.users.roles.role_id",
            "description": "<p>Role id that user belongs to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.users.last_updated",
            "description": "<p>Timestamps of last synced from .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"data\": {\n     \"total_items\": 15,\n     \"users\": [{\n         \"id\": 5,\n         \"account\": \"mickeym\",\n         \"fullname\": \"Micky Mouse\",\n         \"status\": \"active\",\n         \"last_login\": \"2016 Jun 13 12:12:00\",\n         \"roles\": [\n           \"role_id\": 1,\n           \"role_id\": 2,\n         ],\n         \"last_updated\": \"2016 Jun 15 12:12:00\"\n       }]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"You are not authorized to access this page!\",\n  \"code\": \"AUTHORIZE_01\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/search",
    "title": "Search Users",
    "description": "<p>Search users via account and fullname and get list of users with pagenation setting.</p>",
    "version": "1.0.0",
    "name": "SearchUsers",
    "permission": [
      {
        "name": "hr_account"
      }
    ],
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>User token string to authorize.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search_string",
            "description": "<p>Search text (user account or fullname).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "current_page",
            "defaultValue": "1",
            "description": "<p>Current page value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page_size",
            "defaultValue": "20",
            "description": "<p>Number of users per page value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>TRUE if succeed / FALSE if failed.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Users list with pagenation info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.total_items",
            "description": "<p>Total number of users.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.users",
            "description": "<p>List of user accounts of requested page (array of object).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.users.id",
            "description": "<p>User id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.users.account",
            "description": "<p>User account name (login name).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.users.fullname",
            "description": "<p>User fullname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.users.status",
            "description": "<p>Status of user (active / deactive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.users.last_login",
            "description": "<p>Timestamps of last login of user. If user did not login to the system, then this value is <code>null</code>.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.users.roles",
            "description": "<p>List of role ids that user belongs. If there is no roles, this user is in Employee role.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.users.roles.role_id",
            "description": "<p>Role id that user belongs to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.users.last_updated",
            "description": "<p>Timestamps of last synced from .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"data\": {\n     \"total_items\": 45,\n     \"users\": [{\n         \"id\": 5,\n         \"account\": \"mickeym\",\n         \"fullname\": \"Micky Mouse\",\n         \"status\": \"active\",\n         \"last_login\": \"2016 Jun 13 12:12:00\",\n         \"roles\": [\n           \"role_id\": 1,\n           \"role_id\": 2,\n         ],\n         \"last_updated\": \"2016 Jun 15 12:12:00\"\n       }]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Failed Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"message\": \"You are not authorized to access this page!\",\n  \"code\": \"AUTHORIZE_01\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./routes/users.js",
    "groupTitle": "User"
  }
] });
