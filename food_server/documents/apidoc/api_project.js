define({
  "name": "Sample API Service",
  "version": "1.0.0",
  "description": "",
  "title": "Sample API document",
  "url": "http://localhost:8765",
  "template": {
    "withCompare": true,
    "withGenerator": true
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-03-29T13:36:06.260Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
